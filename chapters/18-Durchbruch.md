Durchbruch
==========

*CN: Sex, Genitalien, Tease and Denial, Masturbieren, Minderwertigkeitsgefühle, Vergiftung,
Fieber, makaberer Humor.*

"Ich wünschte, wir hätten das nicht zusammen erlebt.", murmelte Marusch.

Es war spät in der Nacht. Das Meer rauschte an der Küste der einsamen
Insel auf eine Weise, die immer noch nicht wieder zum Wind passte. Die
See hatte sich nach dem Sturm noch nicht wieder beruhigt. Die
Küste war hier steinig. Sie hatten Mühe gehabt, die Ormorane
hinaufzutragen. Sie wollten sie nicht über den Untergrund ziehen,
um ihren Rumpf zu schonen, aber die Steine unter den Füßen, während sie ihr
Gewicht zu zweit getragen hatten, waren alles andere als angenehme
Fußmassage gewesen. Lilið fühlte die Sohlen immer noch pochen. Jeder
Schritt war unangenehm. Und das war wiederum ein Signal für
sie, dass sie die Grenze ihrer Belastungsfähigkeit überschritten hatte.

"Können wir heute Nacht hier bleiben?", fragte sie matt.

Marusch nickte. "Brauchst du etwas?"

"Erholung.", sagte Lilið leise. Es fiel ihr erstaunlich schwer,
das zuzugeben. "Ich hoffe, ich werde nicht krank."

"Dann ruh dich aus." Maruschs Stimme war wieder so sanft, wie Lilið
es eher gewohnt war. "Möchtest du gestreichelt werden? Oder
gewaschen? Soll ich versuchen, mit dem Gepäck eine weiche Unterlage
für dich zu polstern?"

"Du möchtest mich also verwöhnen!", fasste Lilið zusammen. "Die
weiche Unterlage nehme ich, und dann eine Fußmassage. Und anschließend
vielleicht noch eine Kopfkraulung."

Marusch grinste. "Sehr wohl." Sie sagte es nicht unterwürfig, wie
Lilið es von manchen Bediensteten kannte, sondern weich und
vielleicht verspielt. Sie machte sich direkt an die Arbeit.

Lilið hatte es eigentlich bloß als Scherz gemeint. Tatsächlich hatte
ihr nie zuvor eine Person die Füße massiert. Aber sie bereute
keine Sekunde, sich zu der Äußerung dieses Wunschs hinreißen
gelassen zu haben. Als sie so entspannt, wie die Umgebung es hergab,
auf dem Rücken lag, der leichte Nachtwind sie abkühlte, setzte
sich Marusch zu ihren Füßen in einen Schneidersitz und rieb und
drückte zärtlich mit ihren Fingern in ihrer Fußsohle herum. Lilið
hatte nicht geahnt, welche Dimension von entspannend das sein würde. Und
sie fühlte sich dabei geliebt. Vielleicht war sie es auch ein wenig.

Lilið schloss die Augen, atmete bewusst und fühlte in die Berührung
hinein. Nicht nur ihre Füße entpannten sich dabei, sondern das weiche
Gefühl drang auch in ihren Rücken und andere Stellen ihres Körpers vor. Aber
sie schaffte es nicht, sich nur darauf zu fokussieren. Maruschs Satz,
mit dem sie vorhin die Stille zwischen ihnen unterbrochen hatte, ging Lilið
nicht aus dem Kopf. "Du hättest diese Meute lieber allein erlebt?", fragte
sie. "War es überhaupt die Meute, die in dir diesen Hass ausgelöst
hat?" Und dann hinterfragte sie noch einmal: "Habe ich richtig gelesen,
dass es Hass war?"

Marusch legte ihren Fuß ab und nahm stattdessen den anderen. "Hass
stimmt vielleicht. Ich weiß es nicht. Zerstörungswut.", überlegte
Marusch. "Den Wunsch, alles zu vernichten. Alles!" Einen kurzen
Augenblick nahm Lilið die Wut wieder in Maruschs trotzdem ruhiger Stimme war.
Interessanterweise blieben ihre Hände dabei unverändert liebevoll. "Ja,
die Meute, was sie gesagt hat, was sie gemacht hat. Aber auch das
Wissen, dass es überall so ist. Die Aussichtslosigkeit. Wir leben
in einem beschissenen System, aber um es zu ändern, bräuchte es
eine Mehrheit, die sich nicht von irgendwelchen 'So gehört das
aber'-Regeln oder Klatschgeschichten aus der Monarchie ablenken
lässt. Oder deren Ideen, wie eine bessere Welt aussehen könnte, nicht
einmal halb zu Ende gedacht sind. Die Welt, auf die diese
Meute zusteuern möchte, ist immer noch derbst sexistisch. Und
Menschen mit niedrigem Skorem haben immer noch nichts zu
melden. Manche sagen zwar, Leute mit niedrigem Skorem sollte
es auch gut gehen, sie sollten die selben Menschenrechte
haben und so etwas, aber niemand hält eine Person mit niedrigem
Skorem für regierungsfähig."

"Einen niedrigem Skorem, wie die Prinzessin?", fragte Lilið.

"Zum Beispiel.", antwortete Marusch.

"Hältst du sie für regierungsfähig?", fragte Lilið. "Denn, ich
bin auf der einen Seite voll deiner Meinung. In diesem System
ist es ein Privileg, einen hohen Skorem zu haben, der sogar
manchmal Geburtsprivilegien ausgleichen kann. Und das ist
ungerecht. Beides: Die Geburtsprivilegien und die durch hohen
Skorem. Mein Inneres hat
sich deshalb immer gesträubt, anzunehmen, dass ich einen hohen
Skorem haben könnte. Ich wollte mich am liebsten davon unabhängig
durch die Welt bewegen. Ich habe die Abwertung, und später
die Aufwertung von mir als Person deutlich gespürt, und gehasst,
wann immer mein Skorem eingeschätzt wurde. Was halt nicht nur durch
Skoremetrikae passiert, sondern auch im Magie-Unterricht immer
wieder implizit. Ich habe diese Bwertung so gehasst."

Lilið machte eine kurze Sprechpause, um dem unangenehmen Gefühl von
Minderwertigkeit, das sie in der Schule ständig begleitet hatte,
bewusst keinen Raum in sich zu geben, und knüpfte stattdessen
an das Thema von vorher an: "Die Prinzessin hat deshalb tatsächlich
eher Sympathie-Punkte bei mir. Allein, weil ich die Abwertung, die
sie wegen ihres niedrigen Skorems erlebt, so sehr verachte. Aber
sie macht leider auch nicht den Eindruck auf mich, als würde
sie sich mit guten Strategien auseinandersetzen, die Welt zu einer besseren
zu ändern. Sie macht sich keine Gedanken, was für Implikationen ihr
Handeln hat, oder es ist ihr egal. Es gäbe so viele einfache
Wege, wie sie durch ein diplomatischeres Auftreten
an ihrer Beliebtheit arbeiten könnte. Meine
Meinung zu ihr als mögliche Regierungsperson leitet sich nicht
von ihrem Skorem ab." Sich selbst hinterfragend, fügte Lilið ein
skeptisches "Oder?" hinzu. "Bin ich genau so, wie diese Meute,
gefangen in Klatschnarrativen oder so etwas?"

"Du bist in keinem Fall wie diese Meute.", informierte Marusch
sachlich und seufzte dann schwer. "Ich weiß es nicht. Ob sie
eine gute Regierungsperson wäre, wenn sie die richtige Unterstützung
bekommen würde. Oder ob sie es wäre, wenn die Welt bereits eine
andere wäre. Ich kann selbst nicht einschätzen, ob die Welt im
Prinzip einfach nicht bereit ist für eine Königin wie sie, oder
ob sie auch in einer idealen Welt keine gute Königin wäre. Sie
hat Schwierigkeiten, das kann ich nicht leugnen. Sie hat in der
Tat kein besonders tiefes Verständnis von Politik, Diplomatie
oder Strategie."

"Vielleicht gäbe es in einer idealen Welt keine Monarchie mehr.", murmelte
Lilið. Sie hatte das Gefühl, dass Marusch eigentlich nicht
über Politik reden wollte und hoffte, die Bemerkung würde in
dem Falle einen Ausstieg ermöglichen.

"Wahrscheinlich nicht.", bestätigte Marusch. Sie legte Liliðs Füße
beide vorsichtig auf dem Boden ab. "Soll ich dir nun die Kopfhaut
kraulen?"

Lilið gab ein glucksendes und dann ein bestätigendes Geräusch
von sich. "Ich hoffe, du weißt, dass du ablehnen kannst."

"Ja, das weiß ich, und ich fühle mich sicher genug, es auch
zu tun, wenn ich wollte." Maruschs Stimme war durchsetzt von
ihrem Schmunzeln. "Aber ich möchte dich eben sehr gern verwöhnen."

Sie fanden eine liegende Position, in der Lilið halb auf Marusch
lag und Maruschs Finger sich in ihr Haar gruben. Ein Kribbeln durchrann
ihren Körper jedes Mal, wenn es sehr sanft an den Haarwurzeln ziepte.

Auf dem Kopf war Lilið schon oft gekrault worden, vor allem von
ihren Eltern, aber auch mal von der ein oder anderen Freundin. Sie
fand erstaunlich, wie unterschiedlich sich das personenabhängig
anfühlte.

"Ich finde das Thema anstrengend.", verriet Marusch. Lilið hatte
also recht gehabt. "Und das ist auch der Grund, -- um den Bogen
zurückzuschlagen --, warum ich die Situation lieber ohne dich erlebt
hätte.", fuhr sie fort. "Bisher gab es zwischen der Welt, die ich
mit dir erlebe, und der Welt, die uns umgibt, keine Verknüpfung
für mich. Sie fühlten sich getrennt von einander an. Ich konnte
mit dir frei davon sein und habe tatsächlich ein keines Stück
Welt gehabt, in der ich mich gefühlt habe, als dürfte sie auch
einfach so sein. Und als wäre ich darin erlaubt, so wie ich bin."

Lilið tastete nach Maruschs Hand, der, die nicht mit kraulen beschäftigt
war. Bei der Suche drückte sie ausversehen in Maruschs Genitalbereich,
was Marusch zunächst zum hastig Einatmen und dann sie beide zum
Lachen brachten. "Tut mir leid.", sagte Lilið. "Ich suche eigentlich
deine Hand, um sie zu küssen!"

Marusch schob ihre Hand vorsichtig unter Liliðs Finger. "Lilið.", fülsterte
sie.

Lilið umschloss die Hand sachte und führte sie zu ihrem Mund. Sie
verteilte nicht nur einen sanften Kuss darauf und lauschte dabei
auf Maruschs Atem, der ein bisschen angespannt war. "Kannst du den
Abend vergessen?", fragte sie.

"Leider nein.", antwortete Marusch. "Aber vielleicht werde ich in
ein oder zwei Tagen wieder ein bisschen mehr genießen können, dass
es dich in meinem Leben gibt. Es fängt schon an." Sie seufzte.

Ein unangenehm bohrendes Gefühl entstand in Liliðs Brust, als Marusch
das sagte. Nun also doch Trennungsangst. Was, wenn irgendetwas passierte
und sie in zwei Tagen nicht mehr zusammen wären? Nach dem heutigen
Tag war Liliðs Angst vor Unvorhergesehenem wieder gewachsen. Aber
noch, noch waren sie zusammen. Wie um diesen Gedanken zu unterstreichen,
küsste sie noch einmal besonders zärtlich den Handrücken vor ihrem
Mund.

---

Marusch löste sich aus dem verkuschelten Knäul, das sie
waren, als es noch stockfinster war und baute das
Zelt auf. Liliðs müder Kopf verstand nicht warum. Es war eigentlich
warm genug, um draußen zu schlafen. Erst, als es richtig zu regnen
begann, begriff sie, dass es nicht Sprühwasser von der Fahrt gewesen war, das
sich zuvor so angenehm angefühlt hatte, sondern stattdessen
Nieselregen, der Vorbote für den Wetterumschung war.

Marusch scheuchte sie aus dem Schlaf auf, als sie soweit war,
und verfrachtete die weich gebastelte Unterlage ins Zelt. Sie
fühlte sich hinterher nicht mehr so gemütlich an wie vorher. Der
Wind frischte auf und rüttelte an den Zeltwänden. Marusch stand
irgendwann später noch einmal auf, um die Zeltwände neu
abzuspannen. Der Steinstrand eignete sich eher nicht dafür,
aber der Fels weiter oben hätte sich noch weniger
geeignet. Dort, wo die Zelthäute
aufeinander lagen, drang Wasser hinein und durchnässte
einen Schlafsack, den gerade niemand von ihnen benutzte.

Das Wetter blieb so grau, als Tageslicht Lilið weckte. Es
weckte sie vielleicht auch deshalb viel zu spät. Sie mussten
aufbrechen, und eigentlich hätte sie vorher noch navigieren gewollt. Sie
hatte zwar noch eine Route im Kopf, aber es war ja bereits eine
Alternativroute. Weil diese nun ihre neue Route war, bräuchte
Lilið auch entsprechend neue Alternativen, falls etwas nicht
liefe wie geplant.

Aber dazu musste auf der nächsten Insel Zeit sein. Es war
sicherer, früher aufzubrechen als später.

Als sie die Insel hinter sich gelassen hatten und mit der
Ormorane über das Meer segelten, fühlte Lilið sich
wirklich miserabel. Ihre Glieder schmerzten, sie konnte nicht
geradeaus denken und ihr Körper tat jede Bewegung nur mit größtem
Widerwillen. Es regnete ungemütlich. Der Wind war zwar nicht so stark
wie ein Sturm, aber ziemlich hackig. Er wechselte dauernd
die Richtung.

Atmen fühlte sich an, als wäre ihre Lunge porös und würde den
Sauerstoff, den sie in sich einsog, gar nicht richtig aufnehmen. Die
Atmung selbst viel ihr leicht, aber in ihrem Körper kam die positive
Wirkung davon kaum an.

"Vielleicht musst du mich zurücklassen.", rief sie Marusch gegen
den Wind zu. Das war die Angst vom Vorabend, die aus ihr sprach,
das wusste sie. Aber es fühlte sich auch realistisch an.

Marusch blickte sich alarmiert zu ihr um. Als der Wind wieder für
ein paar Momente nachließ, sie ihr Gewicht also nicht über die
Bordkante hängen musste, rückte sie kurz an Lilið heran und
legte eine Hand auf ihre Stirn. "Nicht gut, das.", sagte sie. "Aber
du redest Unsinn."

"Weil ich Fieber habe?", fragte Lilið.

"Erhöhte Temperatur, würde ich sagen.", beruhigte Marusch. "Ja,
kann sein, dass das die Ursache für den Unsinn ist, den du redest."

"Ich dachte, weil ich Fieber habe, oder erhöhte Temperatur,
musst du mich vielleicht zurücklassen.", erklärte Lilið. Ihre
Gedanken dümpelten träge in einem Gedankenmeer, das sich
kein Beispiel an der dunklen, welligen See nahm, das um
sie herum wogte.

"Ich dachte mir schon, dass dir die Idee gekommen ist, weil
du nicht ganz erfolgreich versucht hast, geradeaus zu denken.", antwortete Marusch
mit einem Grinsen, das aber nicht glücklich wirkte. "Aber es
ergibt keinen Sinn! Ich kann nicht ohne dich einfach irgendwohin. Wenn
du nicht mehr weitersegeln kannst, sitze ich mit dir fest. Und
dann werden wir uns, sobald es Sinn ergibt, versuchen, einen Plan
auszudenken, wie wir deine Probleme lösen." Marusch betonte das
Pronomen. "Du möchtest deinen Vater retten, und deshalb möchtest
du weiter. Ich möchte nur, dass du nicht in Schwierigkeiten kommst. Wenn
wir dazu auf einer Reiseinsel um die Welt dümpeln, klingt das
für mich weniger verkehrt als für dich. Folglich werde ich dich
sicher nicht zurücklassen."

Lilið nickte. Ihr Atem zitterte, stellte sie fest. Was
Marusch da sagte, ergab durchaus Sinn. Es beruhigte sie auch. Nur
der Gedanke, dass sie ihren Vater vielleicht nicht retten können
würde, löste ein leicht panisches Gefühl in ihr aus. Sie musste
es schaffen, trotz Fieber zu segeln. Trotz erhöhter
Temperatur.

"Setz einen Hut auf und trink etwas.", orderte Marusch an. "Ich
kann leider nicht das ganze Boot alleine bedienen. Aber ich kann
dir noch die Großschot abnehmen."

Lilið schüttelte den Kopf. "Die ist das kleinste Problem, und
es hilft mir, etwas zu haben, worauf ich mich konzentrieren kann."

"Dachte ich mir." Marusch lächelte.

Als der unbeständige Wind es wieder zuließ, suchte sie für Lilið einen Hut und
die Trinkflasche aus dem Gepäck. Das Wasser half tatsächlich ein
wenig, aber sie hatte Angst, so viel zu trinken, dass ihre
Vorräte dann zu schnell zur Neige gehen würden. Marusch drängte
sie trotzdem dazu.

---

Das Anlegemanöver verlief etwas holprig. Lilið hatte sich
eigentlich zugetraut, es hinzubekommen, im Wasser zu stehen und
das Boot zu halten, aber das Seegras, das sich um ihren Knöchel
schlang, irritierte sie überraschend so sehr, dass sie stolperte
und ganz ins Wasser eintauchte.

Marusch nahm ihr alles ab, was sie konnte, aber die Ormorane
war für sie alleine zu schwer. Lilið musste noch einmal anpacken, als
sie sie den grauen Strand hinaufschoben. Immerhin war es weicherer
Strand als am Tag zuvor, sodass sie sie nicht anheben
mussten. Und es nieselte höchstens noch.

"Ruh dich aus.", orderte Marusch an. "Ich hole Wasser und
kümmere mich hinterher auch um das Abendessen."

"Du holst immer Wasser.", nuschelte Lilið. "Ich frage mich
immer, ob du dabei heimlich masturbierst oder so. Weil du
es nicht in meiner Nähe kannst, aber ich dich dauernd
errege." Das waren Gedanken, die sie schon öfter gehabt
hatte, aber heute war sie so unbeherrscht, dass sie sie
aussprach und sich nicht einmal schämte.

Marusch kicherte. "Heute käme mir dergleichen definitiv nicht in
den Sinn.", versicherte sie. "Ich habe da ein Lilið zu versorgen."

'Ein Lilið'. Das fühlte sich schön an, fand Lilið.

In der Wartezeit breitete Lilið die Decke aus, aber hielt sich
daran, nicht das Abendessen vorzubereiten. Stattdessen rollte
sie die Karte aus, die immerhin anders als das Buch brauchbar
wetterbeständig war. Das Buch würde sie heute Abend in der
dichten Tasche belassen und nicht herausrücken.

Sie schaffte es, trotz des matischigen Gefühls im Kopf, sich in
den Denkmodus zu versetzen, der für das Navigieren notwendig
war. Sie merkte, dass dieser eine lange Nachmittag mit Heelem
viel Übungseffekt hinterlassen hatte. Sie fand überraschend
viele Alternativrouten. Bis sie feststellte, dass sie von
der falschen Ausgangsposition der Karte aus gestartet hatte: Sie
hatte sie ja in der Nacht zuvor nicht um einen Tag
weitergestellt. Als sie das nachgeholt hatte, fiel
ihr das Navigieren dann doch ziemlich schwer. Sie ärgerte
sich, nicht nur über den Fehler, sondern auch, weil sie ihr Gefühl, dass
alles viel zu leicht ginge, ignoriert hatte.

Marusch schüttelte den Kopf über sie, als sie mit Wasser
zurückkam. "Was machst du da?", fragte sie.

"Ich navigiere." Lilið versuchte, ahnungslos ob der Ungläubigkeit
und möglichst unschuldig dabei zu klingen. "Oder ich versuche es zumindest."

"Und wo soll ich das Abendessen aufbauen, Gnädigstes?", fragte
Marusch.

Lilið kicherte. "Auf deinen Schoß?", fragte sie. "Dann
kannst du mich beim Essen auch besser kraulen."

"So ist das also!" Das Schmunzeln in Maruschs Stimme war schon
wieder nicht zu überhören. Aber wenn es eine Doppeldeutigkeit
war, erschloss sie sich Lilið nicht.

Statt Liliðs Vorschlag nachzukommen, nötigte Marusch sie zunächst
noch mehr zu trinken. Nicht, ohne sich zu vergewissern, ob es ihr
vorhin auch wirklich gut bekommen war und trinken das richtige
Mittel. Anschließend machte sie mit ihrer Feuermagie eine Ausnahme,
um in einem überraschend gut dazu geeineten Pützeimer eine Suppe
zuzubereiten. Sie roch ziemlich gut. Wieder erinnerte sie
Lilið an ihre Mutter. Es roch nicht so, wie Lilið es gewohnt
war, aber sie verband gute Küchengerüche einfach mit keiner
Person mehr als mit ihrer Mutter.

"Das ist die Gelegenheit für dich, um mich zu vergiften.", merkte
sie zwischen zwei Kartendurchgängen an. Allmählich nahm sie das
Navigieren nicht mehr als frustrierend, sondern wieder als normal kompliziert
wahr.

"Gute Idee!" Marusch gluckste. "Ich werde es mit Wasser versuchen. Habe
gehört, das ist so ein typisches Gift, aber ich kenne mich auch nicht
allzu sehr aus."

"Du musst noch viel lernen!", sagte Lilið theatralisch. "Ich hoffe, du
kannst besser kochen als vergiften." Sie verkniff sich den Kommentar,
dass das wohl hieß, dass Vergiften nicht Maruschs Mordmethode in
der Vergangenheit gewesen sein würde.

---

Die Suppe tat so gut, wie Lilið es nicht für möglich gehalten hatte. Sie
schmeckte nicht ganz so gut, wie sie roch, aber um Längen besser als
ihr Abendessen gestern. Solide, würde Lilið sie beschreiben. Und
sie fühlte sich genau richtig im Magen an.

Lilið ließ sich auch dazu drängen, die Karte einzupacken und stattdessen
früh schlafen zu gehen, als sie gerade eine Alternativroute
gefunden hatte. Das musste erst einmal reichen. Aber sie belastete
durchaus der Gedanke, dass ihre derzeitige Route genau siebzehn Tage
brauchte, also nichts mehr schief gehen durfte, wenn sie ihr Zertifikat
haben wollte.

---

Sie wachte kurz nach Sonnenaufgang mit drei klaren Gedanken auf. Der
erste war angenehm. Sie fühlte sich viel besser. Keine Hitzewellen
brachten ihren Körper außer Fassung und ihre Gedanken waren sortiert
und greifbar. Marusch hatte sie gut gepflegt. Vielleicht hatte sie
sich auch doch ausreichend geschont. Wahrscheinlicher war, fiel ihr
ein, dass es kein Infekt, sondern
bloß sehr starke Erschöpfung und vielleicht ein Sonnenstich
gewesen war.

Der zweite Gedanke war unangenehm. Sie hatte nicht nur vergessen, die
Karte auf den neuen Tag einzustellen, was sie dann ja nachgeholt
hatte, sie hatte den Tag auch vergessen
mitzuzählen. Das hieß, dass sie mit der aktuell geplanten Route einen
Tag zu spät für ihr Zertifikat wären. Es war kein Weltuntergang. Bei
dem Gedanken musste sie grinsen, weil sie durch Marusch gelernt hatte,
auch dem Weltuntergang nicht völlig abgeneigt gegenüberzustehen. Es
käme drauf an, wie er dann von statten ginge.

Der dritte Gedanke baute auf dem zweiten auf und war ein
Hoffnungsschimmer: Ihr Kopf hatte in der Nacht am Navigationsproblem
weiter gearbeitet und hatte ihr eine Route vorgeschlagen, die funktionieren
müsste.

Der Himmel war immer noch grau, als sie das Zelt zum
Navigieren verlassen wollte, aber es regnete nicht mehr.

"Bleib noch ein bisschen liegen.", nuschelte Marusch.

"Bleib doch selber ein bisschen liegen!", antwortete sie, in einem
Versuch, schlagfertig zu sein.

Marusch richtete sich entgegen ihres Vorschlags auf. "Du klingst
besser!", stellte sie fest. "Antrag auf Aufstehen genehmigt."

"Ich navigiere noch ein bisschen, bevor es losgeht.", antwortete
sie, dieses Mal ernster. "Du kannst wirklich noch etwas liegen bleiben. Oder
das Masturbieren von gestern nachholen, wenn du willst."

Marusch grinste sie an. War der Blick vielsagend? "Du denkst wohl,
ich wäre permanent geil!"

Jedenfalls war der Blick anziehend, fand Lilið, verlockte dazu, Marusch
ins Gesicht zu küssen. Lilið ließ sich also doch noch einmal vom
Navigieren abhalten und tat es. Maruschs Körper in ihren Armen wurde dabei
weich und ihr entfleuchten, vielleicht ausversehen, sachte
fiepende, verlangende Geräusche. Lilið gefiel das. Sie verteilte
ihre Knie rechts und links von Maruschs Becken, legte ihr ihre
Hände in den Nacken und drückte sie, ohne mit ihren Lippen von
Maruschs Gesicht abzulassen, auf die Unterlage. Ihr war schon öfter
aufgefallen, dass es Marusch sehr gut gefiel, unten zu liegen. Aber
anders als sonst, ließ Lilið sie heute dort einfach liegen.

"Ja, das denke ich durchaus.", beantwortete sie die
Frage. Aber bevor sie den Zelteingang
hinter sich schloss, fügte sich leicht verschämt hinzu: "Ich ja
nicht weniger."

Was hatte sie sich dabei gedacht? Wie sollte sie sich denn jetzt
auf die Karte konzentrieren, wenn sie in Gedanken Marusch weiter
mit Liebkosungen quälte? Sie schaffte es, mühsam den Gedanken
an sie zu verdrängen, indem sie sich klarmachte, dass sie nach
dem Navigieren ja wieder ins Zelt gehen könnte.

Falls Marusch masturbierte, tat sie es so leise, dass Lilið davon
nichts mitbekam. Vielleicht aus Rücksicht. Vielleicht sollte Lilið
irgendwann Mal nachfragen. Und vielleicht wäre es jetzt, wo sie
herausgefunden hatte, dass sie keinen Sex haben wollte, bei dem ihr
andere Personen irgendetwas vaginal einführten, auch eher recht, wenn
Marusch es mal in ihrer Gegenwart täte. Sie überlegte sogar, dass
sie es sich vorstellen könnte, selbst wenn sie es nicht sehr
erotisch finden würde, Marusch mit der Hand zu befriedigen.

Nein, die Gedanken waren zwar interessant, aber jetzt überhaupt
nicht hilfreich.

Sie atmete tief durch und bearbeitete die Karte. Wie sie es
sich noch im Halbschlaf gedacht hatte, fand sie eine Route, mit
der sie rechtzeitig da wären. Es war keine schöne Route, aber
eine mögliche. Anschließend versuchte sie, Ausweichrouten für
diese zu finden, oder doch noch eine bessere, aber was sie
herausfand, ernüchterte sie.

Sie legte gerade das Kartensteinchen endgültig bei Seite, als
Marusch das Zelt verließ. Sie wirkte ausgeruht und, wie
immer, charmant, fand Lilið.

"Du bist also sadistisch.", hielt Marusch fest.

"Du magst es doch.", erwiderte Lilið frech. Aber schnell fügte
sie hinzu: "Das ist nicht gut von meiner Seite, das so zu
sagen. Es klingt wie dieser Standardspruch. Ich habe dir Gewalt
angetan, aber das ist schon in Ordnung so, denn dein Körper hat
ja reagiert. So soll das nicht gemeint sein. Wie fühlt es sich
für dich an?"

Marusch setzte sich neben sie auf die Decke und strich ihr
über den Kopf. "Ich genieße unser ganzes Spiel in vollen
Zügen.", antwortete sie sanft. "Ich liebe es, wenn du mich
kriegst, wenn du plötzlich Kontrolle über mich hast und ich
sie verliere. Und umgekehrt mag ich es auch." Einer
ihrer Finger rann bei ihren Worten über die Stelle an Liliðs
Hals, auf die Lilið besonders empfindlich reagierte, wenn
Marusch hineinbiss.

Sie atmete einen Moment hastiger. "Ja.", flüsterte sie. Sie
überlegte, ob sie ein 'bitte' hinzuflehen sollte, aber Marusch
ließ sie einfach los und sah auf die Karte.

"Hast du etwas herausfinden können?", fragte sie.

"Dass es eine Route gibt, eine einzige, mit der wir das Zertifikat
rechtzeitig erreichen könnten.", murrte sie. "Und zwar eine, bei
der unser vorletzter Abschnitt eine Tour von Sonnenauf- bis
Sonnenuntergang ist. Es ist gleichzeitig auch noch der Abschnitt, bei dem wir
die Seenplatten kreuzen, also mit kräftigen Strömungen zu kämpfen
haben und viel Konzentration brauchen werden."

"Und die Route müssen wir schaffen, weil wir sonst ein Jahr unterwegs
wären?", fragte Marusch.

Lilið schüttelte den Kopf. "In dem Fall würde ich sie gar nicht erst
zählen, das wäre mir zu riskant. Aber alle Ausweichrouten brauchen
mindestens acht Tage länger."

Marusch nickte. "Ich verstehe, dass dich das stresst."

"Was mich viel mehr stresst, ist, das es einen Strauß dicht
beieinander liegender schöne Routen gibt, wenn wir uns heute für einen
anderen Zweig entscheiden.", sagte sie. "Aber die kürzeste davon
braucht einen Tag zu lang."

"Du bist in Nautik-Sprech gefallen.", informierte Marusch. "Aber
ich glaube, ich kann dir trotzdem folgen."

"Eigentlich nicht Nautik-Sprech.", gab Lilið zu. "Mehr mein eigener. Ich
finde die Worte intuitiv passend. Jedenfalls ist die Auswahl: Einen
Tag zu lang, dafür aber gemütlicher und mit viel Sicherheit, dass es
nicht viel über den Zeitrahmen hinaus geht? Oder mit recht hoher
Wahrscheinlichkeit pünktlich, aber anstrengend, und wenn etwas schief
geht, dann mit viel Verzug." Murmlend fügte sie hinzu: "Ich hoffe,
ich habe mich nicht verrechnet."

Marusch zuckte mit den Schultern. "Variante zwei."

"Das sagst du so ohne zu zögern?", fragte Lilið. War das Maruschs
Risikobereitschaft zuzuschreiben?

"Du bist mein Nautika.", antwortete Marusch. "Ich kenne dich inzwischen
ein kleines bisschen. Du willst Variante zwei. Du traust uns dazu. Und wenn
etwas schief geht, sind wir immer noch bloß drei Wochen unterwegs. Selbst
das sollte passen, ohne dass dein Vater in allzu schlimme Gefahr gerät."

Lilið nickte. "Du hast schon recht.", murmelte sie. "Ich möchte das
gern, und ich denke, es ist realistisch genug, dass das klappt."

---

Das Wetter an diesem Tag war dem des Vortages ziemlich ähnlich, als sie
wieder in See stachen, nur mit dem Unterschied, dass Lilið es heute
genießen konnte. Sie fühlte sich wach, etwas aufgeregt und gesund. Ihr
Körper reagierte wieder auf die Böen und die Veränderungen des Windes, als
wäre er eins mit der Ormorane. Sie spürte die Lebensfreude, die es ihr gab,
dass Marusch und sie inzwischen so eingespielt waren, dass ihre Bewegungen perfekt
aufeinander abgestimmt waren.

Zum Nachmittag hin klarte der Himmel auf. Die Sonne brannte, aber die
Insel, die sie bald erreichten, bot ausreichend Schatten. Es gab einen
kleinen Wald nahe der Küste.

Wieder holte Marusch Wasser, während Lilið das Abendessen vorbereitete. Marusch
ließ sich etwas mehr Zeit, was Lilið ein Schmunzeln entlockte, weil
sie sich vorstellte, wie Marusch irgendwo auf der anderen Seite der
Insel heimlich masturbierte. Aber eigentlich wusste sie es nicht.
Vielleicht nutzte Marusch die Zeit einfach für etwas Privatsphäre.

Sie überbrückte die Wartezeit mit ihren Übungen, Steine genauer zu
verstehen. Sie suchte sich vom schmalen Strand vor dem Waldboden
zwei aus und hielt sie zunächst einfach mit geschlossenen
Augen in der Hand. Sie dachte über ihre Beschaffenheit nach, wie
Marusch es ihr erklärt hatte, nicht nur mit ihrem Gespür,
sondern auch mit bewussten Gedanken über die Physik. Bloß
erfühlen konnte sie sie prinzipiell schon lange. Das hatten sie in der Grundschule
gelernt. Aber sie versuchte, nun nicht nur die Bestandteile zu
erfühlen, sondern auch, wie diese zusammengefügt waren, welche
Bindungen zwischen ihnen bestanden, welche Schwingungen es
gab. Und dann, als sich für sie eine neue Form erschloss, die
passen könnte, schmiss sie Maruschs Vorschlag über Bord, erst
einmal etwas Einfaches zu versuchen, sondern faltete
den Stein in einer fließenden Bewegung in jene neue Form.

Sie öffnete die Augen, besah sich das Ergebnis und ließ
los. Der Stein behielt seine Seesternform. Ein Glücksgefühl
durchströmte sie. Die Maserung darauf hatte sich in ihrem
Gedankenuniversum gut angefühlt, und nicht einmal sie sprang zurück in
ihre alte Form.

Sie legte den Seestern in die Mitte auf die Decke und probierte, den
zweiten Stein zu verstehen. Er fühlte sich auch gut in der Hand
an, aber anders. Er
war glatt, hatte einige gekrümmte Rillen, wo vielleicht mal etwas
von ihm abgebrochen war. Bei diesem Stein hatte Lilið weniger
Erfolg. Sie schaffte es schon, ihn zu falten, aber er sprang
immer wieder zurück in seine Steinform, wenn sie losließ.

Als Marusch zurückkehrte, auf der Suche nach Wasser wieder
erfolgreich, und sie zusammen aßen, konnte Lilið sich trotzdem
die ganze Zeit ein glückliches Lächeln nicht verkneifen. Sie
hatte etwas verstanden, wenn auch noch nicht vollständig. Aber
es war ein Durchbruch.

Sie störte es dieses Mal auch nicht, dass Marusch keinen Kommentar
zum Seestern in der Mitte zwischen ihnen abgab. Aber als sie fertig gegessen hatten,
hob Marusch ihn doch auf, besah ihn sich von allen Seiten und
strich darüber. Lilið holte das Buch aus ihrer Jacke, um es
gegen den Stern zu tauschen, wenn Marusch soweit wäre.

"Willst du irgendeinen besserwisserischen Kommentar machen?", fragte
Lilið. "Fühl dich frei! Ich würde ihn dann gern bald hören, damit
ich es hinter mir habe."

Marusch schüttelte den Kopf. "Mir fallen zwar viele Dinge ein, die
ich dir noch erklären könnte, aber das mache ich morgen auf der
Fahrt.", sagte sie. "Du hast deine eigene Art, zu lernen, und das
wunderschön zu beobachten. Ich finde es ein starkes Stück, dass du Zwischenschritte
einfach überspringst. Dir fällt es leichter, etwas in einem riesigen
Schritt ganz zu verstehen, als in kleinen Schritten zuvor schon
einmal ein unpräzises, schwammiges Zwischenverständnis zu haben."

Lilið nickte und grinste. "Das meinte meine Mutter auch immer."

Marusch reichte ihr den Seestern und Lilið gab ihr das Buch, aber
als Maruschs Finger sich um es schlossen, hielt Lilið es plötzlich
doch fest. "Da ist Metall drin.", sagte sie.

Es war nicht viel Metall. Sie
erwartete auch kein Metall in einem Buch. Vielleicht hätte sie es
sonst viel eher bemerkt. Aber nun, in einem besonderen Moment der
Wachheit, spürte sie es ganz deutlich.

Marusch ließ das Buch los. "Hast du schonmal die Seiten verstanden?", fragte
sie.

"Schon.", sagte Lilið. "Aber da habe ich kein Metall erwartet. Und darüber,
ob sie zusätzlich zu den merkwürdigen, unkoordinierten Zeichen mit
einer Schrift beschrieben sind, bei der mit unsichtbarer Tinte oder so etwas
geschrieben worden ist, sprachen wir ja schon einmal. Da meintest du, es
wäre schon durchleuchtet worden und alles. Und die Maserung untersucht."

"Magst du die Seiten anfassen?", bat Marusch schlicht.

Lilið nickte. Sie schloss die Augen dafür, öffnete das Buch und
legte die Hand vorsichtig auf das Papier. Es war an sich normales
Papier. Aber das Metall war nicht zufällig darin. Es waren viele
kleine Metallpartikelchen, nicht auf das Papier aufgetragen, sondern
unter seine Oberfläche eingearbeitet. Und sie ergaben viel klarere
Muster als jene sichtbaren Zeichen, die sie immer an Dreck erinnert hatten.
