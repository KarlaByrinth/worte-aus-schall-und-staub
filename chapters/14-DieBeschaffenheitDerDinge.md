Die Beschaffenheit der Dinge
============================

*CN: Beißen, Verletzung, Erotik, sexuelle Anspielungen.*

Bisher verlief die Reise so gut, dass Lilið es etwas gruselte. Es
fühlte sich nicht richtig an, so lange am Stück guten, passenden
Wind zu haben und jeweils schon zwei bis drei Stunden früher als
geplant die Inseln zu erreichen. Lilið schaute sich abends stets
noch einmal die Karte an und brachte sie auf den neuesten Stand. Auch
dabei war ihr bisher keine Ungenauigkeit unterlaufen.

Irgendwann würde zum Ausgleich irgendetwas gewaltig schief gehen,
fürchtete Lilið. Aber darüber lohnte es nicht, sich heute noch
Gedanken zu machen. Sie hatte sich in aller Ausführlichkeit die
Route für morgen angesehen, rollte die Karte ein und verpackte sie
wieder gründlich. Und dann hatte sie nichts weiter zu tun.

Es war der vierte Tag ihrer Reise. Am vorherigen Tag hatten sie
immerhin heimlich einen Abstecher in ein Hafenstädtchen gemacht, aber
diese Reiseinsel war wieder unbewohnt. Sie hatten bereits gegessen. Und
Marusch beschäftigte sich, wie jeden Abend mit dem Buch.

Anfangs hatte Lilið ihr dabei noch -- erlaubter Weise --
länger über die Schulter gesehen. Sie
tat es auch jetzt immer Mal wieder. Marusch hatte einige Notitzzettel
neben sich auf der Decke ausgebreitet und mit Steinen beschwert, damit
der Wind sie nicht davonwehte. Sie zeichnete Zeichen aus dem Buch ab,
versuchte, sie zu kategorisieren und dann zu zählen, wieviele jeweils
in eine Kategorie fielen. Sie meinte, wenn es eine Substitutionsschrift
wäre, wäre es einfach. Eine Substitutionsschrift war eine Kodierung,
bei der einfach jeder Buchstabe einer Originalschrift durch ein anderes
Zeichen ersetzt wurde. Aber sie hielt es für unwahrscheinlich, dass es
einfach das wäre. Sonst wäre das Buch schon längst bei seinem Fund
vor etwa 45 Jahren dekodiert worden. Trotzdem probierte Marusch es
zunächst auf diese Weise, um sich mit den Zeichen und ihrer Anordnung
vertraut zu machen. Eine Schwierigkeit bestand auch darin, dass sie
allebeide nicht einmal einordnen konnten, ob es von links nach rechts
oder andersrum, oder von oben nach unten oder andersrum geschrieben
worden war.

Lilið reizte das Buch durchaus auch, aber sie verstand nicht halb so
viel von Kodierung wie Marusch. Marusch erklärte ihr, wann immer
Lilið wollte, was sie gerade tat oder was sie wusste. Sie meinte,
es lenkte nicht ab. Im Gegenteil, sie würde manchmal sogar auf
weitere Ideen kommen, wenn sie etwas erklärte, und das war auch
gar nicht so selten der Fall. Dann hatte sie mitten in einer Erklärung
von einem Gedanken und verstummte plötzlich oder wechselte mitten
im Satz vin angefangener Erklärung zur Theorie in eigene
Überlegungen zum vorliegenden Material.

Aber bald verlor Lilið den Faden und interessanterweise auch die
Geduld. Das war eigentlich schon am zweiten Abend eingetreten.

Gegen die Langeweile hatte versucht, sich einfach besonders gründlich auszuruhen. Aber
spätestens heute war sie einfach viel zu wach und zu hibbelig dafür. Was
sollte sie tun? Sandburgen bauen? Die Revolution planen? (Aber dazu
wäre sie noch weniger in der Lage gewesen, als Marusch bei ihrem
Vorgehen zu folgen.)

"Darf ich einen Zettel von dir haben?", fragte sie Marusch schließlich.

"Brauchst du ihn zum Navigieren?", frage Marusch, ohne aufzusehen.

"Nein.", sagte Lilið. "Dafür habe ich ein Kursbuch. Ich überlege, ob
ich ein bisschen basteln mag."

Marusch sah auf und ihr nachdenklich ins Gesicht. "Einen pro Tag.", sagte
sie. "Klingt das in Ordnung?"

"Es klingt, als ob du mich wie ein kleines Kind behandeln würdest.", gab
Lilið offen zu.

"Mist, das war nicht der Plan." Marusch sah angenehm zerknittert darüber
aus. "Ich brauche eben selbst vermutlich recht viel Papier. Und basteln
ist oft sehr materiallastig. Korrigier mich gern, wenn ich da falsch
liege."

Lilið nickte. "Ich habe nach einem Zettel gefragt.", erinnerte sie.

Marusch lächelte und nickte. "Da spricht nichts gegen."

Lilið zupfte ein Blatt Papier aus dem beschwerten Stapel hervor. Marusch
machte sich weitere sehr kleine, mit feiner Feder geschriebene
Notizen auf einem Durcheinander an Zetteln daneben, die sie schon benutzt hatte.

Schon während Lilið das Papier berührte, war sie halb in ihrem
Gedankenuniversum verschwunden, in dem nur Falten existierte. Sie
fühlte die Beschaffenheit des Papiers, wieviel Grauwollenanteil
darin war und wieviel Holzphaser. Sie nahm auch den Klebstoff darin
wahr, der alles zusammenhielt. Sie fühlte die Orientierung der
ultradünnen Spähne, und wo entlang sich das Papier gut falten
ließe. Wenn sie ein Material wirklich verstand, dann Papier. Im
Gegensatz zu anderen Materialien, die sie faltete, sprang es
auch nicht in die originale Form zurück, wenn sie es nach dem
Falten mental losließ.

Sie nahm den Bogen ganz in die Hand und strich mit geschlossenen
Augen darüber, noch ohne etwas zu falten. Sie fühlte noch bewusster
in das Papier hinein, mit ihren Gedanken Linien darin entlangfahrend,
die sich in einer komplexen Faltung nicht so sehr wehren würden. Papier
ließ sich nicht beliebig oft falten, zumindest nicht so richtig, aber
die Grenze, wie oft es sich falten ließ, war an verschiedenen Stellen
des Papiers verschieden.

Lilið überlegte sich eine Form, in die das Papier besonders
gut springen könnte und öffnete die Augen wieder. "Möchtest du
zusehen?", fragte Lilið.

Marusch blickte auf. "Wieviel Zeit brauchst du?", fragte sie.

"Ein paar Sekunden.", sagte Lilið. Diese Person ließ sich wirklich
nicht leicht ablenken.

Marusch lächelte. "Die habe ich immer.", antwortete sie.

Lilið hatte ihre Faltkunst noch nie freiwillig anderen Leuten
vorgeführt. Sie wusste, was sie konnte, weil sie es oft allein
für sich gemacht hatte. Aber sie war ein bisschen aufgeregt, als
sie nun entspannt einatmete und beim Ausatmen mit den Fingern einmal
unter dem Papier entlangstrich, wobei es sich ihrem Willen nach
in einen sehr detaillierten Awanendrachen verwandelte. Eine
majestätische Drachenart, der sie sich nicht so gern näherte (zumindest,
wenn sie nicht aus Papier waren), weil
sie üble Verletzungen hinterlassen konnten, wenn sie
sich gestört fühlten und zubissen.

Zu Liliðs Enttäuschung nickte Marusch bloß als hätte sie so etwas
schon tausend Mal gesehen und senkte den Blick wieder auf das
Buch. Es war durchaus eine Enttäuschung, die unangenehm
stark an Liliðs Nerven zupfte. Sie versuchte, sich klar zu machen,
dass Marusch ihr keine Aufmerksamkeit schuldete. Kein Lob,
keine Anerkennung. Dafür, dass sie ein Stück Papier verschwendet
hatte!

Für einen Augenblick hätte Lilið den Awanendrachen gern angezündet. Wenn
sie denn Feuermagie beherrscht hätte. Papier anzünden war
eigentlich keine so fortgeschrittene Feuermagie. Das hatte sie
zu Schulzeiten mit Mühe und Not schon einmal hinbekommen. Aber
sie hatte es nur für die Prüfung parat gehabt, in der sie es
gebraucht hatte, und anschließend wieder vergessen.

Sie merkte, wie ihr Tränen kamen und ärgerte sich sehr darüber. Warum
wollte sie eine Leistung anerkannt bekommen? Von Marusch. Sie war
zu Schulzeiten nicht darauf ausgewesen. Aber irgendein Teil in
ihr hatte sich immer vorgestellt, wie sie ihre Faltkunst irgendwann
einmal außerhalb des Schulkontexts einer Person vorstellen könnte, die
darauf stolz wäre. Sie hatte sich auch irgendwie gut gefühlt,
als Frau Wolle bei der Neueinschätzung ihres Skorems erkannt
hatte, dass sie falten konnte, und dann auf Basis einer viel einfacheren
Faltung als dieses Drachens entschieden hatte, dass sie eine Skorem
von irgendwas hohem hatte. Lilið hatte die genaue Zahl vergessen
und das riss sie schließlich aus ihrem Frust heraus, ließ sie lautlos
kichern.

Es war eine schlechte Situation gewesen. Sie hatte nicht genießen
können, dass eine Person stolz auf sie war, weil es darum gegangen
war, ihr Freiheiten zu nehmen. Die Situation jetzt mit Marusch hatte sie für
geeigneter gehalten.

Sie legte den Awanendrachen neben dem ansonsten unbenutzten
Papierstapel ab, beschwerte einen Flügel mit einem Stein und
bemerkte erst jetzt, dass Marusch sie beobachtete. Seit wann?

"Du bist traurig.", stellte sie fest. "Oder frustriert."

"Oder beides.", ergänzte Lilið. "Aber das ist mein Gefühl."

"Heißt das, dass du nicht drüber reden willst?", fragte Marusch.

"Du möchtest doch viel lieber an deinem Buch arbeiten!" Lilið
konnte nicht vermeiden, dass Aggression in ihre Stimme geriet. "Tut
mir leid. Ich habe gerade wenig Selbstkontrolle. Das hast du
nicht verdient. Du darfst natürlich am Buch arbeiten."

"Was mich mehr stört, als dein Ton, ist, dass du beschließt,
was ich angeblich will.", sagte Marusch, ganz im Gegensatz
zu Lilið, völlig sachlich und gelassen. "Wenn
es dir nicht gut geht und du reden möchtest, -- was du nicht
musst, aber wenn --, dann ist mir das wichtiger. Und ich
möchte, dass du das mir überlässt, was mir wichtiger ist."

Lilið schluckte, versuchte durch diese alberne Handlung vielleicht
Tränen runterzuschlucken, die aber aus ihren Augen traten und
nicht aus ihrem Mund. Diese Vorstellung brachte sie unpassenderweise
zeitgleich zum Grinsen, nur für einen Moment.

Marusch sah sie ein paar Augenblicke weiter an, aber wandte dann
respektvoll den Blick ab, als Lilið sich mit dem Ärmel die Augen
trocken tupfte. Maruschs Blick fiel dabei wieder auf
den Awanendrachen. Sie bewegte die Hand in seine Richtung und
fragte: "Darf ich?"

Lilið nickte. "Aber fühl dich nicht gezwungen, ihn zu bewundern
oder so etwas.", sagte sie.

"Ging es darum?", fragte Marusch. Sie schob vorsichtig den
Stein vom Flügel und hob das filigrange Geschöpf an, um es
genau zu betrachten. "Dass du gern mehr Aufmerksamkeit
von mir gehabt hättest?"

"Schon.", antwortete Lilið ehrlich. "Aber du schuldest mir nichts. Du
hast nichts falsch gemacht. Dass ich gern Anerkennung gehabt
hätte, begründet sich in einer
beschissenen Schulzeit, in der nichts von meinen Fähigkeiten gewürdigt
wurde, weil sie nicht ins Muster passten, aber ich eigentlich schon
wusste, dass sie gut sind! Dafür kannst du nichts. Damit hast
du nichts zu tun."

Marusch lächelte. "Sie sind gut!", betonte sie. Dann zuckte sie
mit den Schultern. "Ich interessiere mich nicht so sehr für Leistung,
glaube ich. Ich habe dazu einen sehr anderen Bezug. Aber um das
zu erklären, müsste ich dir mehr Hintergrund zu mir erzählen, den
ich noch nicht teilen mag."

"Noch nicht?", fragte Lilið überrascht.

"Noch nicht.", bestätigte Marusch mit einem sanften Lächeln, das
sehr nach Innen gerichtet wirkte. "Ich habe mich bei dir gut aufgehoben
gefühlt, als ich mich vor dir mental nackt ausgezogen habe. Ich kann
mir vorstellen, ich dir doch irgendwann alles sagen will."

Ein warmes Gefühl durchfloss Lilið, dass sie für all den Frust, den
sie eben noch gefühlt hatte, entschädigte. "Marusch.", sagte sie
leise. "Du musst mir nichts erzählen. Aber dass du dich bei mir
gut aufgehoben gefühlt hast, berührt mich sehr." Warum hatte
sie wieder den unpassenden Drang, Marusch in die Halsbeuge zu
küssen und zart zu beißen? In den letzten Nächten hatte sie herausgefunden, wie
sie es tun musste, damit sich Marusch in ein Bündel flehend fliepsenden
Genuss verwandelte.

"Du möchtest mich küssen.", las Marusch richtig von ihrem Gesicht
oder ihrer Körperhaltung ab.

"Aber du möchtest lieber das Buch bearbeiten.", hielt Lilið fest, weil
das in den vergangenen Tagen vor Einbruch der Dunkelheit immer Maruschs
Antwort gewesen war. Aber heute fügte sie hinzu: "Es tut mir leid,
ich habe wieder quasi beschlossen, was du willst."

"Lilið, du bräuchtest mich vermutlich nur einmal falsch berühren
oder anatmen und mein Willen würde brechen, ich würde mich dir
hingeben.", verkündete Marusch.

Richtig, an den richtigen Stellen den Atem über ihre Haut
streichen lassen, war auch ein gutes Mittel gewesen. Liliðs
Atem stolperte bei den Worten. Manchmal waren Worte sogar noch erotischer,
als was sie beschrieben. "Möchtest du?", hauchte sie.

Marusch schien einen Moment hin- und hergerissen, dann schüttelte
sie den Kopf. "Wenn du das brauchst, damit es dir wieder besser
geht, ja. Sonst, wie immer, wenn es zu dunkel zum Schreiben
ist. Bitte.", sagte sie, vielleicht fast ein bisschen flehend. "Aber
ich würde gern was zum Thema Falten loswerden, wenn du magst."

Lilið nickte zögerlich. Irgendetwas an dieser Ankündigung fühlte
sich so an, als würde es nicht einfach ein Lob für ihren Awanendrachen
sein.

"Der Awanendrache ist schön." Maruschs Stimme klang dabei zu
sachlich, zu unemotional, als berührte es Marusch gar nicht. "Er
ist nicht sehr individuell. Du hast das Papier verstanden, und
das daraus gefaltet, was es so an klassischen, wenn auch komplexen
Formen gibt, die genau da hineinpasst, vermute ich?"

Lilið nickte. Sie hatte sich mal ein Grundlagenbastelbuch für
Papier geliehen, aus dem sie alle Formen auswendig gelernt
hatte. Es war Marusch also sozusagen nicht besonders genug. Sie
hatte es doch gewusst, dass die Sache einen Haken hatte.

"Mich hätte vermutlich mehr berührt, wenn du etwas gefaltet hättest,
was eine persönlichere Note hat, mehr du bist, sogar dann, wenn es
schlecht gefaltet wäre.", fuhr Marusch fort. "Aber das ist Geschmacksache. Das ist halt,
wie ich Kunst wahrnehme. Sie muss für mich nicht irgendwelche Qualitätsmerkmale
aufweisen, die von irgendwelchen alten Magiestudierten festgelegt
worden sind, sondern ich mag das Persönliche darin."

Lilið senkte den Blick. "Das klingt eigentlich nach viel schöneren
Bewertungskriterien. Es klingt so, als könnten in einer Welt, in
der Leute so bewerten wie du, auch Menschen schöne Reaktionen erhalten,
die überhaupt nicht Falten können. Oder andere Arten von Magie nicht
beherrschen.", schloss Lilið sofort. "Und zwar Reaktionen, die
nicht automatisch weniger wert ist." Sie
überlegte einen Moment. "Aber die andere Bewertung brauchen wir
vielleicht schon noch, um Menschen zu finden, die Arbeit sinnvoll verrichten
zu können. Sie könnte nur unemotionaler sein."

"Das ist ein komplexes Thema. Das würde mich gerade auch
zu viel ablenken, aber wenn du möchtest, können wir darüber
ein anderes Mal nachdenken.", sagte Marusch. "Ich wollte gern noch
auf etwas anderes hinaus: Was ich von dir schon an
Fähigkeiten erlebt habe, was Falten angeht, ist nämlich
viel beachtlicher als dieses Drachentier. Wenn du Anerkennung
für Leistung haben möchtest, dann mag ich dir sagen, dass
du mich gefaltet hast. Das war mächtige, angewandte Magie."

Lilið konnte nicht anders, als zu lächeln, obwohl sie sich
doch gerade vorgenommen hatte, diese Form von Bewertung
eher sachlich zu verstehen, während sie die individuelle
Bewertung, die Marusch erklärt hatte, als die wichtigere wahrnehmen
wollte, als die, die sie mit Emotionen verknüpfen wollte.

"Du musstest dafür die Luft anhalten?", fragte Marusch.

Lilið nickte. "Nicht ganz, aber fast.", sagte sie. "Ich
kann in gefalteten Formen nicht so gut atmen, weil ja die
Lunge nicht mehr an Ort und Stelle ist."

"Das ergibt keinen Sinn.", sagte Marusch.

Weiter sagte sie nichts, aber der Ton ließ wenig Freiraum
für Unsicherheit oder Widerspruch. "Aha.", sagte Lilið
im selben Tonfall.

"Wie klein ist das kleinste, in das du dich schon einmal
gefaltet hast?", fragte Marusch.

"Willst du mir jetzt Falten erklären?", fragte Lilið. Die
Kiebigkeit hatte sich wieder zurück in ihre Stimme verirrt.

Marusch senkte den Blick zurück zum Buch. "Ich habe keine
tiefgreifende Ahnung von Falten aber ein brauchbares
Verständnis der Dinge generell.", sagte sie. "Es tut mir
leid, ich möchte dich nicht belehren. Du hast miese Erfahrungen
in dem Zusammenhang gemacht. Das war unsensibel, das so
anzufangen."

Lilið sah ihr eine Weile dabei zu, wie sie Zeichen begutachtete,
mit Tabellen verglich, in einer Tabelle einen Strich ausradierte
und einen in eine andere Spalte hinzufügte. Das Papier war schon
rau vom Radieren. Ein Zettel flattertete besonders, weil
er der Wind eine Angriffsfläche gefunden hatte und nun daran
zerrte, aber der Stein hielt ihn kompromisslos fest.

In Lilið stritt ihr Ärger darüber, ihr eigenes
Fachgebiet erklärt zu bekommen gegen die Hoffnung, Marusch
könnte ihr brauchbares Wissen in die Hand geben. Letzteres
gewann.

"Erklär.", sagte sie. Ein Teil von ihr vermutete, dass
Marusch nun sagen würde, dass Lilið darauf auch warten müsse, bis
es dunkel wäre. Dann wäre sie richtig sauer gewesen.

Aber Marusch legte alles beiseite und richtete ihre volle
Aufmerksamkeit Lilið zu. "Ich wünschte Heelem wäre hier.", sagte
sie. "Er kann besser vermitteln als ich. Ich versuche mich, und
wenn ich mich irgendwie mies verhalte, hau mich."

Lilið erinnerte sich an die Stelle im Brief, in der Marusch
'räch dich gern an mir, wenn nicht' geschrieben hatte, um einen
Satz mit R anzufangen, und schmunzelte. "Du forderst mich des
Öfteren zu Gewalt gegen dich auf.", stellte sie fest. "Das
fängt an, verlockend zu werden. Aber schauen wir mal. Vielleicht
finden wir ja auch so zusammen."

Marusch grinste. "Jedenfalls wenn du Blutgefäße und alles
nicht mitfalten würdest, würdest du flach wie ein Tuch keine
zwei Sekunden überleben.", erklärte Marusch. "Wenn du dich
faltest, funktioniert dein Atemsystem und Blutkreislauf
nur anders als zuvor. Deshalb
fällt es dir schwer, dabei zu atmen. Du müsstest aber den Atem
der Faltung entsprechend anders leiten können."

"Woher weißt du das?", fragte Lilið, skeptisch und eine Spur
wütend. Sie hatte seit kleinauf gefaltet. "Kannst du auch
Falten?"

"Ich habe viele Bücher gelesen.", erklärte Marusch. Sie
machte eine bedeutungsschwere Geste auf das Buch, das sie
versuchte, zu dekodieren. "Es gibt ganze Kapitel, die erklären,
dass, -- verzeih mir, ich zitiere nur --, bei körpermodifizierender
Magie die falsche Atemübersetzung ein typischer Anfangsfehler
wäre."

Lilið hätte fast wütend geschrien, biss aber die Zähne zusammen. Dann
ließ sie es doch zu, dass ein wütender, aber vergleichbar leiser Laut
über ihre Lippen kam. "Ich mache das schon Jahre! Jahrzehnte fast!" Sie
war eigentlich noch keine zwei Jahrzehnte alt, und sie hatte wahrscheinlich
nicht zu ihrer Geburt angefangen. "Nicht ganz fast."

Marusch versuchte, ein Lächeln zu verbergen. "Du bist grandios gut!", betonte
sie leise, aber überzeugt. "Ich kenne keine Person, die nach zwei Treffen mit einer anderen
jene schon auf Buchdicke falten kann. Außer dir."

Lilið grinste und schlug die Augen nieder. "Natürlich tun Komplimente
gut.", seufzte sie. "Ich werde das nicht los."

"Dir fehlen Grundlagen für eine Magieart, die nicht in der Schule
unterrichtet wird.", fuhr Marusch fort, ohne auf Lilið einzugehen. "Du
hast ein beachtliches Verständnis der Dinge. Ich traue dir zu, zu lernen,
wie du Stein auf Dauer falten kannst."

"Du meinst, so, dass er nicht mehr in seine alte Form zurückspringt,
wenn ich die Faltung nicht mehr halte?", fragte Lilið.

"Ich bin mir absolut sicher!", bestätigte Marusch. "Und zwar, wenn du
das richtige Wissen hast, vermutlich recht zügig."

Lilið runzelte die Stirn. "Hätte ich so etwas auf dem Internat für
skorsche Damen gelernt?", fragte sie skeptisch. Sie wollte ein
'nein' hören und wurde nicht enttäuscht.

"Ich denke nicht." Marusch strich sich die Haare hinters
Ohr. (Ins Ohr beißen hatte bei ihr keine beeindruckende Wirkung, aber
Lilið hatte gelernt, dass sie es bei sich sehr
mochte.) "Das Internat ist für Nachbildung
da, um einen Hochschulabschluss erringen zu können. Damit können die
Damen und die vermeintlichen Damen dann studieren, und vielleicht, irgendwann
im zweiten Semester der passend gewählten Fachrichtung erst würden sie
das lernen können, was du brauchst. Nicht, weil es so schwierig wäre, wobei,
das ist personenabhängig. Ich glaube, für dich wäre es nicht schwierig. Sondern
weil es eine andere Herangehensweise an die Sache zeigt, die wunderschön
ist, aber eben nicht der Denkstruktur der meisten Menschen entspricht und
daher nicht schon früh unterrichtet wird."

"Hast du Magie studiert?", fragte Lilið irritiert.

"Aus Büchern. Ich kenne die Theorie.", antwortete Marusch. "Ich liebe
das Verständnis sehr. Der Moment, in dem sich mir alles erschließt
und ich die Welt immer ein Stückchen besser verstehe."

Lilið dachte an das schöne Gefühl, wenn sie ein Papier in seiner
Zusammensetzung voll durchdrungen hatte. Oder ihren Körper. "Du
bist zu beschäftigt mit deinem Buch, um mir
etwas beizubringen, oder?", fragte sie.

"Ich glaube, selbst ohne mich am Buch zu verbeißen habe ich heute
höchstens den Nerv, mit dir über einen bestimmten Teil der Grundlagen
zu reden, der mir gerade sehr präsent ist.", antwortete Marusch. "Aber
ich kann dir gern beim Segeln alles erklären, was ich weiß."
