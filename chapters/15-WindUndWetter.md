Wind und Wetter
===============

*CN: Sturm, Lebensgefahr, Kopfverletzung erwähnt,
Misgendern, Angst vor Ertrinken, sexuelle Übergriffigkeit.*

Die Wolken am Horizont hatten fast harmlos ausgesehen, vielleicht
etwas gelblich, sonst normal, wie die meisten anderen ungefährlichen
Wolken auch. Aber als sie nach einer Stunde Segeln unter wieder
angenehmsten Bedingungen, an sie herannahten, sah
das ganz anders aus.

Lilið wusste, wie es aussah, wenn sich mehr Wind näherte. Das Wasser
darunter war mit vielen kleinen Miniwellchen auf den großen Wellen
übersäht, die es dunkler wirken ließen. Sie mochte den
Anblick. Sie mochte, wie diese dunklen Flächen über das Wasser
huschten und das Gefühl in Boot und Körper, wenn der Wind, den
sie mitgebracht hatte, in die Segelfäche traf. Und ihr die Ohren
kühlte. Sie mochte die Dynamik dadurch, wie ihr Körper angespannt
auf die eintretende Krängung des Bootes wartete, und dann wie
von selbst reagierte, um Gewichtslagerung des eigenen Körpers
entsprechend anzupassen.

Die dunkle Fläche, die sich nun näherte, war nicht einfach eine kleine
Böe, sondern bedeckte, weiter als sie sehen konnte, das ganze
Meer zu steuerbord. Und es war nicht bloß eine
dunklere Fläche, sondern die See war fast schwarz.

"Das wird unlustig.", rief sie Marusch zu.

"Glaubst du?" Marusch schmunzelte. "Gefährlich ja, aber du hast
doch Spaß an so etwas, oder lese ich deine Freude im Gesicht
falsch?"

Das stimmte. Lilið konnte nicht anders, als breit zu
grinsen und fast zu lachen. "Wir müssen trotzdem vorsichtig
sein."

Lilið fühlte die Anspannung unter der Haut, aber war vollkommen ruhig,
als die schwarze Wetterwand sie erreichte. Sie hatte sie richtig
eingeschätzt, kurvte doch in den Wind, weil sie sonst gekentert
wären (was auch ein lösbares Problem gewesen wäre, nur kein angenehmes),
und gab Marusch Anweisung, das Vorsegel straff zu halten, damit sie
es einrollen konnten. Aber auch, als das getan war, und sie nur noch
die Großsegelfläche hatten, war es noch zu viel. Sobald der Wind in
das Segel in einer Weise strich, dass es auch Antrieb gab, kippte
die Ormorane bereits auch so stark auf ihre Seite, dass Lilið
wieder in den Wind steuern musste, weil die Lage nicht stabil genug
war, die Ormorane sich andernfalls auf die Seite gelegt hätte. Dabei
war die Schot nicht einmal ganz dicht geholt dafür und das Segel
flatterte noch halb.

"Ziehen wir das Schwert?", fragte Marusch schreiend gegen den Wind an.

Lilið hatte gerade überlegt, auch das Großsegel einzuholen und den
Sturm einfach wie in einer Nussschale auf den bald hohen Wellen
auszusitzen, aber das ginge natürlich auch. Sie zögerte kurz,
dann nickte sie. "Gute Idee!"

Das Schwert erfüllte den Zweck, dass ein Segelboot nicht abdriftete. Wenn
der Wind von der Seite kam, waren die Segel so ausgerichtet, dass er das
Segelboot eher nach vorn als nach hinten schieben würde, aber eben
an sich im Wesentlichen zur Seite. Das Schwert war nicht viel mehr als ein
stromlinienförmig abgeschliffenes Brett, das unter Wasser
die Querbewegung des Bootes erschwerte, sodass
die vom Wind vorgesehene Seitwärtsbewegung abgebremst oder sogar in eine
Vorwärtsbewegung umgesetzt wurde. Der Preis dafür, nicht einach
abzudriften, war die Krängung des Bootes: Wenn der Wind das
Boot nicht in seiner Gänze einfach über das Meer pusten konnte,
weil es durch das Schwert vermieden wurde, so resultierte der
Druck in einer Schräglage des Bootes, das durch Körpergewichtverteilung
ausgeglichen werden konnte. Wenn der Wind nicht zu stark war, wie
jetzt.

Ohne Schwert würden sie also nicht in die Richtung segeln, die
sie geplant hatten, dafür aber aufgerichtet. Aber ihr Weg
zu ihrer Zielinsel wäre vermutlich hinterher trotzdem kürzer, es
wäre vermutlich ungefährlicher und würde mehr Spaß machen.

Marusch zog also das Schwert und Lilið drehte die Ormorane aus dem
Wind, sodass er nun schräg von hinten kam. Wenn sie schon
in Richtung Wind abdrifteten, dann konnte sie den Bug auch mehr
in die Richtung drehen, wo sie eigentlich hinfuhren. Je weiter
von hinten der Wind kam, desto weniger krängte das Boot, aber
wenn er ganz von hinten käme, wäre es auch wieder gefährlich, weil
durch leichte Drehungen des Windes das Segel plötzlich auf der anderen
Seite stehen können wollte. Wenn es die Seite dann mit Wucht
wechseln würde, konnte das durchaus lebensgefährlich sein, wenn
ihre Köpfe nicht rechtzeitig wegduckten. Und das Material würde
auch dann leiden, wenn sie es rechtzeitig schaffen würden.

Ein Raumwindkurs war der sicherste Kurs, und auch der schnellste. Lilið
füllte das Segel und fühlte eine unbeschreibliche Freude in
sich aufsteigen. Sie war noch nie in ihrem Leben so fühlbar schnell
über das Meer gejagt. Der Sturm hob die schwere Ormorane fast aus
dem Wasser. Wellen interessierten sie kaum, sie schnitt durch
sie hindurch, als wären sie viel dünnflüssiger als sonst. Sie
zerstob die Wellenkämme in kleinste Tröpfchen, statt dass
sie für sie wie üblich einen Widerstand darstellten. Lilið
fühlte die Vibration des Wasserdrucks auf der Ruderanlage und
in der Bordskante unter ihrem Po, das leichte Rütteln in
der Schot, die sie über den Flaschenzug führte. Marusch hatte
keine Schot mehr zum Bedienen, hielt sich an einem der dafür
gedachten Griffe fest und ihr Körper reagierte auf die leichtesten
Veränderungen, wie die Ormorane im Wasser lag, um mit ihrem
Gewicht auch leichter Krängung entgegenzuwirken. Sie wussten beide, dass sie keine zu
großen Fehler machen durften. Die Gewalt des Windes würde einiges
an der Ormorane auseinandernehmen könnten, wenn sie sich
nicht wie vorgesehen bewegte.

Sie waren derzeit weit weg von jeglichem Land, was auf der einen
Seite ungünstig war, weil so, wenn etwas Schlimmeres passierte, weniger
Rettung nah war, auf der anderen Seite aber auch gut, weil
das Land, was ihnen derzeit am nächsten war, eine felsige
Küste aufwies, in der die Ormorane hätte Schaden nehmen können.

Als nächstes setzte der Regen ein. Und was für ein Regen. Er
hätte sie auch binnen Sekunden vollständig durchnässt, wenn sie
in frischer, trockener Kleidung gesteckt hätten. (Lilið vermisste
nach knapp einer Woche durchaus, etwas anzuziehen, was nicht ein
durchgeschwitzter Anzug oder ihr von Salz starrende, fast
dauerhaft feuchte andere Kleidung war.) Die angenehme Kälte kroch unter Liliðs
Segeljacke und über die nackte Haut, um bald wieder angewärmt
zu werden. Die kräftigen Regentropfen klatschten auf ihre
Hände und in ihr Gesicht. Es war ein beeindruckendes Wetter,
wunderschön! Zum Glück blitzte oder donnerte es nicht.

Gerade, als sie sich an die Lage gewöhnt hatte, fühlte sie mehr, als
dass sie hörte, dass die Ruderanlage brach. Sie hatte plötzlich
keinen Widerstand mehr an der Pinne. Einen kurzen Moment fühlte sie
noch den Sog, weil sie noch an einigen Phasern an der Bruchkante
hing, dann fehlte auch jener.

Die Ruderanlage war zum Steuern eines Bootes da. Ein Boot
brauchte sie nicht unbedingt, es konnte auch über Gewichtsverteilung
gesteuert werden. Aber das war wesentlich kniffeliger, erforderte
ein gutes Gefühl fürs Boot und viel Wissen. Lilið hatte es nicht allzu
oft geübt, aber ihre Reflexe saßen trotzem gut genug, dass sie für
den Moment mit Gewichtstrimm ein weiteres Abfallen verhindern
konnte. Abfallen hieß es, wenn das Boot weiter von der Windrichtung
weggedreht wurde, in diesem Fall also so, dass der Wind von hinten
käme.

"Was ist passiert?", schrie Marusch gegen Wind und Regen an.

"Ruderbruch!", schrie Lilið zurück.

Marusch fluchte.

Sie schlingerten für ein paar Momente unkoordiniert über
die Wellen hinweg, immer noch mit beachtlicher Geschwindigkeit, bis
Lilið beschloss, dass das zu gefährlich war, Segel locker ließ
und versuchte, das Boot in den Wind zu drehen. Es klappte nicht
ganz, aber gut genug, um zu bremsen. Der lose Segelstoff veranstaltete
einen beängstigenden Lärm. Lilið würde ihn bald mit Marusch zusammen
bergen, aber zunächst sah sie sich noch einmal nach dem fehlenden
Ruder um. Es hatte keinen Sinn. Die Wellen waren inzwischen einen
halben Meter hoch oder höher, die See eine einzige dunkle
Fläche mit dem Muster der einschlagenden Regentropfen darin. Die
Ruderanlage selbst war ebenfalls dunkel lackiert und vermutlich längst weit
weg. Sie würden sie nie und nimmer wiederfinden.

"Segel einholen?", fragte Marusch.

Lilið verstand sie gegen Lärm von Wetter und Segel nur gerade so, obwohl
sie schrie, und nickte.

Die Ormorane schaukelte sehr, als sie das Fall lösten, mit dem sie
das Segel hochgezogen hatten. Es verhakte sich mehrfach, während sie
gegen die Kraft des Windes das Tuch hinunterzogen, der es immer
wieder ergriff und davon reißen wollte.

Ob es nur den Anschein erweckte oder auch so war, wusste Lilið nicht,
aber nun, ohne Segel, ohne Fahrt, fühlte sie sich nicht halb so sicher
wie bei der rasenden Geschwindigkeit davor. Vielleicht spielte auch
das Wissen hinein, dass sie nun keine Ruderanlage mehr hatten und ohne
eben nur bei wenig Wind gesegelt werden könnte. Dabei würden sie dann
auch noch verlangsamen. Die Wahrscheinlichkeit war einfach sehr
hoch, dass der weitere Verlauf ihrer Reise eine größere Umplanung erfordern würde und
die Uneinkalkulierbarkeit machte Lilið zu schaffen.

Sie schaukelten in ihrer Nussschale zwischen den immer höher werdenden
Wellen im strömenden Regen, der immerhin Salz von ihren Körpern
spülte, und konnten nichts tun, als abzuwarten und zu hoffen, dass sie
das Wetter nicht sonstwohin abtrieb. Die geplante Route würden sie
nur mit sehr viel Glück beibehalten können. Natürlich gab es Ersatzrouten,
die Lilið sich zurechtgelegt hatte, aber sie war sehr nervös, ob eine
davon taugte, und wenn ja welche. Wie sich dieser Tag weiterentwickelte,
war derzeit völlig ungewiss. Lilið spürte durchaus auch
eine gewisse Angst, zu weit von jeglichem Land abzudriften und zu ertrinken, die
sie nicht ganz verdrängen konnte. Aber interessanterweise war ihr Gedanke,
dass es vielleicht mit dem Zertifikat dann nicht mehr klappen würde,
unangenehmer.

Marusch saß ihr gegenüber. Mit dem notdürftig aufgerollten Segel
dazwischen war das leider nicht einmal ansatzweise gemütlich. Lilið
liebte das Wetter trotzdem, aber segelnd hätte sie sich
trotz Gefahr wohler gefühlt. Und als der Regen
irgendwann nachließ und die ersten hellgrauen Stellen am Himmel sich
zwischen die dunklen Wolken mischten, nachdem sich Marusch das Haar
ausgewrungen hatte, gab Lilið Anweisung, das Segel wieder hochzuziehen.

"Jetzt schon?", fragte Marusch. "Was, wenn das gleich wieder so
heftig wird? Traust du dir bei dem zackigen Wind, den wir noch
haben, steuern ohne Ruderanlage zu?"

"Das sieht dir nicht ähnlich, die Vorsichtigere von uns zu
sein.", antwortete Lilið mit einem schmalen Grinsen, in
das sofort das süße Regenwasser floss. Sie bemerkte erst
einen Moment verspätet, dass sie sich gerade selbst
in ein Femininum eingeschlossen hatte. Weil sie Marusch gönnen
gewollt hatte, sich darüber zu freuen, darin aufgezählt
worden zu sein. Das Unwohlsein drückte trotzdem zu sehr,
also korrigierte sie: "Die vorsichtigere Person von uns. Ich
bin ja keine Frau."

Marusch lächelte. "Ich vertraue dir, dass du die
Lage richtig einschätzt.", sagte sie. Sie mussten immer noch
laut sprechen, um sich zu verstehen. "Ich wollte nur
vorsichtshalber nachfragen."

"Gib mir das eine Ersatzpaddel, damit kann ich auch ein bisschen
steuern!", forderte Lilið sie auf.

Sie hatten zwei. Sie waren für Fälle von Schiffbruch gedacht, wenn
sie nicht mehr segeln könnten. Schiffbruch hatten sie ja tatsächlich. Aber
eigentlich war es der alberne Stolz aller, die beim Segeln etwas auf
sich hielten, es nicht zu benutzen, um schneller voranzukommen. Lilið
war diese Regel eigentlich egal. Trotzdem sie fühlte sich oft unangenehm
beobachtet, wenn sie es doch mal tat und andere Segelboote in der
Nähe waren.

In diesem Fall würde sie ja aber gar nicht paddeln, sondern steuern. Das
war etwas anderes. Sie sollte es dabei nicht verlieren, eben weil
es für Notfälle gedacht war. Aber sie hatten für den Fall ja auch noch ein
zweites an Bord.

Lilið zog also mit Marusch das Segel wieder hoch, immer dann, wenn der
Wind gerade eine Pause davon machte, peitschend und hackig zu sein. "Wenn
es wieder schlimm wird, holen wir es wieder runter!", schrie Lilið
gegen den Lärm.

Sie segelten erst ohne Schwert, wieder mit Wind schräg von hinten, aber
sobald Lilið den übrigen, hackigen Wind einigermaßen gewohnt war, forderte sie
Marusch auf, es zu stecken. Sie steuerte die Ormorane so, dass
sie fast einen Amwindkurs mit Wind schräg von vorn segelten, um
ihr ursprüngliches Ziel vielleicht doch noch zu erreichen. Inzwischen
erblickte Lilið Espanoge in der Ferne, eine etwas größere Reiseinsel,
auf der es mehrere kleinere Städtchen gab. Espanoge wollten sie
eigentlich auf backbord passieren, aber bald stellte sich heraus, dass
es aussichtslos für sie wäre, ihr eigentliches Ziel anzusteuern. Und
zwar nicht, weil sie zu doll abgriftet wären, zumindest nicht allein
deswegen. Sondern weil der Sturm rapide nachließ und in eine Flaute überging.

Das war Liliðs Befürchtung gewesen, deshalb hatte sie beharrt, das Segel
schnell wieder zu setzen. Und als es einigermaßen sicher war, dann auch
das Vorsegel wieder auszurollen. Sie waren noch vielleicht eine knappe halbe Stunde
bei zu dem mäßigen Wind unpassend hohen Wellen brauchbar vorangekommen, aber
dann war der Wind fast ganz eingeschlafen. Die Segel labberten im Wind. Es
war nicht schön. So, wie es aussah, würden sie Espanoge erst gegen späten Abend
erreichen, und wenn sie auch nur ein bisschen Gegenströmung hätten, gar
nicht. Und für den Fall hatte Lilið keine Alternativroute im Kopf. Vielleicht
hätte sie am Abend zuvor doch etwas mehr Zeit damit verbringen sollen, sich
mit der Seekarte auseinanderzusetzen, anstatt Steine zu falten. Letzteres
hatte auch nicht sonderlich gut geklappt. Aber damit, dass sie eine Ausweichroute
zur Ausweichroute hätte parat haben müssen, und sie dafür nicht ohne
weiteres Gelegenheit auf der Fahrt haben würde, hatte sie nicht gerechnet. Für
den Fall von Sturm oder Flaute hätte sie einen Plan gehabt, aber beides
zusammen und ein Bootsschaden, der ihr nun Kartenlesen erschweren würde,
hatte sie nicht einkalkuliert.

Sie seufzte tief und besprach mit Marusch die Lage. Sie kamen beide
zu dem Ergebnis, dass sie vielleicht gerade so noch eine Chance hätten,
Espanoge zu erreichen, bevor die Insel schneller wegreiste, als sie
ihr folgen könnten, wenn sie jetzt nur alles aus der Ormorane
herausholten. Also paddelten sie, versuchten sich nicht um ihre
Segelndenehre zu scheren, und holten durch passenden Segeltrimm noch das letzte
bisschen raus, was ging. Irgendwo weiter weg dümpelten noch andere
Schiffe auf dem Meer, die eher eine Chance hatten, Espanoge noch zu
erreichen, oder die hochseetauglicher waren. Aber sie näherten sich
nur langsam und irgendwann blieb die Distanz einfach gleich. Sie
waren schweißgebadet und erschöpft, als sie zwei weitere Stunden
später aufgeben mussten.

Lilið zog resigniert und müde die Karte aus ihrem Fach und
rollte sie vorsichtig aus. Immerhin gab es keinen Wind, der
Teile der Karte hätte wegpusten können.

"Warte noch einen Moment.", hielt Marusch sie auf, als sie sich
den Zirkel hinters Ohr klemmte.

Lilið sah auf und folgte dann Maruschs Blick Richtung Espanoge. Ein
Ruderboot der Seewacht näherte sich ihnen. "Vielleicht möchten
sie uns reinschleppen.", murmelte Lilið. Sie fühlte sich bei dem
Gedanken erleichtert, aber auch ängstlich. "Espanoge ist nicht
so ab vom Schuss wie die anderen Inseln, die wir bisher angesteuert
haben. Meinst du, wir könnten Unannehmlichkeiten bekommen, weil
ich der Blutige Master M bin?"

Marusch zog einen Mundwinkel hoch. "Ich denke, wir sollten es
riskieren.", antwortete sie. "Wir brauchen eine Reparatur. Ich
sehe nicht, wie wir egal welche deiner geplanten Routen ohne
Ruderanlage schaffen können."

Lilið nickte alarmiert. "Du hast recht." Ja, sie konnte
prinzipiell mit dem Paddel steuern, oder auch über Gewicht, aber
sie hatte bei der kurzen Strecke heute schon gemerkt, dass es mächtig
auf die Arme ging. Das würde sie nicht mehrere Tage hintereinander
durchhalten. "Aber eine Reparatur wird nicht leicht zu bekommen
sein. Es sei denn, du kennst auf Espanoge zufällig noch eine
Person wie Heelem."

Marusch schüttelte den Kopf. "Das nicht. Aber ich habe auf dem
Ball, bevor du kamst, die Königin abgezogen und unter den Marken
ist eine Reisemarke."

Lilið starrte Marusch einige Augenblicke fassungslos an. "Was?", war
das einzige, was sie zunächst hervorbrachte. Und dann: "Ich habe
Fragen."

Die Königin war auf dem Ball gewesen? Hatte sie ihnen beim Tanzen
zugesehen?

Wie hatte Marusch das geschafft? Und warum ging sie ein solches
Risiko ein?

Von allen Königlichen aus der Monarchie, warum hatte Marusch ausgerechnet
die Königin bestohlen? Von allem, was Lilið mitbekommen hatte, war
sie die einzige Person aus der ganzen Monarchie-Familie, die
sich für ihr Volk tatsächlich interessierte. Aber vielleicht
war es auch deshalb am einfachsten gewesen, gerade sie zu
bestehlen.

Hatte Marusch überhaupt nur deshalb geplant, auf diesem Ball zu sein?

"Da wir nicht viel Zeit haben, würde ich die Fragen auf später verschieben
wollen.", hielt Marusch fest. "Erst einmal: Ich würde mich als Wache
der Königin ausgeben. Die Rolle habe ich schon ein paar Mal gespielt. Ich
wäre in einer Mission unterwegs, über die ich nicht viel Genaueres
sagen darf und du wärest mein Nautika. Ich mag die Rollenverteilung eigentlich nicht,
weil du mir dabei untergeordnet wärest. Aber ich halte für realistisch,
dass das klappen kann. Wir müssten aber wachsam sein, denn sobald etwas
auffliegt, sollten wir verschwinden." Marusch sah sich noch einmal zu
dem sich nähernden Rettungsboot um, aber es war noch außer Hörweite. "Möchtest
du das Risiko eingehen?"

Auch wenn die Königin vielleicht am nachsichtigsten sein mochte,
Lilið hätte zur Sicherheit der eigenen Haut sicher nie gewagt, irgendeine
Person aus dem Hause der Monarchie zu bestehlen. Also, nicht absichtlich. Sie
hatte es ja ausversehen bereits getan. Bei dem Gedanke musste sie beinahe
grinsen, aber noch fühlte sie sich zu alarmiert über Maruschs Offenlegung.

"Du kannst nicht rein zufällig in kurzer Zeit abschätzen und
darlegen, was uns blüht, wenn wir auffliegen?", fragte Lilið. Sie
konnte sich vorstellen, dass es wirklich nicht gut ankäme, dabei erwischt
zu werden, die Königin abgezogen zu haben.

Sie fragte sich außerdem, ob sie nach ihrem Nautika-Zertifikat
gefragt werden könnte, aber vermutete, wenn Marusch die Geschichte mit
der Wache abgekauft würde, dann würden sie eher noch ein
Angebot bekommen, von neugierigen Blicken abgeschirmt zu werden.

"Tatsächlich für überschaubar.", antwortete Marusch. "Zumindest
schätze ich die Folgen, wenn wir entlarvt werden, als geringer ein,
als würde irgendwie rauskommen, dass du der Blutige Master M bist."

Sie hatte leiser gesprochen, weil es vielleicht doch hätte sein
können, dass einzenle Sprachfetzen beim Ruderboot angelangt
wären, vermutete Lilið. Immerhin hörte sie das Stimmengewusel
der Rettungscrew bereits als Gemurmel aus der Ferne.

Lilið nickte. "Ich kann in so kurzer Zeit keine durchdachte Entscheidung
fällen.", murmelte sie. "Aber wenn du meinst, dass das klappen kann,
bin ich dabei. Ich versuche, mich zurückzuhalten und in dein Spiel
einzufügen."

Sie rollte die Karte wieder ein, hörte die Stimmen und Rudergeräusche
der Rettungscrew herannahen und sah noch einmal auf in Maruschs Gesicht,
als sie bemerkte, dass Marusch sie unentwegt anblickte. Maruschs dunkle
Augen hafteten auf ihr, das Gesicht leicht angespannt und vielleicht
besorgt. Um mich, dachte Lilið. Das warme Gefühl, das ihr unter der
durchnässten Kleidung über die Haut rann, war nicht nur positiv. Oder
doch? Es hatte eine Komponente, die sich wie Leiden anfühlte, aber es
fühlte sich zugleich schön an, wie der zerrende Klang einer sehnsuchtsvoll
gespielten Schwinge.

Marusch wandte sich wieder zum sich nähernden Ruderboot um, als es
in einer sinnvolle Rufweite gekommen war, noch bevor diese auch
ausgenutzt wurde. Sie winkte der Crew freundlich zu.

"Hey, braucht ihr Hilfe?", hörten sie eine laute Stimme zu ihnen hinüberdonnern.

"Das wäre ganz reizend!", rief Marusch zurück.

Lilið fühlte sich sofort angespannt. Nun war sie also ein Nautika einer
Wache der Königin. Wie verhielt sich so ein Nautika professionell? Einfach
ruhig sein? Vielleicht wäre das das beste.

Unvermittelt kam Lilið der Gedanke, wenn Marusch diese Rolle tatsächlich
gut spielen konnte und Lilið zuvor nichts von der Marke erzählt hatte, dass
sie vielleicht am Ende tatsächlich Wache der Königin gewesen war. Ob das ihre alte Identität
war? Das könnte mit den Morden zusammenpassen, die Marusch erwähnt hatte.

Die acht Rudernden legten sich noch einmal kräftiger in die Riemen, bis
sie bei ihnen angekommen waren und gerade so dicht neben ihnen abbremsten,
dass Marusch die Ruderblätter hätte anfassen können. Der Schliff sah sehr
schön aus, fand Lilið, sie mochte ihre flache schaufelartige Form.

"Habt ihr zwei Hübschen einen Sturmschaden?", fragte die Steuerperson.

Es war auch die Person, die vorhin gebrüllt hatte, erkannte Lilið an der
Stimme. Sie riss sich zusammen, um nicht zu reagieren. Die Betitelung
gefiel ihr gar nicht.

"Leider ja.", antwortete Marusch. "Unsere Ruderanlage ist gebrochen. Würdet
ihr uns nach Espanoge schleppen?"

Lilið fragte sich, ob sie richtig heraushörte, dass es Marusch auch nicht
passte, hübsch genannt worden zu sein. Obwohl sie es aus Liliðs Sicht
war. Sie könnte es ihr mal sagen. Es war schwer, irgendwas außer Freundlichkeit
aus Maruschs Stimme zu lesen, aber Lilið glaubte, sie inzwischen gut genug
zu kennen. Sie hatten noch ein weiteres Mal so gestritten, wie als Marusch
ihr mitgeteilt hatte, dass Lilið gefaltet trotzdem atmen können müsste. Auch
da hatte Marusch eigentlich ziemlich freundlich geklungen und doch war
da eben diese Anspannung in ihrer Stimme gewesen.

"Dafür sind wir da.", antwortete die schmierige Steuerperson. "Schafft
ihr das, unser Schleppseil um euren Mast zu knoten? Oder soll ich
rüberkommen und helfen?"

---

Lilið und Marusch sprachen keinen Ton miteinander, bis sie im Hafen eingelaufen
waren, was zum Glück recht zügig ging. Sobald sie mit dem Schleppboot verbunden
gewesen waren, war es losgegangen. Es war ein seltsames Gefühl gewesen, bei
Flaute über die noch vorhandenen Wellen voranzukommen, ohne Segel. Lilið hatte
hinten am Heck das nach der Verdrängung zurückströmende Wasser beobachtet. Sie
liebte diesen Anblick auch nach Jahren noch.

Sie wurden in den Teil des Hafens geschleppt, der zur Werft gehörte. Eine dicke
Person in einem Ringelhemd stapfte eine Rampe hinunter zu ihnen ins Wasser, um
das Boot entgegen zu nehmen. Lilið knotete sie wieder los, während Marusch
auch über Bord glitt und die Reste der Ruderanlage aushakte.

"Vielen Dank!", sagte sie laut, an niemanden bestimmtes gerichtet.

Die Rettungscrew verabschiedete sich, um wieder hinauszurudern und
nach weiteren Booten mit Sturmschaden Ausschau zu halten. Lilið
erleichterte das, und nicht nur, weil dann auch für andere eine
Rettung in Sicht wäre. Sie hasste so sehr, wenn Menschen, von deren
Hilfe oder gutem Willen sie abhängig war, schmierig waren oder
übergriffige Bemerkungen machten.

"Einen Sliphänger für eine Ormorane!", schrie die Person im Ringelhemd, die
ihr Boot immer noch hielt, in Richtung einer Halle, in die Lilið nicht
einsehen konnte.

Nachdem sie alles befestigt hatte, stieg auch sie aus. Gerade als eine
höchstens jugendliche Person aus der Halle einen Anhänger
herausrollte. Da Marusch und Lilið inzwischen beide das Boot hielten,
eilte die dritte Person der jugendlichen entgegen, um mit anzufassen. Der
Hänger wurde unter Wasser gerollt, sodass die schwimmende Ormorane mühelos hinaufgeschoben
werden konnte. Anschließend zogen sie das Boot zu viert in die Halle.

"Ich bin hier Hafenmeisterin und zugleich Restaurika.", stellte
die Person im Ringelhemd sich vor. "Der Hafen Goffold von Espanoge
ist vielleicht ungewohnt klein für euch, was? Deshalb fällt das
jedenfalls zusammen, dass ich für beides herhalten muss."

"Guten Tag.", grüßte Marusch. Ihre Stimme nahm dabei den
selbstsicheren und gleichzeitig ausladend freundlichen Tonfall an,
den Lilið von einer sehr adeligen Wache erwartet hätte. "Wir sind Marusch
und Allil. Wir kommen von Angelsoge."

Marusch hätte Lilið damit täuschen können. Irgendwann müssten sie allerdings
mal darüber sprechen, ob Lilið einen anderen Decknamen bekommen
könnte als Allil.

"Ah, richtig, mein Name ist Anni Sonne. Vornamen sind unter
Seeleuten ja üblich, also Anni reicht von mir aus.", fiel Anni
ein. "Angelsoge! Da habt ihr ja schon ein gut Stück Reise
hinter euch. Und irgendwer von euch ist ein gutes Nautika!"

"Ich kann mich nicht über Allil beklagen!", antwortete Marusch.

"Wo soll die Reise denn noch hingehen?", erkundigte sich Anni.

Lilið warf einen Blick auf die jugendliche Person. Sie stand
ein Stück hinter Anni und wartete. Es hatte eine gewisse Symmetrie,
dass Lilið ihrerseits ein Stück hinter Marusch stand und
genauso untätig wartete.

"Belloge.", antworte Marusch ohne Umschweife.

Die Insel Belloge lag noch hinter ihrem eigentlichen Ziel Nederoge. Es
war eine eher wenig wandernde Insel, die bereits zu einem anderen
Königreich gehörte.

"Also habt ihr nicht einmal ein Drittel eurer Reise geschafft.", stellte
die Hafenmeisterin fest. "Was sage ich, nicht einmal ein Viertel."

Das konnte hinkommen, überlegte Lilið.

Anni beugte sich über die Ormorane und besah sich den
Rest ihrer Ruderanlage. Sie strich mit der Handfläche über
die Abbruchkante. Das konnte kein schönes Gefühl sein, mutmaßte
Lilið. "Daran könnte ich schon was tun.", sagte
Anni. "Aber dafür würde ich schon ganz gern eine Marke sehen. Ich
handele mir sonst Schwierigkeiten ein."

Es wäre schon hilfreich gewesen, eine andere Marke gehabt zu haben,
als ausgerechnet eine der Königin, überlegte Lilið. Es wäre harmloser
gewesen, hätte weniger Aufsehen erregt und geringeres Risiko
bedeutet, wenn es aufgeflogen wäre.

"Dürfte ich deine Lizens sehen?", fragte Marusch. "Das muss seltsam
auf dich wirken, das tut mir leid. Es sollte sich schnell klären."

Anni blickte skeptisch, aber sie holte aus einer blauen
Gürteltasche ein stabiles, wasserfestes Papier heraus. Sie gab
es Marusch nicht in die Hand, sondern hielt es ihr bloß dicht genug
hin, dass sie es mustern konnte.

Marusch berührte, wie zur Prüfung üblich, Papier und Handgelenk. Es
gab Magie, mit der sich Haut auf Papier eichen ließ. Die Eichung
selbst benötigte höhere Magie-Kenntnisse, aber das Prüfen durch
Berührung war etwas, was Wachen üblicherweise konnten. Lilið fragte
sich, ob Marusch es gerade nur nachahmte, weil das das wäre, was
sie als Wache täte, oder ob sie es tatsächlich konnte.

Marusch ließ die Hand wieder sinken, nickte und holte
ihrerseits eine kleine Börse aus einer
Jackentasche, und daraus die Marke, von der sie Lilið erzählt hatte. "Ich
bitte darum, vertraulich mit dieser Information umzugehen.", sagte sie.

Anni nahm sie entgegen, blieb noch einige Momente misstrauisch, dann klärte
sich ihr Gesicht. "Selbstverständlich.", sagte sie. "Die Reparatur sollte
in drei Stunden erledigt sein. Ihr dürft im Hafenhaus essen und
in der Hafenherberge nächtigen, wenn ihr mögt. Ich schreibe euch dafür
einen Wisch, mit dem ihr nirgends Probleme bekommen solltet und anonym
bleibt. Sollte jemand Ärger machen, kommt gern wieder zu mir."
