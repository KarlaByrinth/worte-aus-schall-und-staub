Das Tanzdesaster von Lilið aus der Unterwelt
============================================

*CN: Sex, Erotik, Erektion, Vergiftung - erwähnt.*

Lilið antwortete nicht sofort, sondern hoffte zunächst, dass
Marusch sie in eine ruhigere Ecke führen würde. Das mit der
ruhigen Ecke stellte sich allerdings eher als realitätsferne
Wunschvorstellung heraus. Selbst
wenn Marusch gewollt hätte, wäre es kein leichtes Unterfangen
gewesen. Der Ball fand auf zwei Stockwerken statt. Im oberen
war es ruhiger. Dienstpersonal verteilte Getränke und kleine
Häppchen für zwischendurch. Marusch führte sie zum hinteren Bereich,
wo breite Treppen in den Tanzsaal hinabführten. Es gab eine Bühne, auf
der das Orchester die Tanzmusik spielte. Der Boden
vor der Bühne war mit glattpoliertem Holzparkett verkleidet, wie sich
das für eine erstrebenswerte Tanzfläche gehörte, riesig
und gefüllt mit Tanzenden. Es war nicht so voll, dass sich
die Tanzenden gesondert auf ihre Füße Acht geben müssen, und dadurch, dass die
Musik die Tänze vorgab, waren auch schnelle Tänze möglich, weil dann
alle geschlossen schnell tanzten.

Marusch war am Geländer stehen geblieben und blickte hinab. "Meinst
du, wir kriegen ungefähr das Niveau hin?"

Lilið hatte noch gar nicht auf die vorherige Frage geantwortet. Aber
vielleicht hatte Marusch korrekt von der Art, wie Lilið sich
hier her hatte führen lassen, schon abgeleitet, dass sie mindestens
Grundkenntnisse haben musste.

Lilið konzentrierte sich auf die neue Frage und beobachtete
die Tanzpaare im einzelnen. Sie sahen sehr geübt aus, und
wunderschön. Die Röcke flogen, die Dynamik brannte. Sie
tanzten in einer Weise, dass ihre Körper sich von oberhalb
der Hüfte hinab eng berührten. Ihnen dabei zuzusehen, und
sich Marusch und sich in so einer Haltung vorzustellen, fühlte
sich bereits fast zu viel an.

"Ich habe mich beim Tanzen nie im Spiegel gesehen. Aber
die meisten Figuren, die dort getanzt werden, kenne ich.", sagte
sie schließlich. "Muss ich die Herrenschritte beherrschen?"

"Ich würde wirklich gern mal in der geführten Rolle tanzen.", antwortete
Marusch. "Und ich denke, zu dem Tanz, den ich angekündigt habe, gehört
schon, dass du zeitweise führst. Es ist ein Werk, in dem
der Charakter Lilið, der passenderweise du wärest, Geschlechterrollen
vertauscht, indem sie die Führung übernimmt. Aber 'Das Tanzdesaster von
Lilið aus der Unterwelt' heißt nicht ohne Grund Desaster. Es kennt
hier außerdem wahrscheinlich niemand, und Neuinterpretationen
sind auf den angelsberger Bühnen weitgehend
akzeptiert. Ich denke, wenn es ein Desaster
wird, müssen wir es als Absicht verkaufen, und dann passt
das." Marusch schmunzelte. Ihr Blick wanderte von der
Tanzfläche weg und fixierte Liliðs Gesicht.

Liliðs Atem stolperte schon einmal vorsorglich, damit sie später
beim Vortanzen schon geübt darin sein würde. Oder aus
anderen Gründen. "Haben wir die Möglichkeit, vorher irgendwo
nicht allzu auffällig zu üben?"

"Sicher! Besonders, weil ich dich als meine Ersatztanzperson
ausgegeben habe, würde nicht weiter auffallen, wenn
wir uns zu den anderen auf die Tanzfläche
begeben.", sagte Marusch. "Es tut mir leid, dass
ich dir vor der Wache ein Femininum zugewiesen habe. Und ich
habe, unabhängig davon, versäumt, dich zu fragen, was du eigentlich möchtest,
als wir uns das letzte Mal gesehen haben."

"Ich mag Tanzperson und solche Wörter." Der Abstand zwischen
Lilið und Marusch am Geländer entsprach etwa dem, den sie
zu Tomden vor Allils Toilettenkabine gehabt hatte, nachdem sie weggerückt
war. Wegen dieser Erinnerung scheute sie sich, näher an Marusch
heranzurücken, obwohl sie es eigentlich wollte. Sie fügte
hinzu: "Vor der Wache habe ich eine Rolle gespielt. Es war
für mich in Ordnung, die Rolle einer Frau im Anzug zu
spielen, das war ja nicht ich."

"Ein sehr schicker Anzug wohlgemerkt. Er hat ein schönes
Schillern. Du siehst sehr schön aus darin." Aus Maruschs Stimme
war jegliche Kante gewichen. Leise und sanft strich sie in
Liliðs Gehörgang und von dort unter die Haut.

Lilið schloss einen Moment die Augen und atmete langsam
und vielleicht etwas zittrig ein und wieder aus, bevor
sie sie wieder öffnete. Dieses Mal wagte sie es, Marusch
genauer anzusehen. Sie war nun ganz froh, nicht näher
gerückt zu sein, weil sie sonst zu wenig von Marusch gesehen
hätte. Das Kleid war, -- als hätten sie sich abgesprochen --, aus
einem blaugrünen, schillernden Stoff. Es ließ Maruschs
Arme frei und saß am Oberkörper so perfekt, dass es
unmöglich einer anderen Person geklaut sein konnte, zumindest
nicht ohne, dass es angepasst worden wäre. Der Rock fiel bis auf den
Boden und war etwa zur Hälfte von schräg abgeschnittenen, durchsichtigen
Überröcken verdeckt, die in jedem Windhauch leicht flatterten.

"Du bist auch wunderschön.", hauchte Lilið, Maruschs
Sprechweise kopierend, so weich sie konnte. "Das Kleid und
du wirkt wie eins."

Lilið konnte die Wirkung der gewählten Worte und des
Klangs derselben an Maruschs Körper
beobachten. Das verlegene Ausweichen des Blicks, der so glücklich
wirkte, und vielleicht ein bisschen nervös, als er ihren dann doch wieder
fand. "Müssen wir eigentlich noch zu zweit auf dem Balkon
verschwinden, um einer Person ein Kleid zurückzugeben?", fragte
Marusch verschmitzt?

Lilið kicherte. "Das ist bei dir ja mit dem Wunsch, nicht aufdringlich
zu sein, den du noch im Brief formuliert hattest, ja nicht weit her."

Zu Liliðs Überraschung und vielleicht auch Sorge verschwand
der Schalk aus Maruschs Gesicht. "Das ist so interpretierbar,
das tut mir leid.", sagte Marusch ernst. "Falls
es dir Sicherheit gibt: Ich habe den Eindruck, dass wir
noch mindestens ein klärendes Gespräch über unser Verhältnis
zueinander führen sollten, im diebischen Sinne. Das
hätte ich gern hinter mir, bevor ich überhaupt in Betracht
ziehe, dich zu fragen, ob du mit mir eventuell Sex haben
wolltest. Und wenn ich dich dann fragen sollte, werde
ich ein 'nein' immer akzeptieren. Es ging mir wirklich
nur darum, eine andere Person, nachdem diese Sex hatte,
nicht nackt dastehen zu lassen."

"Sehr nobel.", kommentierte Lilið. "Ich habe mich nicht bedrängt
gefühlt. Aber ich bin trotzdem dankbar darum, dass du es
so klar aussprichst." Sich an den Ursprung der Offenlegung
erinnernd, fügte sie hinzu: "Und nein, wir müssen kein Kleid zurückbringen.
Es war mein eigenes Kleid."

Interessanterweise hatte Lilið, bevor Marusch es so klar ausgesprochen
hatte, noch gar nicht explizit an Sex gedacht. Ihr war klar, dass
sie die Anspielung selbst gemacht hatte, und dass der Balkon
dafür vorgesehen war, aber so richtig hatte sie den Gedankenschritt
noch nicht vollführt, dass mit Marusch in einer Kabine auf dem
Balkon zu verschwinden, nicht nur eine anzügliche, sondern genau
diese Bedeutung hatte. Oder musste das gar nicht so sein?

Sie stellte sich unwillkührlich vor, wie sie nackt in so
einer Kabine wären, ihre Körper sich in einer Weise berührten, dass
Berührung eigentlich ein viel zu zarter Ausdruck wäre. Sie stellte
sich Maruschs Erektion vor, wie sie gegen Liliðs Beine presste, oder
anderswo. War das, was sie wollte?

Sie verdrängte den Gedanken. Es fühlte sich falsch und richtig
zu gleich an. Sie musste es irgendwann sortieren, aber Marusch
hatte recht: Es gab so viel, was noch zuvor geklärt werden
sollte. Vielleicht besser zügig.

"Hast du zugehört?", fragte Marusch.

"Ich doch nicht!", erwiderte Lilið mit gespielter Irritation, was
denn die Frage sollte. Und fügte ernst hinzu: "Ich bin
in Gedanken abgedriftet. Es tut mir leid. Magst du wiederholen?"

"Ich hatte mich gefragt, warum du ein Kleid dabei hattest, aber
das ist gar nicht so wichtig.", wiederholte
Marusch. "Jedenfalls hat uns das wohl Probleme vom Hals gehalten und
das habe ich anerkennend zum Ausdruck bringen wollen. Dein
eigenes war natürlich kein Kleid, das die Wache zuvor
an einer Person hat auf den
Balkon gehen sehen. Ich hatte befürchtet, wenn du ein Kleid von draußen
stielst, dass wir das Problem mit der Wache nur verschieben, und hatte
mich auf mehr einfallsreiches Schauspiel gefasst gemacht."

Lilið überlegte einen Moment, Marusch zu erklären, dass sie
falten konnte. Aber das war ihr zu viel Information für
den Anfang, als nickte sie bloß.

Die Musik verklang und Liliðs Blick wurde von der anderen
Art Bewegung, die auf der Tanzfläche aufkam, von Marusch
abgelenkt. Personen strömten von der Tanzfläche und tupften
sich das Gesicht ab oder nahmen sich etwas zu trinken. Einige
Paare verharrten auf der Tanzfläche und warteten auf
den nächsten Tanz. Dann erklang ganz zart der sehnsuchtsvolle
Ton eines Bratschophons über die Tanzfläche. Mehrere Kontraphone
stimmten nur wenige Momente später, als der Einstigston begann, sich
allein zu fühlen, mit einem drängenden Rhythmus darunter
ein. Lilið fühlte all die Muskeln in sich anspringen, die fürs
Weinen da waren, aber bei Musik eher den Drang auslösten,
ebenfalls zu musizieren, zu singen, oder sich in einer Art
zu bewegen, in der sie den ganzen Körper spürte. "Tanzen?", flüsterte
sie.

Marusch nickte und lächelte ein schmales Lächeln, aber rührte
sich zunächst nicht. Lilið blickte sie etwas irritiert an, was
Marusch doch dazu brachte, den Arm zum Einhaken anzubieten. "Vielleicht
hast du recht, und es ist besser, wenn ich beim ersten Tanz
noch führe. Das hat vielleicht weniger Katastrophenpotenzial.", sagte
sie.

"Oh, du wolltest, dass ich den Arm anbiete?" Lilið hakte sich
ein. Sie spürte die Klänge des Bratschophons unbarmherzig an
Körper und Bewusstsein zerren.

Marusch nickte. "Das war der Gedanke.", sagte sie.

Lilið spürte, wie sie leicht vor Anspannung zitterte. Es würde
aufhören, wenn sie gleich tanzte, das wusste sie. Oder glaubte
sie zu wissen. Aber sie wusste nicht, ob sie es mit dieser
Anspannung geschafft hätte, das Führen auszuprobieren, bevor
sie nicht etwas davon abgearbeitet und in Tanz umgesetzt
hätte. "Nicht dieses Mal.", sagte sie leise.

Es war nicht erotisch geplant gewesen, aber sie fühlte, wie
Maruschs Körper darauf reagierte, und sie ihr einen Moment
sehr nah kam. Wieso mochte Lilið den Geruch so sehr? Sie
konnte ihn nicht einmal beschreiben.

Marusch führte sie in eine etwas größere Lücke, die sich zwischen
den Tanzenden ergeben hatte, und führte ihre Flanierhaltung
in eine Tanzhaltung für diesen Tanz über. Sie kamen sich
nun doch fast so nah wie vorhin in Liliðs Vorstellung, nur
mit zwei Schichten Kleidung dazwischen. Und ihre Gesichter waren
für ein stereotypes sexuell Interagieren ein gutes Stück zu weit
von einander entfernt. Liliðs Oberkörper hatte Raum und lehnte
sich zurück, verdrehte sich etwas wie sich das für diesen
Tanz gehörte. Maruschs Arme hielten sie fest und weich
zugleich, und sie hielt Marusch in der dazu passenden
Weise zurück. Marusch verlagerte das Gewicht auf eine Seite und Lilið
folgte ohne zu zögern, als wären ihre Körper nicht mehr
unabhängig. Dann, das Gewicht vorbeugend und passend
zum Takt der rhythmisch am Gefüge zupfenden Controphone schob
sich Maruschs Bein zwischen Liliðs zum ersten Tanzschritt.

Eigentlich war es egal, ob da nun Kleidung zwischen ihnen war oder
nicht. Was nicht hieß, dass es für Lilið sexuell wäre. Es war erotisch,
keine Frage. Aber es war Musik in Körpern, eine andere Dimension, in
der Sex in ihrem Kopf keinen Raum hatte. Es war, miteinander verschmelzen, in
eine andere Welt abtauchen, und endlich loslassen.

Marusch führte sie über die Tanzfläche, als hätten sie schon
lange miteinander getanzt. Lilið kannte es aus den Tanzstunden: Wenn
mal eine Lehrkraft mit ihr getanzt hatte, hatte es einfach
funktioniert. Sie hatte die Augen schließen können und überhaupt
nicht über Schritte nachdenken müssen. Es war ein Lauschen des
Körpers auf die schalllosen Worte, die der jeweils andere Körper
sprach.

Aber dieser Tanz übertraf jede Tanzerfahrung, die sie je
gemacht hatte. Marusch führte etwa so gut, wie ihre Tanzlehrkräfte.
Wo hatte sie das eigentlich gelernt? Aber Marusch war nicht
hier, um Lilið etwas beizubringen, sondern, weil sie etwas
miteinander zu tun haben wollten. Und weil sie beide
ein Verlangen danach hatten, körperlich und romantisch miteinander
zu sein. Das hatte Lilið so noch nie erlebt.

"Schön, dass du da bist und lebst.", sagte Marusch leise. Sie hatten
vielleicht vier Runden getanzt und allmählich waren selbst
die letzten Unebenheiten überwunden. Sie waren eingespielt. Zumindest,
solange sie keine Experimente wagen würden, sondern bei den
klassischen Figuren blieben. "Daraus schließe ich, du hast die Nachricht
entschlüsselt?"

Liliðs Herz hatte sich eben erst wieder beruhigt. Nun dachte
sie daran, dass sie gerade Mal vor zwei Tagen gegen ein
Gift ums Überleben gekämpft hatte. "Warum hast du mir eine
potenzielle Mörderin geschickt?", fragte sie. Sie gab
sich nicht die Mühe, ernst dabei zu klingen, sondern ließ
die Erotik, die Marusch vorgelegt hatte, zwischen ihnen
einfach bestehen.

"Pragmatische Gründe" Marusch schmunzelte. "Es löst dein und
ihr Problem."

"Es hätte das Problem, das ich so als Person darstelle, beinahe
ganz und für immer beseitigt." Dieses Mal passierte es ihr doch,
dass sich eine gewisse Kiebigkeit in ihre Worte schlich. Vielleicht
war das auch ganz gut so.

Marusch führte in der Kurve eine Katjunka, bei der ihr Körper sich wie von
selbst nach hinten lehnte und die Blickrichtung wechselte. Sie
fühlte die angenehme Rotation im Körper, als sie die Drehung wieder
ausgedrehten.

"Ich bin sehr froh, dass es das nicht hat.", sagte Marusch
sehr sanft und blickte ihr ins Gesicht dabei. Das gehörte sich
beim Tanzen eigentlich nicht, aber seit jeher hatten Tanzende, die
sich gleichzeitig mochten, diese Regel geflissentlich ignoriert. Außerhalb
von Wettbewerben zumindest.

"Aber du warst gewillt, das Risiko einzugehen?", bohrte Lilið
schnippisch nach.

"Schon. Ungern natürlich.", gab Marusch auch noch
zu. "Unter den Umständen, dass du dem System entfliehen willst, wird
das vermutlich ab jetzt dein Standardrisiko sein. Unter anderen Umständen
hättest du Allil weggeschickt und wärest sicher gewesen."

Lilið fragte sich, ob ihr das irgendwie hätte klar sein müssen. Eigentlich
lag es tatsächlich nicht sehr fern. Sie wusste, sofern es nicht
bloß um Lebensmittelraub oder so etwas ging, dass das Langfingerdasein
lebensgefährlich war. Dass unter den Diebischen andere
Regeln galten. Nicht einmal einheitliche. Es war schwer zu
wissen, woran man jeweils war. Sie war schließlich auch nicht überrascht gewesen, als
die landstreichende Person ihr in der vergangenen Nacht ihr Gepäck hatte
stehlen wollen, und sie wusste, dass manche Langfinger im Kampf
so weit gingen, zu töten. Aber sie hätte es nicht von Allil
erwartet.

Sie waren eine Runde mit sehr einfachen Standardfiguren getanzt, als
Lilið aus ihren Gedanken zurückkehrte. Vielleicht hatte Marusch
darauf Rücksicht genommen, dass sie weggetreten gewirkt hatte, aber
als sie sie anblickte, war Maruschs Gesichtsausdruck selbst
sehr nachdenklich.

Was auch immer da für Gedanken sein mochten, Lilið unterbrach
sie mit ihren Überlegungen. "Dadurch, dass du mir diese Option
dargelegt hast, sahen meine Chancen besser aus und
es hat mir mehr Sicherheit gegeben, die Entscheidung zu fällen.", sagte
sie.

"Es sind die besten Chancen, die du kriegen konntest, denke
ich." Da war wieder dieses sanfte Schmunzeln. Die Nachdenklichkeit
war nicht aus dem Ausdruck gewichen, sondern vermischte sich
damit, was auch sehr schön aussah.

Aber Lilið ließ sich nicht davon ablenken. Sie fand schon, dass
Marusch eine Mitverantwortung trug und überlegte, wie sie sie
dazu bringen konnte, sie einzugestehen.

"Wenn du aus mir rauslocken willst, ob ich bewusst einen Deal eingegangen
bin, bei dem dein Leben in Gefahr ist: ja bin ich.", legte Marusch offen,
als hätte sie Liliðs Gedanken gelesen. Aber vielleicht waren sie
auch offensichtlich. "Ich bin kein
moralisch einwandfreier Mensch. Und ich habe Verständnis, wenn du
deshalb mit mir nichts zu tun haben willst."

Lilið schluckte. "Ich mag, wenn Dinge klar ausgesprochen sind.", sagte
sie. "Deshalb möchte ich festhalten: Wäre ich gestorben, hättest
du eine Mitverantwortung gehabt."

"Ja.", stimmte Marusch ohne zögern zu.

"Und du fragst mich trotzdem einfach fröhlich, ob wir eine
Diebesbeziehung eingehen wollen?", fragte Lilið.

Die Figur, die Marusch jetzt führte, war neu für Lilið. Sie war
schwungvoll und sie wäre fast gestolpert. Seltsamerweise
fühlte sie sich deshalb nicht verunsichert, sondern eher
positiv aufgeregt. Ihr Herz machte einen seltsamen Satz, als die
Vollendung der Figur sich wie ein Auffangen anfühlte und
sich ihre Gesichter für einen kurzen Moment dicht aneinander
vorbeibewegten. "Ja, auf jeden Fall!", flüsterte Marusch
in gerade diesem Moment. "Ich frage dich fröhlich, oder
auch ernst, wenn du willst."

Sie hatte Mühe, ihren Atem zu beruhigen, als sie wieder
zu eingeübteren Figuren übergingen. Vielleicht sollten
sie es nicht gleichzeitig tun: Aufregend tanzen und
sich über fiese Themen unterhalten.

"Hast du irgenwelche Erklärungen dafür, wie das für dich
übereingeht?", fragte sie.

Auch diese Frage beantwortete Marusch mit "Ja.". Aber
sie sagte nichts weiter dazu.

Lilið merkte, wie ihr die Tränen doch kommen wollten. Weil
alles so sehr zog. Die Musik an ihrem Inneren. Die Rotation
des Tanzes ganz physisch an ihrem Körper. Die Nähe an
ihren Sehnsüchten. Den Widerspruch in ihr, dass sie
diese Person mochte, die einfach ihr Leben riskiert hatte,
an ihrem ganzen Sein.

"Die du verraten möchtest?", fragte sie, die Stimme dabei
nicht ganz frei von einem Flattern halten könnend.

"Was wäre die Alternative gewesen?", fragte Marusch. Der
nachdenkliche Ausdruck von vorhin war wieder da. "Dir einfach nicht die
Option öffnen? Bereust du es, sie eingegangen zu sein?"

"Nein, ich bereue das nicht.", sagte sie ohne jeden Zweifel. Nie
und Nimmer hätte sie diesen Tanz verpassen wollen. Und es war
erst der erste heute Abend.

Marusch führte sie noch einmal die neue Figur von vorhin, bei
der sie fast gestolpert wäre. Dieses Mal fühlte sie keine
Unsicherheit mehr dabei, nur Aufregung. Und eine innere
Begeisterung, dass es klappte, dass sie das zusammen konnten, und
über die bewegte Schönheit, die sie innerlich waren und äußerlich
für alle sichtbar sein mussten.

Marusch schloss die Figur allerdings anders ab als vorhin, sodass
sie sich mit dem letzten Ton des Musikstücks in eine gegenseitige
Verbeugung auflöste. Lilið spürte das Ziehen des ausklingenden
Bratschophonklangs im ganzen Körper. Ihr war warm. "Gibt
es noch einen unverfänglicheren Balkon?", fragte sie.

"Ja. Aber wenn du einfach nur an die kühle Luft willst, schlage ich dir
den Hof vor. Vielleicht finden wir dort sogar ein bisschen
Raum für uns.", schlug Marusch vor.

Lilið nickte. Einen Augenblick drängte sich wieder das ungewollte
Bild ihrer nackten Körper in ihre Vorstellung. Nein, das
wollte sie nicht, nicht heute zumindest. Nähe schon, aber andere.

"Es war keine einfache Entscheidung.", erklärte Marusch auf dem Weg zum Hof,
das Gespräch von vorhin fortsetztend. "Ich habe abgewägt und bin zum Schluss
gekommen, das müsste trotz Risiko in deinem Sinne sein. Aber ich kann dich auch
noch nicht sehr gut einschätzen. Hätte ich aus deiner Sicht etwas Bestimmtes
anders machen sollen?"

Lilið hatte recht spontan eine Antwort, und doch fühlte
es sich überraschend nachvollziehbar und vor allem wertschätzend
an. Ja, das war es, glaubte Lilið, was sie so sehr an Marusch
schätzte: Wenn sie etwas kritisierte, sagte, dass sie etwas
störte, machte sich Marusch nicht Gedanken darum, sich selbst
zu verteidigen, sondern ließ es dabei um sie und ihren
Freiraum gehen. Darum, dass sie sich wohlfühlte, wenn es
in Maruschs Macht stand.

"Es nicht drauf ankommen lassen, dass ich eine Kodierung
rechtzeitig knacke, sondern für mehr Sicherheit sorgen, dass
die Nachricht auch wirklich bei mir ankommt.", erklärte
sie, kaum mehr aufgebracht, sondern fast sachlich.

Marusch nickte. Sie blickte dabei nachdenklich. Als würde
sie einiges an Erwiderungen herunterschlucken. "In Ordnung."

Vielleicht würde Lilið später danach fragen, was in ihrem
Kopf dabei vorgegangen war. Es gab ohnehin noch viel zu
klären.

Sie fanden einen Platz an der kühlen Mauer in der schützenden
Dunkelheit einiger niederiger Bäume und der sternenlosen Nacht. Marusch
lehnte sich gegen die Wand und verschnaufte. Lilið blieb unschlüssig vor
ihr stehen. Der Geruch war beim Tanzen noch etwas mehr
geworden. "Du riechst gut.", flüsterte sie.

Marusch reagierte nicht, nicht mit Worten zumindest. Ihr
Blick lag auf Liliðs, ohne Lächeln, aber sehr weich, vielleicht
flehend. Lilið beobachtete, wie sich Maruschs Brustkorb hob und senkte. Lilið
trat dicht an sie heran, nicht so dicht, wie sie beim Tanzen
gewesen waren, aber nun hatte es eine andere Bedeutung. Sie
fragte sich, ob sie Marusch küssen sollte. Sie hatte noch
nie geküsst, nur davon geträumt. Sie legte ihre Hände
links und rechts von Marusch an die Wand. Die Wirkung
war schön. Marusch wehrte sich nicht, machte keine Anzeichen, ausweichen
zu wollen, aber ihr Atem verhedderte sich einen Moment.

"Lilið.", sagte sie leise, kaum ein Flüstern.

"Ja?" Lilið ahmte wieder Maruschs Lautstärke und Anspannung nach.

"Ich glaube, du wirst das ganz gut hinkriegen mit dem
Führen.", sagte Marusch.

Das war nicht direkt, was Lilið erwartet hätte. Aber vielleicht
stimmte es und passte doch zur Situation. Lilið kannte so viele Geschichten über
Romantik, in der sie eher für die Rolle der Person vorgesehen
wäre, die von einer anderen gegen die Wand gedrückt wurde. Oft
mit fragwürdigerem Einverständnis.

"Fühlst du dich wohl?", fragte sie. Sie wusste es eigentlich,
aber es war wichtig, das zu fragen, fand sie.

"Schon." wieder das leise Flüstern.

Das war auch nicht die Antwort, die sie ihr Sicherheit gegeben hätte. Sie
überlegte, die Hände wegzunehmen. Sie überlegte, dass 'schon'
bedeutete, dass da ein 'aber' zugehörte, aber gleichzeitig hieß
es trotzdem auch 'ja', deshalb verharrte sie doch.

"Ich denke, wir sollte das aufschieben. Wir sollten
entweder noch einmal für die Aufführung üben, aber mit
dir in der führenden Rolle, weil bis zu den Aufführungen
nicht mehr so lange hin ist, oder wir sollten uns
entscheiden, uns jetzt aus dem Staub zu machen."

Lilið nahm die Hände von der Wand weg. Es war schwer. Weil
sie wusste, dass sie es beide wollten, und nur Verpflichtungen
dazwischen standen. Sie hielt Marusch den Arm hin, wie sie
es vorhin für Lilið getan hatte.

Um auf der Tanzfläche die Tanzhaltung korrekt spiegelverkehrt
einzunehmen, brauchte Lilið alles an Konzentration. Und auch
der erste Schritt fühlte sich noch seltsam falsch an, eben wie
mit der falschen Seite ausgeführt. Aber dann passierte es, dass Lilið
in die führende Rolle schlüpfte. Es eröffnete sich für sie abermals
eine neue Welt. Sie konnte keineswegs so viele Figuren führen, wie
Marusch. Sie fing mit den Grundschritten an. Aber als sich
Marusch schließlich auch, von fast einem Moment auf den
anderen, in die folgende Rolle fügte und Lilið bestimmte, wie
sich ihrer beider Körper bewegen würden, sich mit ihrem Blick in Maruschs
Röcken verlor, die besonders bei schnellen Drehungen und
Richtungswechseln schwangen, fühlte sie sich frei.

---

Lilið war eigentlich nicht ganz klar, wie es sein konnte, dass
sich Maruschs so spontan vor der Wache vorgelegter Programmpunkt auch
tatsächlich ohne weitere Probleme ins bestehende Programm einfügte. Vielleicht
gab es eigentlich kein festes Programm. Aber alles war egal, alle
Fragen waren weggewischt, als sie allein auf der Tanzfläche
standen. Einige Aquaristikae ließen Wasserdampf um ihre Füße
wabern, der die Unterwelt darstellen sollte. Es war ein unbeschreibliches
Gefühl, im Mittelpunkt zu stehen, von so vielen Leuten dabei
beobachtet zu werden, zu tanzen -- und zu betrügen. Es war
ein Tanz mit der Gefahr und mit dem Feuer.

Die Musik zum 'Tanzdesaster von Lilið aus der Unterwelt' war
dem Orchester nicht sehr vertraut, wie Lilið aus dem Flüstern
unter den Musizierenden heraushörte, bevor sie einsetzten. Es war ein Stück, das
unheimlich anfing, sich bombastisch aufbaute und zart
endete.

Sie tauschten beim Tanz die führende und folgende Rolle, teils
einvernehmlich, teils durch gespielten Kampf. Marusch kannte
das Stück und Lilið las an ihren Bewegungen und Anspannungen
ab, was für eine Stimmung es als nächstes haben würde.

Aus ihrer Sicht waren sie beide Lilið in dieser Vorführung.

Wie es dazu kam, dass sie so endeten, war Lilið nicht ganz klar, aber
als der letzte Ton erklang und der Nebel noch einmal verstärkt
wurde, hielt sie Marusch von hinten in einer Umarmung, in der
ihr Kopf in Liliðs Halsbeuge lag, ihre Lippen sie fast dort
berührten.

---

Sie hielten es nach dieser Aufführung nicht mehr auf dem Ball
aus. Es kostete Lilið alles an Kraft, zwischen den Leuten hindurch
in die Umkleide zu ihren Spinds zu gehen, wo sie ihre
Sachen bewacht und eingeschlossen zurückgelassen hatten. Sie traute
keinem Schloss und holte deshalb trotzdem als erstes
das Buch hervor, um festzustellen, ob
es noch da war.

Maruschs Blick wanderte über ihre Hände, die es hielten
und verweilten darauf. "Was ist das?", fragte sie.

"Ein Buch!", sprach Lilið neckend das Offensichtliche aus. Sie entnahm
Maruschs Brief daraus. "Ein Lagerort für wertvolle Dinge.", fügte
sie erklärend hinzu.

Wer weiß, vielleicht war das Buch doch irgendetwas wert. Dann könnte
sie so davon ablenken.

Marusch nickte, mit einem Schmunzeln im Gesicht. "Ich kann nicht
leugnen, dass ich Spaß und sehr liebevolle Gedanken an dich hatte, als
ich ihn geschrieben habe." Leise fügte sie hinzu: "Und Sorge."

Lilið packte Brief und Buch wieder in die Jacke, diese in den Beutel und
folgte Marusch durch das Haupttor ins Freie. Die Nachtluft empfing sie angenehm, aber
erst, als sie den Ball und die Enge der Stadt hinter sich gelassen hatten
und irgendwo über Feldwege durch die Nacht spazierten, fühlte Lilið sich
wirklich erleichtert, hatte keine Angst mehr, doch noch
entlarvt zu werden. "Es war schön.", sagte sie.

Maruschs Hand streifte ihre. "Sehr."

Lilið sah zu, dass sich die Berührung zwischen ihren Händen
nicht wieder löste. Ihr ganzer Körper kribbelte und fühlte
sich fast so weich und unkontrolliert an, wie als das Fieber
eingesetzt hatte.
