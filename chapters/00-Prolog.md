Prolog
======

Lilith -- das war der Name einer dunklen Kreatur. Der große Obergott
hatte dieses Wesen einst heraufbeschworen und dem allerersten Menschen, Edem,
antrauen wollen, damit sie ihm treu ergeben sei. Lilith schien ihm eine gute
Wahl für eine Menschenmutter zu sein. Sie war schön und intelligent, skorsch
und zielstrebig, und voll Liebe. Aber der Obergott war noch nicht sehr erfahren
und vergaß bei seiner Wahl, dass sie nicht treu war. Lilith war nicht geneigt,
ihre Freiheit einem höheren Ziel, und sei dies auch der Anbeginn der
Menschheit, zu opfern und zahlte für ihre Freiheit einen hohen Preis: Sie
landete in der Unterwelt. Doch eine Kreatur, die so stark war, dass sie sich
dem Willen des großen Obergotts entgegenlehnte, war keine, die sich in das
Schicksal des ewigen in der Unterwelt Ausharrens fügen würde. Immer wieder
wandelt sie unbemerkt, in vielen unerwarteten Verkleidungen
im irdischen Reich und bringt das Geschlechtsgefüge durcheinander.

Etwa eintausend Jahre später an einem heißen aber stürmischen Spätsommertag
hoch oben im Norden in einem Dorf an der von Schären zerfressenen Küste wurde
ein kleines Kind zur Welt gebracht.

Die Mutter war Köchin auf einem großen Gut.
Sie war kräftig und dick, trug weit ausgeschnittene, figurbetonte Kleider,
roten Lippenstift, der je nach Stimmung adäquat geschminkt oder verschmiert
war, und eine bewusst lässig halbhochgesteckte Haarpracht. Sonst war sie
ungeschminkt und ihre Hände zeigten deutlich, dass sie gewohnt waren, Brotteig
zu kneten, Fleisch zu schneiden oder auch Torten zu verzieren und zu
musizieren.

Der Vater liebte diese Frau abgöttisch. Ihm gehörte das Gut und
noch so einiges an Grund, sowie waren ihm die Frau und eine Menge Gefolge
schutzbefohlen. Das hieß, er schützte sie mit seiner Macht und Magie und sie kamen im
Gegenzug Verpflichtungen für ihn nach. Er hatte bereits zwei Frauen geehelicht
-- und beerdigt. Beide ausgestattet mit exquisit ausgebildeten magischen
Fähigkeiten, aber es waren keine Liebesbeziehungen gewesen und auch anderweitig
keine innigen. Es waren Beziehungen fürs Geschäft gewesen und sie hatten ihn
gelangweilt, nicht mit Lebenssinn erfüllt.

Er hatte kein drittes Mal geheiratet, nicht offiziell. Aber für seine
Köchin fühlte er innige Gefühle auf jedweder Ebene, die ihm wichtig
war, mehr als er je für eine seiner Frauen zu empfinden auch nur erträumt
hätte. Er hatte einen anderen Vertrag mit seiner
Köchin. Er hatte sie umworben, ein riesiges Fest mit ihr veranstaltet und sie
hatten insgeheim eine Ehe geführt, von der die Sakralta nichts wusste. Auf dem
Hof aber wussten von dieser Ehe alle. Nur der Hofsakralet verschloss die Augen.
Der kleine Säugling nun war eins der Ergebnisse dieser Vertragsschließung und das
Dorforakel, eine alte, runzlige Person, die vom Landkreisorakel dazu auserkoren
worden war, in diesem Dorf die Namen zu vergeben, nannte dieses Geschöpf
ausgerechnet Lilith.
