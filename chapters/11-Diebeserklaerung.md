Diebeserklärung
===============

*CN: Verinnerlichte Ace-Feindlichkeit (oder etwas Vergleichbares)
gegen sich selbst, Sex, Genitalien, Dildo, Blut.*

Lilið konnte nicht lange schlafen, obwohl sie doch so erschöpft gewesen
war. Erst verarbeitete ihr Gehirn in einer nicht sehr entspannten
Traumwelt all die Ereignisse der vergangenen Tage, seit sie auf Allil
getroffen war. Dann hatte sie eine Tiefschlafphase, die wenigstens
spürbaren Erholungseffekt zeigte, als bohrende Gedanken sie unbarmherzig
weckten.

Sie hatte sich etwas vorgemacht. Es war nicht Marusch gewesen, die
anfangs eine klare Vorstellung von diesem 'es' gehabt hätte, worauf die
körperlich gierende Interaktion hinauslaufen sollte, sondern sie. Und
der Grund, warum sie sich vorgemacht hatte, es selber nicht zu wissen,
war, dass sie eigentlich innerlich schon gewusst hatte, dass sie es
gar nicht unbedingt wollte. Je näher sie dem Moment gekommen waren,
in dem in ihre Vagina etwas eingeführt würde, desto
unwohler hatte sie sich gefühlt. Ironischweise fühlte sie sich enttäuscht
darüber, es nicht durchgezogen zu haben. Oder es nicht zu mögen.

Warum? Warum hatte sie darauf zusteuern wollen? Warum wollte sie
es immer noch getan haben?

Lilið atmete bewusst tief und leise ein und wieder aus. Sie hatten sich inwzwischen
nicht mehr im Arm, aber Maruschs Körper berührte ihren großflächig. Maruschs
Atem war gleichmäßig und nur sachte verschnarcht. Lilið mochte
alles daran. Es fühlte sich vertraut und sicher an. Und zärtlich
und warm. Und als wären sie beide für einander sehr besonders und
wertvoll.

Es war eine so penetrant überall gegenwärtige Vorstellung, dass
Sex immer auf diesen Moment hinauslaufen müsste, dass ein Penis in eine
Vagina gesteckt wurde. Oder etwas anderes, wenn kein Penis zur
Verfügung stand. Alles andere war in all den Geschichten
und sogar während Liliðs Aufklärung durch ihre Mutter, die auf
sie immer so fortschrittlich gewirkt hatte, immer als Vorspiel
deklariert worden. Maximal noch als Nachspiel. Nicht als eigentliches
Spiel.

Aber Lilið war die Vorstellung davon, etwas in ihre Vagina
einzuführen, eigentlich schon immer schlecht vorgekommen. Als
sie darüber aufgeklärt worden war, hatte es sie sehr irritiert,
dass irgendwer das gut finden könnte. Aber damals hatte
sie es darauf geschoben, dass sie als Kind nicht auf
die Idee gekommen war. Oder darauf, dass sie in dem Alter noch kein
sexuelles Bedürfnis gehabt hätte. Was überhaupt nicht stimmte,
stellte sie fest. Sie hatte sich viel angefasst, aber wäre eben
nie von selbst auf die Idee gekommen, etwas einzuführen.

Als sie aufgeklärt worden war, hatte sie es mit Fingern
getan. Und fast nichts gespürt. Oder
es war ihr sogar unangenehm gewesen. Sie hatte sich das damit
erklärt, dass die Voraussetzungen für ein Genießen nicht erfüllt
wären. Sie hatte sich eingeredet, dass es bestimmt
etwas ganz anderes wäre, sobald es während einer Interaktion mit einer
Person passieren würde, für die sie empfand wie für Marusch.

Vielleicht hatte sie es daher auch jetzt ausprobieren wollen. Denn
wann würde es eine bessere Gelegenheit geben, es zu probieren,
als jetzt mit einer Person, die so vorsichtig war und die sie so
sehr anziehend fand?

Es würde keine bessere Situation geben. Es gab keine Ausreden
mehr dafür, dass sie es nicht mochte. Sie mochte es einfach
nicht. Lilið versuchte mühevoll gegen das Gefühl, kaputt zu
sein, anzukämpfen.

Das weniger dringliche Gefühl, das sie dennoch störte, war, dass
sie eigentlich gern diesen albernen Status, jungfräulich zu sein,
losgeworden wäre. Es ergab überhaupt keinen Sinn, und sie konnte
sich trotzdem nicht von diesem Konzept lösen. Das Problem hielt
sie allerdings für lösbar. Irgendwann würde sie einfach einen
Dildo benutzen, nun wissend, dass sie es nicht mögen würde, aber
es würde mit dem Wissen dann auch nicht so schlimm sein. Sie
könnte es vielleicht jetzt, da sie wusste, dass es nichts für
sie war, einfach als Experiment an sich selbst sehen: Genau zu erfahren,
wie es sich für sie anfühlte, wie ihr Körper funktionierte und
auf was er wie reagierte. Ein Experiment, das sie allein
mit sich selbst machen würde.

Nur wäre es Unfug, einen Dildo anzuschaffen, nur um sich
ein wenig auszuforschen und ihn dann nicht mehr zu brauchen. Ob
Marusch so etwas hatte und ihr leihen könnte?

Maruschs zartes Schnarchen endete im selben Augenblick abrupt, als
Lilið sich die Tür zum Besuchshaus öffnen hörte. Sie wurde
auch sofort wieder geschlossen und ein Schlüssel klickte.

Lilið glitt eilig so lautlos wie möglich aus dem Bett. Als
erstes zog sie die Jacke über. Sie fühlte sich nicht richtig
auf dem nackten Oberkörper an, aber das war egal.

Marusch schlich nackt, wie sie war, richtig Zimmertür und
linste auf den Flur. "Niemand da.", flüsterte sie. "Die Person
ist draußen geblieben."

Oder versteckt sich, dachte Lilið. Aber die Tür war zu kurz offen
gewesen, als dass eine Person hätte hineinhuschen können. Und
warum sollte sich eine Person einschließen? Es war wahrscheinlicher,
dass einfach Dienstpersonal auf einem Rundgang kontrolliert
hatte, ob abgeschlossen war.

Trotzdem machte es Lilið nervös, dass Marusch sich das Kleid wieder
überstreifte, besonders als sie sich im Stoff verhedderte. Lilið sah
im fahlen Morgenlicht, dass durch die Ritzen in den Vorhängen und
durch diese hindurchdrang, was zu tun war und half ihr.

"Danke!", sagte Marusch und fing an, ihre Sachen in einen Beutel
zu stopfen.

Auch das machte Lilið nervös, aber sie verstand, dass es ungut wäre,
Maruschs Unterdecke, auf der sie vielleicht Blutspuren hinterlassen
hatte, oder das benutzte Handtuch zurückzulassen. Mit Blut wären
sie leichter auffindbar, gegebenenfalls Bilder von Lilið
rekonstruierbar. Hoffentlich hatte sie nicht noch anderswo
welches hinterlassen. Lilið beschloss, statt einfach halb panisch zu
warten, die Zeit, die Marusch zum Packen brauchte, damit zu
überbrücken, selbst zu packen.

Aber als sie ihre Anzughose in die Tasche schieben wollte, hielt
Marusch sie auf. "Zieh die schnell an. Du glaubst nicht, was für
einen Vorteil gute Kleidung am Körper darstellt, wenn es ums
Ausreden erfinden geht.", empfahl sie.

Lilið diskutierte nicht lange. Marusch kannte sich besser aus. Als
sie mit leicht zittrigen Fingern den letzten Knopf verschloss, machte
Marusch noch das Bett. Lilið seufzte innerlich, aber es war nur ein
Handgriff. Lilið war schleierhaft, wie Marusch es hinbekam, dass mit
einer zackigen Bewegung die Bettdecke ins Segeln kam und sich
perfekt glatt hinlegte, als hätte hier niemand geschlafen. Wenn sie
es jetzt schafften zu fliehen, würden sie es also ohne sofort
offensichtliche Spuren tun.

Lilið schob vorsichtig die Vorhänge ein Stück beiseite, um zu
sehen, ob die Luft rein war. (Dieses Mal ging es um Draußenluft,
da ergab das schon Sinn.) Es sah alles ruhig und still aus. Sie
winkte Marusch und schob das Fenster vorsichtig auf, und die
hereindringenden Geräusche verhießen, dass es doch nicht so ruhig
und still war, wie es den Anschein erweckt hatte. Überzeugte Schritte
näherten sich dem Eingang des Besuchshauses auf dem sandigen
Weg.

"Du wurdest nicht fürs Rumstehen und Nichtstun eingestellt! Wurde hier
nun eingebrochen oder nicht?", hörten sie eine agressive Stimme dazu.

Lilið schloss, dass die Person, die das Besuchshaus abgeschlossen hatte, noch
dort stand, und eine zweite, die dazugekommen war, diese gerade
dafür tadelte.

"Raus.", flüsterte Marusch. "Leise."

Sie hörten Schlüsselgeklapper, das in Liliðs Ohren nervös und unkoordiniert
klang. Sie hob die Vorhänge beiseite und stieg vorsichtig und lauschend
aus dem Fenster. Die Wache fand den Schlüssel und steckte ihn ins Schloss.

"Hast du Angst alleine?" Die Stimme der aggressiven Person von vorher
sprach die Frage eher wie einen Fluch aus.

"Ja, Mensch!", antwortete die andere Person. "Ich finde nicht in Ordnung, dass
ich alleine nach einem Verbrecher wie dem Blutigen Master M geschickt
werde. Dieser Mann ist gefährlich!"

Lilið hielt den Vorhang für Marusch zurück, als diese ihre Röcke
raffte und ebenfalls über die Fensterbank stieg. Zum Glück war sie nicht
sehr hoch. Die Tür sprang auf und sie hörten Schritte im Flur. Marusch
wirkte, als habe sie die Ruhe weg. Sie sie beeilte sich zwar, aber machte
keinen hektischen Eindruck dabei. Als sie neben Lilið angekommen war, ließen
sie den Vorhang zurückgleiten.

"Es ist nicht einmal sicher, ob er es ist!", schrie die aggressive
Person.

"Aber es ist schon sehr wahrscheinlich!", beharrte die andere und
betonte: "Er ist auf dem Ball gewesen!"

Die andere Person ging nicht weiter darauf ein. "Los jetzt!", forderte
sie auf. "Du oben, ich unten!"

Das Fenster stand noch offen, fiel Lilið ein. Sie sollten rennen,
oder? Aber die rumpelnden Schritte, die sie nun hörte, kamen
bereits aus dem Zimmer, dass sie gerade verlassenen hatten.

Marusch zog sie neben das Fenster an die Wand.

Das hatte doch keinen Sinn, dachte Lilið. Selbst ohne eine Ausbildung zur
Wache wäre es ihr erster Schritt, aus dem Fenster nach links und
rechts zu schauen. Es sei denn, sie hätte Angst. Aber die ängstlichere
Person war ja oben.

Sie hörte das verräterische Geräusch von Vorhanggleitern in ihrer
Nut. Kannte sie Maruschs Körper nach der Nacht gut genug, um sie zu
falten? Es gab wenig Zeit, um darüber nachzudenken. Sie drückte
Marusch in einer fließenden Bewegung gegen die Wand, berührte sie
dabei mit den Händen an den Hüften und schmiegte ihren ganzen
Körper gegen ihren, dieses Mal nicht mit irgendwelchen erotischen
Absichten, sondern in einem Moment voller Konzentration die
Physik und Biologie ihres Körpers zu erfassen und zu begreifen. Flach
atmend gelangte sie in den Bewusstseinszustand für Falten, diese
angenehme Welt, die vor allem aus Euphorie fürs Verständnis
der Dinge bestand. Dann schob sie Marusch zusammen.

Sie hatte noch nie eine fremde Person gefaltet. Dafür klappte es
erstaunlich gut. Aber nicht gut genug. Sie selbst faltete sich
zu einer dünnen Holzschicht, die Marusch einfasste, mit ein
paar Kanten, die es wirken lassen mochten, als wären sie gemeinsam
einfach ein Abzug oder so etwas.

Sie hatte an ein gefülltes Astloch in ihrer Maserung gedacht, wo sie ihr eines
Auge hingefaltet hatte, und lugte daraus unauffällig zum Fenster. Die
Person, die sich aus über den Sims lehnte und umsah, trug eine Uniform, die
an die der Wachen auf Lord Lurchs Hof erinnerte. So lange wie deren
Blick auf den hölzernen Vorsprung gerichtet war, den Lilið darstellte,
der immerhin flacher war, als könnte er einen Menschen mit üblichen
Ausmaßen dahinter verbergen, konnte das doch kein Zufall sein. Lilið
merkte außerdem, wie ihr schwindelig wurde, weil sie so lange nicht
atmete.

Aber als Liliðs Hoffnungsschimmer gerade ganz erlöschen wollte,
wurde die Wache von etwas anderem abgelenkt: Ein Feuer loderte
an der ihnen nächsten Stelle in der Hecke auf, die das Gut
in einigen Meter Entfernung begrenzte, und brannte binnen
Sekunden ein Loch in sie hinein. Die Wache verlor keine Zeit,
sprang aus dem Fenster und hechtete zum nun erzeugten Durchgang in
der Hecke. Sie blickte sich noch einmal um, aber entschied sich
dann, sich hinter der Hecke umzusehen.

Das sprach dafür, dachte Lilið, dass eine Person diese Feuermagie
ausgeführt hatte, die flüchten wollte, und nicht eine der Wachen. Mehr konnte sie nicht
denken. Sie wusste, wenn sie sich nicht jetzt fließend entfaltete, dann
würde die Faltung in wenigen Sekunden auffällig auseinanderspringen, weil
sie nicht mehr konnte. Also atmete sie langsam ein, während sie
sich aus ihrer Holzform wieder in ihre eigene begab. Marusch auf eine
ebenfalls achtsame, fließende Weise zu entfalten, war interessant. Sie
spürte, wie der schnellere Herzschlag im fremden Körper dabei wieder
ruhiger wurde, und wie ihre Verständnisverbindung, für die Lilið
sehr viel Konzentration gebraucht hatte, sich wieder löste. Interessanterweise
hatte sie sie mitten während der Faltung für einen Moment gar nicht
wahrgenommen. Mehr so, als gehörte es einfach so.

Marusch bot ihr, ohne ein Wort zu sagen, den Arm an. Lilið schluckte, fragte
sich, ob das so eine gute Idee wäre, aber hakte sich ein. Als sie
sich aufmachten, das Grundstück zu verlassen, als wären sie hier
offiziell, war von der Wache hinter der Hecke nichts zu sehen. Sie
verfolgte wohl die Spur einer anderen Person, die geflüchtet war. Oder
war Marusch, an eine Wand gefaltet durch Lilið hindurch, zu Feuermagie
im Stande gewesen. Etwas, was sich später klären ließe.

---

Liliðs Adrenalinpegel beruhigte sich erst wieder, als sie sicher
eine halbe Stunde gerannt waren. Sie waren noch bis außer Sichtweite
flaniert, aber dann hatten sie beschlossen, dass Raum gewinnen die
beste Entscheidung wäre. Es war gut möglich, dass die Wachen zwar
noch nur zu zweit waren und daher nicht koordiniert genug, alle
Spuren zu verfolgen, aber dass sie die Verfolgung wieder aufnehmen
würden, sobald sie mehr wären.

Sie waren einen Zickzackkurs durch die Wälder und Felder gerannt
und waren nun wieder an der Küste angekommen. Einer Steilküste
dieses Mal. Ein Stück von ihnen entfernt gab es
eine steile Holztreppe, die sie mit labberigen Beinen hinabkletterten, um
sich unterhalb der abgebrochenen Landkante auf dem schmalen, steinigen
Strand umzuziehen. Lilið zog die einzige Alternativkleidung an, die
sie neben dem Anzug dabei hatte. Marusch bekleidete sich mit einem ärmelfreien Oberteil
aus festem Stoff und einem praktischen, blauen Rock mit Taschen. Er
glänzte nicht, fiel Lilið auf, sondern war eher von einer Beschaffenheit, die
viel aushalten konnte.

Weil sie immer noch aus der Puste waren, ließen sie sich am oberen Rand des
Strands nieder, wo sie sich an die sandige Wand hinter sich lehnen konnten.

"Faltungsmagie.", kommentierte Marusch. "Ziemlich ausgereifte."

"Wie war es?", fragte Lilið.

Vor ihnen im heute leise rauschendem, ruhigem Meer paddelten Ockdrachen
im Wasser, quakten und spielten miteinander, indem sie sich
immer Mal wieder mit Wasser oder Feuer anspien und Wettrennen
veranstalteten. Irgendwo am Horizont schwamm gemächlich
eine Insel vorbei.

"Schön.", sagte Marusch nach einer Weile leise. "Ich habe mich
auf eine Weise verstanden gefühlt, wie nur Magie schön sein
kann."

"Bist du gut in Magie?", fragte Lilið. "Warst du das mit dem Feuer?"

Marusch grinste und schnaubte gleichzeitig freundlich. "So ein
Feuer ist eigentlich klassisches Mittelschulniveau.", sagte sie. "Ja,
das war ich."

Lilið versuchte sich an die Schule zu erinnern. Ja, die besseren
in der Klasse hatten so etwas hinbekommen. "Ich war in Magie immer
schlecht."

Marusch gluckste und schüttelte den Kopf. "Du hast einen fremden
Körper gefaltet und willst mir weiß machen, du wärest schlecht?"

"In der Schule haben sie anderes verlangt." Lilið zuckte mit
den Schultern. Feuer und Gefrieren zum Beispiel, erinnerte
sie sich. "Du hast immer noch nicht beantwortet, ob du gut bist."

"Ich mag gern die Theorie dahinter, die ja auch mit zu Magie
gehört. Das Verständnis.", erklärte Marusch. "Darin bin ich
gut. Ich führe Magie nicht gern aus. Feuer- und Wärmemagie noch am
häufigsten. Ohne Feuer anzünden zu müssen, Tee aufwärmen und
kochen zu können hat schon Vorteile."

Lilið nickte. Sie erinnerte sich daran, dass Marusch damals in
der Teeküche das Teewasser zum Kochen gebracht hatte. "Bist
du der Blutige Master M?", fragte sie.

Maruschs Blick haftete deutlich amüsiert auf ihr. "Nein."

"Bist du hinter ihm her und deshalb auf dem Ball gewesen?", fragte
Lilið.

Marusch kicherte, aber reagierte nicht sofort.

Lilið verstand das Amusement nicht und es machte sie allmählich
sauer. "Kannst du mir dein Gekichere erklären?"

Sofort war Marusch wieder ernst. "Magst du mir das Buch
zeigen?"

Liliðs Gefühle beschäftigten sich noch damit, nun am liebsten
eine Trotzreaktion zeigen zu wollen, als ihre Gedanken parallel
dazu zu begreifen begannen. "Du glaubst, dass das Buch der
Wertgegenstand ist, den der Blutige Master M gestohlen haben
soll?", fragte sie und fügte folgernd hinzu: "Und dass ich
der Blutige Master M wäre?"

"Ich bin nicht ganz sicher, aber halte es für möglich, ja.", antwortete
Marusch auch noch.

"Die Theorie hat ein paar Haken!", gab Lilið zu verstehen. "Der
Gegenstand wurde erst einige Tage gestohlen, nachdem du
in der Teeküche aufgetaucht bist."

"Inwiefern ist das ein Haken?", fragte Marusch.

Das war doch offensichtlich, dachte Lilið, aber einen
Moment später verstand sie, dass Marusch eine entscheidende Information
fehlte. "Ich hatte das Buch zu dem Zeitpunkt schon.", informierte
sie.

"Hm.", machte Marusch. "Das ist in der Tat ein Haken."

Lilið grinste triumphierend. Sie entnahm ihrem Gepäck ihre
Trinkflasche, aus der sie etwa die Hälfte des Rests trank, der
noch übrig war.

"Aber du hast das Buch doch geklaut?", fragte Marusch.

Lilið verschluckte sich, hustete und versuchte, die wertvolle
Flüssigkeit trotzdem nicht auszuspucken. Ach, was soll's, dachte
sie. Sie holte das Buch aus ihrer Jacke, entnahm ihm den
Brief, damit er Marusch nicht ausversehen in den Sand fiele,
und reichte es ihr.

Marusch nahm es entgegen und strich darüber, in einer Weise, wie
Menschen das tun, die Bücher lieben und zu ihnen Bindungen
aufbauen. Dann schlug sie es behutsam auf. Sie sah sehr schön
dabei aus, fand Lilið. Sie nickte. "Das ist der wertvolle
Gegenstand, um den es ging.", teilte sie mit.

Lilið hob eine Augenbraue. Sie erinnerte sich daran zurück,
wie sie das Buch gestohlen hatte. "Ich habe es aus
einer in eine Karrustra eingebauten Kiste gestohlen, in
der manchmal wertvolle Ladungen zwischen dem Haupt- und Zweitwohnsitzes
meines Vaters hin- und hergefahren werden.", berichtete sie. "Ich
hatte nie die Absicht, etwas sehr Wertvolles zu stehlen. Es passiert
lediglich manchmal, dass nach einem Transport etwas darin zurückgelassen
und vergessen wird. Das lasse ich dann mitgehen für meine
Sammlung."

Marusch grinste sie an. "Nun, dieses Buch hier ist jedenfalls
aus der Sammlung des Schatzes der Monarchie."

"Und jemand war hinter dem Gegenstand her, was bekannt war, weshalb er
an einen sichereren Ort verlegt werden
sollte.", fügte Lilið hinzu. "Das Ganze ergibt für mich aber
noch nicht so richtig viel Sinn. Der gestohlene Gegenstand ist immerhin
erst Tage, nachdem ich das Buch gestohlen habe, als gestohlen gemeldet
worden. Wenn es das Buch gewesen wäre, dann wäre es doch bei dem Versuch
aufgefallen, das Buch aus der Karrustra in den Tresorraum zu bringen."

"War in der Karrustra noch ein anderer Gegenstand?", fragte Marusch. "Vielleicht
wussten die, die etwas in den Tresorraum gebracht haben, nicht, um was
für einen Gegenstand es sich handeln würde."

Lilið wollte verneinen, aber kicherte dann. "Das Buch hat in meine
Brotdose gepasst. Also habe ich mein schimmeliges Brot zurückgelassen.", erklärte
sie. "Glaubst du, ein buchförmiges Pausenbrot geht als Teil des Schatzes
der Monarchie durch?"

Marusch lachte mit. Es war ein so schönes Lachen, Lilið hätte sie am liebsten
dafür noch einmal geküsst. Vielleicht gleich. "Ich glaube, das wäre
schon aufgefallen.", sagte Marusch. "Rätselhaft alles." Sie warf
einen erneuten Blick in das Buch. Die schulterlangen, schwarzen, festen
Haare fielen dabei schön um ihr beiges Gesicht und glänzten in
der Sonne.

"Wobei ich es in ein Taschentuch eingeschlagen habe.", fiel Lilið
ein.

"Und auf das Taschentuch hast du nicht zufällig zuvor mit Blut unförmige
Zeichen hinterlassen, die mit fiel Mühe als das Zeichen für Master und
ein M durchgehen könnten?", fragte Marusch mit einem verspielten, alberigen
Grinsen im Gesicht.

Wieder hatte Lilið den Wunsch Marusch zu küssen. Aber sie wurde
rasch abgelenkt, als ihr klar wurde, dass sie die Frage nicht
klar mit 'nein' beantworten konnte. Ihr wurde unangenehm
heiß, als sie antwortete: "Es war schon ziemlich blutdurchtränkt."
Und weil es in der Form, in der sie es festgebunden hatte, Falten
gehabt hatte, mochte vielleicht so ein Zickzack wie ein M
entstanden sein. Auch das andere Zeichen bestand vor allem aus
geraden und schrägen Linien. "Es ist nicht ausgeschlossen. Ich
habe das Taschentuch als Verband genutzt."

Marusch schüttelte kichernd den Kopf. "Was für eine kuriose
Hintergrundgeschichte für die derzeit meist gesuchte Diebesperson
der Monarchie.", sagte sie.

Lilið schluckte. "Meistgesucht?"

"Kennst du euren Tresorraum?", fragte Marusch.

Lilið nickte. Ihr wurde schwindelig. Ja, wenn die Annahme wäre,
dass eine fremde Person in diesen Tresorraum gelangt wäre, und das bei
dem erhöhten Einsatz von Wachen ungesehen, verstand sie, dass der
Diebesperson grandiose Fähigkeiten zugeschrieben wurden. "Ich
bin nicht so gut.", flüsterte sie. "Was mache ich jetzt?" Sie spürte, wie
sie zitterte. "Ich sollte das Buch zurückbringen. Das war ohnehin
mein Plan."

"Du kannst dir vielleicht vorstellen, dass ich von der Idee nicht
begeistert bin.", sagte Marusch mit weicher Stimme. Sie klappte das
Buch zu, hielt es lose in den Händen und betrachtete es.

Lilið widerstand dem Impuls, es ihr einfach zu entreißen. "Du
wolltest es von vornherein stehlen.", sagte sie.

Marusch nickte. "Ich habe überhaupt durch Streuen von Gerüchten in
die Gänge geleitet, dass es an einen anderen Ort gebracht wird.", erklärte
sie. "Und du bist mir beim Versuch, es zu stehlen, zwei Mal in die Quere gekommen."

Lilið blickte irritiert. "Welche zwei Male?", fragte sie.

"Ich wollte es aus der Karrustra stehlen, als diese einen Moment
unbewacht war, als die Kutschperson sich den Schlüssel und ein
paar Wachen geholt hat, um den Gegenstand in den Tresorraum
zu bringen. Sie durfte auf der Fahrt natürlich selbst
keinen Schlüssel haben.", erklärte Marusch. "Aber als ich die Karrustra
erreicht habe, war das Buch nicht darin. Ich dachte, vielleicht wäre
sie mit einem anderen Gefährt als erwartet transportiert worden. Vielleicht
wäre diese Karrustra ein Ablenkungsmanöver. So etwas wird häufiger
beim Verlegen wertvoller Schätze gemacht."

"Müsstest du dann nicht mein Pausenbrot gefunden haben, als du nachgesehen
hast?", fragte Lilið.

"Ich habe das Schloss nicht geknackt.", widersprach Marusch. "Ich
habe eine Art Verbindung zwischen mir
und dem Buch aufgebaut. Ich kann es fühlen, wenn ich mich darauf
konzentriere. So wie du Dinge verstehen kannst, wenn du sie faltest."

Lilið nickte, runzelte aber auch die Stirn. "Daher hast du im
Besuchshaus gesucht, weil es dort war?", fragte sie.

Marusch nickte. "Aber genau lokalisieren konnte ich es nicht.", sagte
sie. "Ich nehme an, weil du falten kannst und es auf diese Art, wenn
es dicht bei dir ist, immer ein bisschen abgeschirmt für mich ist."

"Du siehst jedenfalls auch so aus, als hättest du eine Verbindung
zu dem Buch.", kommentierte Lilið. "Und ich kann nicht leugnen, dass
es sehr schön aussieht. Warum wolltest du es stehlen."

Marusch lächelte sanft. "So unwahrscheinlich das klingt, aber einfach, um
es zu lesen.", antwortete sie und strich abermals zärtlich über den
Einband. "Es ist kodiert. Ich mag Kodierungen. Ich möchte es
einfach entschlüsseln, lesen und dann zurückbringen."

In Lilið entstand ein unerwartet warmes Gefühl von vielleicht so etwas
wie Liebe für diese Person neben sich, deren harmloser Wunsch es
war, doch bloß ein Buch aus dem Schatz der Monarchie lesen zu wollen. Ein
Buch, dass sie so wertschätzend ansah und berührte, wie sie Lilið
behandelt hatte. Das konnte doch nicht so schlimm sein.

Außer, dass das Leben ihres Vaters daran hing.

"Übertrieben edle Absichten für eine Diebesperson.", kommentierte
sie, um davon abzulenken, dass sie nun gedanklich ihre Möglichkeiten
ausloten würde. "Oder Diebin? Möchtest du so genannt werden?"

"Gern!", sagte Marusch. "Ich glaube, ich habe mich in den vergangen
Tagen, auch wenn ich selten mitbekommen habe, dass es es passiert,
dass Leute so über mich reden, sehr wohl mit Femininum
und 'sie' gefühlt. Vielleicht trage ich
auch deshalb gerade einen Rock, damit du gedanklich dabei bleibst."

"Kannst du meine Gedanken lesen?", fragte Lilið alarmiert.

Marusch glückste und schüttelte den Kopf. "Nein. Ich erinnere mich nur
daran, was ich dir gesagt habe."

"Dann kannst du ja nun einfach was anderes sagen.", sagte Lilið. Wobei
sie sich fragte, ob es wirklich so einfach wäre, oder ob da auch
verinnerlichte Selbstverleugnung in Marusch stecken mochte, wie
Lilið sie in anderem Zusammenhang heute morgen in sich gefunden
hatte. "'Sie' und Femininum also. Ich finde, das steht dir. Generell."

Das Lächeln, das nun in Maruschs Gesicht trat, war ein breites und
unverkennbar glückliches. "Danke, Lilið.", sagte sie. "Und du? Formen
weiß ich, am liebsten neutrale bei dir. Aber hast du
eigentlich ein liebstes Pronomen?"

Das klappte überhaupt nicht, stellte Lilið fest, sich während eines
Gesprächs mit Marush über Pläne Gedanken zu machen, jetzt, da sie
den Wertgegenstand gefunden hatte. Würde Marusch sich überzeugen
lassen, das Buch wieder abzugeben? Nachdem sie so viel Mühe und
Vorplanung in den Raub investiert und mehrfach ihr Leben riskiert
hatte, wohl nicht.

"Ich finde die Frage sehr schwierig.", antworte Lilið, damit
Marusch nicht zu lange auf eine Antwort warten musste. "Ich
bin sehr gewöhnt an 'sie', denke ich. Ich glaube, ein anderes
Pronomen würde mich mehr irritieren, als dass es mir etwas bringen
würde. Aber ich mag nicht, dass Menschen damit 'weiblich' verknüpfen,
oder es mir zuweisen, weil sie mich so einordnen. Ein anderes Pronomen
zu wählen, obwohl es mich irritiert, damit andere lernen, mich
nicht so einzuordnen und mich ich sein zu lassen, halte ich aber für
ein Rudern gegen Seeplattenströmungen. Und vielleicht sogar wie
den Versuch einer Erziehungsmethode, auf die ich keine Lust
habe."

Diese Gedanken waren noch nicht alt. Sie hatte sie noch nie
zurecht sortiert und war ziemlich überrascht über sich selbst,
es so darlegen zu können. Sie blickte zu Marusch hinüber, die
nachdenklich und vielleicht etwas traurig auf das Buch hinabgesehen
hatte, aber nun ihren Blick erwiderte.

Marusch legte das Buch in ihrem Schoß ab, eine Hand darauf gebettet
und streckte die andere aus, um Lilið tröstend über die Wange zu
streicheln. "Ich kann dich sehr gut als nicht weiblich sehen.", sagte
sie. "Aber ich verstehe das Problem."

Es fühlte sich trotzdem schön an. So erleichternd, dass
sie bei Marusch nicht in diese Kategorie gepresst wurde. Sie
ging in Gedanken noch einmal durch, was sie gesagt
hatte. "War das unsensibel mit den Erziehungsmethoden?", fragte
Lilið. "Weil du dich ja für 'sie' entschieden hast. Das wollte ich
nicht abwerten."

Marusch schüttelte den Kopf. "Das war nicht unsensibel.", versicherte sie. "Mich
irritiert ja das Pronomen 'sie' für mich nicht. Mich irritiert inzwischen
viel eher 'er'. Es hat also auch andere Gründe in meinem Fall, als
Leute dazu zu bewegen, mehr mich zu sehen. Wobei letzteres vielleicht
auch in Ordnung ist. Vielleicht ist die Idee mit Erziehungsmethoden
nicht völlig verwerflich, aber du darfst für dich und deine Entscheidung
dazu trotzdem dieses Gefühl haben." Marusch
grinste ein wenig verlegen. "Ich kann mich nicht gut ausdrücken. Vielleicht ist
es auch ein schwieriges Thema. Jedenfalls warst du mir gegenüber
nicht unsensibel."

Lilið griff nach der Hand, die sich gerade von ihrem Gesicht
wegbewegen wollte, zog sie zu ihrem Mund und küsste sie sanft. Diese
zarte Haut. Dieses Stolpern im Atem der schönen Person neben sich,
das sie damit verursachte.

Sie musste sich konzentrieren! Hatte sie eine Möglichkeit, einen
Kampf gegen Marusch zu gewinnen?

Auch das war unwahrscheinlich. Marusch hatte zusammengefaltet ein
Feuer verursacht. So etwas konnte Lilið nicht. Selbst wenn Marusch
nicht viel mehr können mochte als das, wäre sie Lilið wahrscheinlich
in einem Kampf überlegen.

Lilið konnte sich vor allem verstecken, Schlösser knacken und
stehlen. Die Lösung war also wohl, Marusch in Sicherheit zu
wiegen, dann in einer der kommenden Nächte das Buch zu stehlen
und abzuhauen.

Sie blickte Marusch an und fragte sich, ob sie das dringend
genug wollte. Sie wollte diese Person nicht verlassen. Was
Marusch wohl mit ihr täte, wenn Lilið ihr das Buch abgenommen hätte,
ihrem Vater zurück gebracht hätte und Marusch dann wieder finden
würde?

Marusch hob die Augenbrauen und schmunzelte, was ein
unverschämt charmantes Bild abgab. "Du überlegst, ob du dir
das Buch von mir zurückstehlen kannst?"

Lilið fühlte einen heißen Schauer über ihren Rücken und in
ihre Eingeweide rinnen. Es hatte keinen Sinn Marusch etwas
vorzumachen. "Und du kannst wirklich keine Gedanken lesen?"

Marusch kicherte. "Du hast gemeint, du möchtest es
zurückbringen.", sagte sie. "Ich kann nicht in deinen Kopf
gucken, aber ich kann mir Zusammenhänge zusammenreimen, wenn
ich genug Anhaltspunkte habe. Du hast das Gespräch ein wenig
abgelenkt und kein weiteres Mal deine Pläne angesprochen, eben
dies zu tun, obwohl du beim Aussprechen derselben sehr überzeugt
geklungen hast. Und Lord Lurch ist dein Vater. Er wird in die
Verantwortung gezogen, wenn es nicht wieder auftaucht. Das
legt nahe, dass es dir dringlich genug sein könnte, es mir
wieder abzunehmen."

Lilið nickte. "Du hast recht, so schwer zu schließen ist das
nicht." Sie seufzte. "Du hast kein Interesse an Lord
Lurchs Sicherheit, nehme ich an?"

Marusch schüttelte den Kopf. "Aber an deiner.", fügte sie
so sanft und weich hinzu, dass es Lilið durch Mark und
Bein rann.

Sie verzog irritiert das Gesicht in ein Runzeln. "Werde
ich auch in die Verantwortung", sie unterbrach sich und
stockte. "Ja, werde ich. Also, wenn ich erkannt werde. Weil
ich der Blutige Master M bin." Nach kurzer weiterer
Überlegung fügte sie hinzu: "Und sie haben mein Blut. Wahrscheinlich
ist es nicht mehr gut genug geeignet gewesen, um mich
zu rekonstruieren, weil ich sonst erkannt worden wäre, aber
es ist wahrscheinlich ausreichend, um nachzuweisen, dass ich
es bin, wenn ich erwischt werde."

Marusch nickte und reichte ihr das Buch. "Wir werden eine
Weile bis Nederoge brauchen, weil du nicht einfach auf einer
Reisefragette oder -kagutte mitsegeln kannst.", sagte sie. "Sie
werden regelmäßig durchsucht, damit niemand unbefugt mitreist, und
es ist zwar zu einer geringen Wahrscheinlichkeit möglich, dass
wir es trotzdem schaffen, unentdeckt mitzureisen. Aber wenn wir
entdeckt werden, wärest du todgeweiht. Wir werden anders
reisen müssen."

"Fliegen lernen?" Lilið lachte, wurde aber sofort wieder ernst. Ihr
wurde erst nach und nach bewusst, was es bedeutete, die meist
gesuchte Diebesperson der Monarchie zu sein. "Wir müssen ein
eigenes kleines Segelboot nehmen, meinst du das?"

Eigentlich hätte Marusch nicht zu antworten brauchen. Lilið
kannte sich nicht im Diebesgeschäft aus, aber soweit dann schon,
dass gesuchte Diebespersonen sich auf diese Weise unbemerkt fortbewegten.

Marusch nickte. "Du wolltest Nautika werden.", erinnerte sie. "Meinst
du, du bekommst uns zurück nach Nederoge navigiert?"

"Es wird eine Reise über etwa zwei Wochen werden.", überschlug
sie. "Wenn das meiste gut geht. Ob es dann noch nicht zu spät
ist?"

Marusch schüttelte den Kopf. "Der Blutige Master M ist hier gesichtet
worden. Das heißt, der Fall erscheint nicht aussichtslos. Es
wird vermutlich noch mindestens einen Monat gesucht, bevor
überhaupt in Erwägung gezogen wird, deinen Vater zur
Rechenschaft zu ziehen.", versicherte Marusch. "Es wäre sonst
Verschwendung von Einsatzkräften. Solange er nur unter Druck steht, aber
noch all seine Ressourcen hat, kann er den Blutigen Master
M besser suchen lassen. Ziel ist es ja in erster Linie, das
Buch wieder aufzutreiben."

"Aushändigen ist keine Option?", fragte Lilið.

Marusch konnte sich davon nicht abhalten, einen Moment belustigt zu
wirken. "Du bist wirklich noch nicht lange in der Diebesszene.", sagte
sie. "Das wird dir nicht einfach verziehen, wenn du es zurückgibst. Und
wenn es nicht von Leuten deines Vaters gefunden wird, hat
er trotzdem Probleme. Bei einem Diebstahl dieses Kaliebers musst du auch, nachdem es
wieder an Ort und Stelle ist, eine Weile untertauchen.", erklärte
sie, und fügte hinzu: "Am besten wäre, es im Tresorraum unterzubringen, wo
es eigentlich hätte sein sollem, und zu hoffen, dass die
Beteiligten, weil dein Vater davon profitieren
würde, eine Geschichte erfinden, die besagt, dass es doch nie weggewesen
und alles ein Irrtum gewesen wäre, glaube ich. Aber das ist auch nur
ein spontaner Plan. Vielleicht fällt uns auf dem Weg noch etwas besseres ein."

"Du würdest für mich deinen Traum aufgeben, dieses Buch zu lesen?", versicherte
sich Lilið.

"Ich habe ja jetzt erst einmal mindestens zwei Wochen, um es zu
Entschlüsseln." antwortete Marusch. "Und wenn ich es nicht schaffe, dann
stehle ich es lieber irgendwann später wieder, auch wenn das dann viel
schwieriger sein wird, als dich zu verlieren."

"Das klingt übermäßig romantisch und melodramatisch.", kommentierte
Lilið.

Marusch schmunzelte und grinste. "Ich mag dich eben."

Lilið betrachtete sie lange eingehend, aber das Schmunzeln wich
nicht aus Maruschs Gesicht. "Ich dich auch.", flüsterte sie.

"Und? Ziehen wir die Sache durch?", fragte Marusch.

Nun schmunzelte Lilið. Sie lehnte sich an Marusch und genoss den
Geruch, der ihr dadurch wieder in die Nase strömte. "Ich mag es
nicht, übereilte Entscheidungen zu fällen, und dies ist eine große,
für die ich gern mehr Zeit hätte.", sagte sie. "Aber vorerst
gehe ich diese Diebeserklärung ein. Ich denke,
gerade ist es durchaus wichtig, eine eilige Vorentscheidung zu
treffen und Land zu gewinnen. Oder viel mehr Wasser."
