Weg
===

*CN: Gewitter - ein schlimmes Fantasy-Gewitter, Lebensgefahr, Verletzung*

Ihr Vater hielt Lilið nicht auf. Die Sache war vorerst geklärt. Als durchaus
wichtige, einflussreiche Person hätte er sich wahrscheinlich trotzdem gegen
eine Aufnahme seines Kindes im Internat für skorsche Damen auf Frankeroge
stellen können, aber dazu hätte er es wollen müssen. Lilið glaubte nicht, dass
es gerade Sinn hatte, ihn davon zu überzeugen, es zu wollen. Ein Versuch wäre
sinnvoller, wenn er gerade an etwas anderes dachte und nicht so spontan
Argumente oder wahlweise auch Sturheit parat hätte.

Sie hatten noch geklärt, dass es nicht sofort losginge, sondern
dass Lilið einen knappen Monat Zeit hätte. Lord Lurch hatte
also abgesegnet, dass es kein Problem wäre, wenn sie eine Woche
bei ihrer Mutter wäre. Vielleicht würden sie dann gemeinsam
zurückkommen. Ihr Vater vermisste ihre Mutter sehr.

Lilið bemerkte eher, als dass sie es bewusst tat, dass sie
nicht nur Sachen für eine Woche Aufenthalt bei ihrer Mutter
packte. Zumal sie Ersatzkleidung und Pflegeutensilien auch
bereits drüben hatte. Sie packte auch Dinge, die ihr sehr wichtig waren, für
den Fall, dass sie nicht wiederkäme. Sie hatte keine konkreten
Fluchtpläne. Aber sie hielt für möglich, auf ihrem Weg oder
bei ihrer Mutter welche zu entwickeln.

Für den Weg benötigte sie durchschnittlich dreieinhalb Stunden. Ihr
Vater oder auch Bedienstete hatten längst aufgegeben, darüber den
Kopf zu schütteln, dass sie diese Strecke üblicherweise einmal im
Monat zu Fuß zurücklegte. Aber neben Musik half eben auch spazieren dabei,
ihren Kopf zu sortieren und sie wieder freier atmen zu lassen.

Dieses Mal brauchte es ganz schön lange, bis ihr Gehirn nicht mehr
alles, was in diesem Gespräch unangenehm gewesen war, in
Worte zu fassen versuchte, die sie besser verstehen konnte, als dieses
vage Gefühl. Der Versuch, Worte zu finden, war heute auch nicht erfolgreich. Nicht
Sortiertheit in ihrem Kopf brachte sie zurück in die Wirklichkeit. Sondern
die Abenddämmerung gepaart mit grauen Wolken, die sich über den Himmel
schoben. Sie befand sich auf dem Feldweg genau zwischen dem Wald, aus dem
sie gekommen war, und der Strauchplantage, die wieder Sicherheit bieten würde, als
sie realisierte, dass es ein Gewitter war, was sich da über sie ergießen
würde. Eines der weniger harmlosen, die sie in ihren Fingern kribbeln
spürte und von denen sie wusste, dass sie ihnen eigentlich besser nicht
unter freiem Himmel begegnen sollte.

Lilið war nicht der Typ Mensch, der sich mit fürchten aufhielt, wenn
es nichts zu entscheiden gab. Hier gab es für sie nur die Möglichkeit,
zu rennen und zu hoffen, dass sie die Plantage rechtzeitig erreichen würde. Und
zu genießen. Was sollte sie sich mit Gedanken an Lebensgefahr aufhalten,
wenn sie die volle Atmosphäre des schwach bunten Geflackere unter den
Wolken, des Donnerns und Krachens und der Regenflut in sich aufsaugen
konnte. Binnen Sekunden war sie durchnässt. Sie streifte die Schuhe von
den Füßen, weil die Sandalen mit dem Wasser darin weniger stabilen Halt
auf dem Boden boten und sie damit langsamer war. Ihre nackten Füße
patschten über die flache Strömung, die sich auf dem sandigen Weg bildete und
sich ihren Weg irgendwo hinab bahnte. Leider waren auch immer wieder
kleine Steinchen auf dem Weg. Immer, wenn sich einer davon in Liliðs
Fußsohle bohrte, hielt sie einen Moment die Luft an. Das regulierte
den Schmerz, sodass sie ihn nur einen bruchteil eines Moments
fühlte. Dann atmete sie wieder bewusst durch und fühlte sich, als wäre
nichts gewesen.

Aber kurz bevor sie die Plantage erreichte, ließ sie doch einmal
ihr volles Körpergewicht in einen besonders spitzen Stein sinken, weil
sie ein lautes Krachen eines Blitzeinschlags hinter ihr davon
ablenkte, den Fuß um den Stein herum zu belasten. Er blieb auch für zwei
weitere Laufschritte in ihrer Sohle stecken und bohrte sich beim
Aufkommen noch tiefer hinein. Sie überlegte rasch, ob sie schneller
wäre, wenn sie kurz anhielte, um ihn aus dem Fuß zu ziehen, weil er
nicht selber herausfiel. (Seltsamer Stein, aber auch solche kamen
halt alle paar Jahre mal vor.) Aber sie entschied sich, leicht humpelnd
zu rennen, mit dem verletzten Fuß auf Zehenspitzen.

Es gab keinen weiteren Einschlag in ihrer dichteren Umgebung, bis
sie die ersten hohen Buschranken der Plantage erreichte. Es gab sogar
einen großen Stein, auf dem sie sitzen konnte, um sich den kleinen
spitzen Stein aus dem Fuß zu entfernen. Es war in der Tat ein seltsamer
Stein. Er hatte eine flache untere Seite und nach oben hin so etwas wie
einen Stachel. Der Fuß sah überraschend übel zugerichtet aus. Da war
wohl doch eine Menge Adrenalin in ihrem Blut gewesen, dass sie den
Schmerz nur schwach gespürt hatte. Sie spürte ihn auch jetzt kaum, aber
das war bei ihr normal. Sie hatte ein recht geringes
Schmerzempfinden. Trotzdem verschnaufte sie eine Weile, genoss
das Brennen des Atems und die Feuchte, die den Staub aus der Luft
wegspülte und den Geruch der Umgebung anpasste. Sie beobachtete
das Wetterspektakel, das so völlig unbeeindruckt von beliebigen,
möglichen menschlichen Wünschen über sie hinwegzog.

Als es nur noch tröpfelte, brach sie wieder auf. Und stellte fest,
dass dies ein Nachtmarsch werden würde. Sie unterbrach den Weg
gleich nach wenigen Metern noch ein weiteres Mal, um ihren Fuß
mit einem Stofftaschentuch zu verbinden, das dazu mäßig
gut geeignet war. Es war eher zu klein, sodass
es nur durch einen winzigen, friemeligen Knoten um den Fuß
befestigt werden könnte und vermutlich alle Nase lang abfallen
würde. Sie verzichtete außerdem darauf, mit der Verletzung
aufzutreten, selbst wenn nun neben dem Taschentuch auch wieder
eine Schuhsohle zwischen Fuß und Boden war. Schon nach einer
weiteren halben Stunde Spazierweg, als sie die Plantage
passiert hatte und nun wieder Felder ihren Weg einfassten, bemerkte
sie, dass sie schief ging und deshalb ihre Beckenmuskulatur
verkrampfte. Oder irgendwelche Muskeln im Beckenbereich.

Mit einem inneren Seufzen wählte sie einen schmalen Abzweig
zur Hauptstraße, wo die Kutschen langtuckerten. So spät am Abend
waren nicht mehr viele unterwegs, aber sie hatte Glück. Kaum hatte
sie die Straße erreicht, erblickte sie eine Karrustra ihres
Vaters, ein Gefährt, das zwischen seinen Wohnsitzen hin- und
herfuhr, um Lieferungen zu transportieren.

Lilið hielt den Daumen raus und entzündete ihn in einer
sacht glimmenden, weißen Flamme. Diesen Magietrick
lernten sie schon in der Erstschule, aber Lilið erinnerte
sich heute besonders wehmütig, wie schwer ihr gefallen war, ihn
zu erlernen.

Zu Liliðs Überraschung und Frust wurde die Karrustra nicht
wesentlich langsamer. Mehr für eine rasche Mitteilung, weniger,
um anzuhalten. Lilið behielt mit der Einschätzung
recht.

"Tut mir leid!", rief die Kutschperson aus dem Fenster hinter
der Steuerung. "Ich darf heute niemanden mitnehmen. Vielleicht
beim nächsten Mal!"

Lilið hob eine Augenbraue, was vermutlich niemand in der
nassen Dunkelheit sah. War das ungewöhnlich? Sie hatte nicht
viel Erfahrung damit, per Anhalter mitzufahren. Die zwei Male, die
sie es getan hatte, war es noch heller gewesen und sie war vom
jeweils ersten Gefährt mitgenommen worden.

Sie senkte gerade enttäuscht den Daumen und wollte sich damit
abfinden, dass so etwas wohl vorkam, wobei sie sich schon
fragte, warum, als die Karrustra doch hielt.

Die Kutschperson stieg aus und erzeugte einen kurzen, etwas
größeren Lichtzauber, der sie und Lilið für einige Augenblicke
in milchiges Licht tauchte. "Ach, Lilið, was machst du denn
hier? Ich hätte dich fast nicht erkannt. Ist dir was passiert?"

Lilið hatte keinen Plan, wer das jetzt genau war. Und das tat
ihr furchtbar leid. Sie hätte gern das gesamte Dienstpersonal
gekannt, mit Namen und Gesichtern und Gefühlen, wenn sie letztere
hätte erfahren dürfen. Aber ihr fiel das
schwer, ein Gesicht von einem anderen zu unterscheiden, wenn sie
nicht schonmal ein längeres, intensives Gespräch mit besagtem
Gesicht im Blick geführt hatte. Und das
ergab sich nun einmal nicht mit dem Dienstpersonal. Schon gar
nicht, da Lilið keine allzu gesprächige Person war.

Lilið versuchte, jene Planlosigkeit zu verbergen. "Ja, aber
nichts Schlimmes. Ich habe mir den Fuß verletzt. Das ist
alles.", sagte sie.

"Willst du hinten mitfahren?", fragte die Kutschperson.

Ihre Stimme war warm und freundlich und wirkte, als würde sie Lilið
durchaus in guter Erinnerung haben. Die Tracht war aus schlichtem
grauem Stoff mit hellblauen Einfassungen.

Lilið zögerte. Sie wollte wirklich gern, aber sie wollte die
Kutschperson auch nicht in Schwierigkeiten bringen. "Sagtest
du nicht, du dürfest heute niemanden mitnehmen?"

Die Kutschperson lächelte. "Ich glaube, beim Kind des Lord Lurchs
ist das was anderes.", sagte sie. "Es wäre trotzdem nett, wenn
du das vorsichtshalber für dich behältst. Willst du zu deiner
Mutter? Klingt das so, als würde ich dich wie ein Kind behandeln?"

Lilið grinste und schüttelte den Kopf. "Ich will zu Muddern. Und
finde, das darf ich auch mit 19 wollen. Ich würde mich geehrt
fühlen, wenn ich hinten bei dir mitfahren dürfte."

"Da ist es gemütlicher und einsamer als bei mir vorn, was?", fragte
die Kutschperson. "Du warst schon immer gern für dich."

Lilið nickte, wieder etwas irritiert darüber, von einer Person
gekannt zu werden, die sie nicht einordnen konnte. Aber das war
nun auch kein seltenes Erlebnis.

---

Die Karrustra war nicht voll bepackt. Es lagen Stapel gut
verpackter, weicher Kleidung darin, die wohl am einen Standort
geflickt worden waren und nun zum anderen fuhren. Oder so. Lilið
versuchte, ein Verständnis für die Abläufe des Hofstaats
zu gewinnen, und sie lag zwar häufiger richtig mit ihren
Einschätzungen, aber auch immer noch sehr oft falsch.

Sie legte sich vor der Sitzbank auf den Boden und machte sich
ans Werk. Sie hatte neben Rückzug noch einen anderen Grund gehabt,
hinten in der Karrustra mitfahren zu wollen. Im Gegensatz zur
Kutschperson kannte sie die Karrustra nämlich durchaus ganz gut.
Die Sitzbank war ein Kasten, der mit einem nicht ganz trivialen
Schloss verschlossen war. Neben Musizieren und Spazieren gehörte
auch Schlösserknacken zu den Dingen, die Lilið beruhigten. Oder
vielleicht eher in einen anderen Denkmodus versetzten, ähnlich,
wie Falten das vermochte, der sie
beruhigte und ihr guttat. Dieses Schloss hatte sie schon einige
Male geknackt, wenn die Karrustra still in der Garage stand, aber
noch nie während einer Fahrt mit schlechten Sichtverhältnissen.

Sie fühlte ihren ganzen Körper und liebte dieses Gefühl, als sie
die Werkzeuge, die sie in ihrer Kleidung immer gut greifbar bei sich
trug, durch ihre Finger wandern ließ. Sie spürte die
sachte angespannten Muskeln in den Beinen und im Rücken, um gegen
das Ruckeln der Karrustra an ruhig zu liegen. Sie steckte sachte
das eine Werkzeug in den Schlitz, um das Schloss zart auf Spannung
zu bringen, und setzte dann mit dem anderen, spitzeren Werkzeug
die Stifte. Sie atmete bewusst dabei, fühlte keine starke
Emotion oder gar Frust, wenn sie ihr wieder entgegen
sprangen, weil das Fahrzeug über eine Unebenheit holperte, und
lernte allmählich die Bewegungen mit einzuberechnen. Es brauchte
sicher doppelt so lange wie beim letzten Mal, bis das Schloss
aufsprang.

Sie legte die Lade der Kiste sachte auf dem Boden ab, und
wagte, mit etwas Licht hineinzuleuchten. Bisher hatte sie die
Kiste oft leer vorgefunden, aber manchmal war auch noch etwas
Interessantes darin liegen geblieben. Ein einzelnes kleines
Edelsteinchen, das niemand vermisste, oder eine Garnrolle
mit Silberfaden etwa. Dieses Mal fand sie ein kleines, in
ein Tuch eingewickeltes Buch. Es wirkte nicht sehr besonders. Vielleicht
war es darin bei einem Buchtransport liegen geblieben. Sie
wagte nur einen kurzen Blick hinein. Sie hatte Bedenken, von
Texten gefesselt zu werden und dadurch doch bei ihrem
Vorhaben entdeckt. Aber die Schrift, die sie erblickte, wenn es
denn eine war und nicht nur Flecken, konnte sie nicht lesen. Nun, so
etwas packte sie gegebenenfalls nicht weniger. Lilið mochte durchaus
auch Rätsel und Schriften.

Kurzerhand suchte sie für das Buch einen guten Platz in ihrem
Gepäck, wo es keinen Schaden nehmen würde, und überlegte, dass
sie es ja zurückgeben könnte, wenn sie sein Geheimnis gelüftet
hätte. Der gute Platz für das Buch war ihre Brotdose, der sie ihr
Brot entnahm, das sie nicht gegessen hatte. (Aus gutem Grund, es war
da schon seit ihrer letzten Wanderung, weil sie vergesslich
war, und roch etwas schimmelig.) Sie wischte die Brotdose außerdem
zuvor gründlich aus. Das Buch passte sehr knapp hinein, was gut war, weil
es so nicht verrutschen würde. Das Tuch, in das das Buch eingeschlagen
gewesen war, passte allerdings nicht auch noch mit hinein. Es wirkte
sauber, stellte Lilið fest. Es eignete sich besser als Verband
für ihren Fuß als das zu kleine Taschentuch. Kurzerhand schlug
sie das schimmelige Brot in ihr blutiges Taschentuch und legte
beides zusammen anstelle des Buchs in die Kiste, bevor sie sie
wieder schloss und das Schloss einschnappen ließ. Vielleicht
war das nicht die freundlichste
Idee. Ihr Gedanke war gewesen, dass das Brot erstaunlich
buchförmig war. Sie stellte sich den Moment witzig vor, in dem
doch eine Person nach dem Buch suchen und stattdessen ein Brot
finden würde.

Sie überlegte einen Moment, ob sie die nun wieder verschlossene
Kiste noch einmal aufpicken sollte, um das Brot doch nicht dort
herumschimmeln zu lassen, aber sie gab die Idee rasch auf, als
sie bemerkte, dass sie nun auf Kies rollten, also das Gut des
zweiten Wohnsitzes erreicht war.

---

Ihre Mutter schlief eigentlich schon, als Lilið eintraf, aber das
blieb wie erwartet nicht so. Liliðs Mutter hatte sich noch nie
nehmen lassen, nach einiger Zeit der Trennung das Wiedersehen
ohne Verzögerung zu feiern. Was auch immer gerade anstand,
ließ sie liegen, solange es nicht gerade zu einem Brand oder so etwas
führte, es zu unterbrechen. Also bestand sie zwar darauf, dass
Lilið ihren Fuß nicht nur gründlich auswusch, sondern auch
desinfizierte, aber bereitete anschließend ohne Umschweife ein
Essen vor, während dessen Zubereitung sie erzählten. Lilið
genoss durchaus, so wichtig zu sein.

Sie feierten das Wiedersehen zu zweit in der geräumigen Küche, ein
altbacken wirkender Raum mit guter Durchlüftung. Die angenehme
Regenfeuchte kroch durch die Fenster und Ritzen herein. Lilið saß am
Tisch und zerkleinerte Zutaten, während ihre Mutter aus selbigen
einen Eintopf produzierte. Lilið brauchte keine Aufforderung, um
mit ihrem Bericht anzufangen, was los war. Aber sie erzählte nicht chronologisch, sondern
schichtweise. Zerst erzählte sie von der neuen Bestimmung ihres
Skorems und den Konsequenzen. Es fühlte sich seltsam an, nun die
Zahl 160 auszusprechen.

"Kann schon sein, dass sie recht hat, diese Frau Wolle.", kommentierte
ihre Mutter. "Ich wusste schon immer, dass du was drauf hast. Aber darum
geht es eigentlich nicht, oder? Du willst nicht auf dieses Internat."

"Überhaupt nicht.", stimmte Lilið zu.

Die nächste Schicht, die Lilið darlegte, war diese komplizierte Sache,
dass sie sich auf einem Internat für Damen nicht wohlfühlen würde. Ihre
Mutter hörte einfach zu. Aber so richtig verstand sie das wohl nicht.

"Du möchtest auch nicht Lord Lurchs Grund übernehmen, wenn er mal
nicht mehr ist, oder?", fragte sie.

Lilið runzelte die Stirn, weil sie keinen Zusammenhang sah. Trotzdem
nickte sie. "Ich möchte ein Nautika werden. Ich habe es nicht so
mit Verwaltung. Das würde ja auch dazu gehören, nicht nur Magie."

"Ich glaube, er hofft, dass du das irgendwann lernst und alles
übernimmst. Er hat sonst niemanden zum Vererben.", sagte
Liliðs Mutter. "Aber ich habe dich auch nie
in der Rolle gesehen. Ich wünsche mir, dass du das machen kannst,
wovon du träumst. Das ist in dieser Welt nicht leicht. Ich kann
mir vorstellen, dass es besonders schwierig für Außenstehende
zu verstehen ist, dass du nicht so viel mit Magie am Hut haben
willst, wo du so einen hohen Skorem hast. Aber wenn du diese
Schule nicht besuchen willst, akzeptiere ich das und helfe
dir, einen Weg drumherum zu finden."

Lilið seufzte und blickte durch das Fenster hinaus in den regennassen
Kräutergarten. Ihre Finger wanderten über die Unebenheiten der Tischplatte. An
manchen Stellen der Finger fühlte sie noch nach, dass die Einbruchswerkzeuge
in sie gedrückt hatten.

"Im Moment sehe ich als Weg nur weglaufen." Lilið hatte eigentlich
wenig Hoffnung, dass ihre Mutter das gutheißen, geschweige
denn ernst nehmen würde, aber wurde überrascht.

"Als auszubilndendes Nautika wärest du auch unterwegs. Das sind keine
sich beißenden Pläne.", erwiderte sie.

Lilið nickte und blickte auf. Es überraschte sie zusätzlich und
füllte sie mit einem angenehmen Gefühl, als sie selbst wahrgenommen zu werden, dass
ihre Mutter den Begriff Nautika für sie geschlechtsneutral verwendete, wie
Lilið es getan hatte. Den meisten Leuten fiel so etwas nicht auf. Oder
es irritierte sie, sie gingen aber nicht drauf ein und übernahmen
es nicht. Und das alles selbst dann nicht, wenn das Gespräch gerade
zuvor darum gegangen war, dass sie sich Frauen nicht zuordnete.

Ihre Mutter hörte auf, Hitzemagie auf
den Topf zu wirken, kühlte mit der flachen Hand die Henkel herunter, die
dabei leicht knisterten, und stellte den Topf auf den Tisch. "Lasst
uns eine Nacht drüber schlafen. Eine halbe zumindest."

Die dritte Schicht der Erlebnisse sparte Lilið aus. Ihre Mutter
hätte keine Macht gehabt, ihr gegen Herrn Hut und die widerliche
Erfahrung zu helfen. Also wäre es eine Belastung gewesen, bei der
ihre Mutter sich vielleicht noch mehr Sorgen gemacht hätte als Lilið selbst.

---

Viel zu früh wachte Lilið am nächsten Morgen auf, als das charakteristische
Krähen des Uhnerdrachens sie weckte. Uhner gehörten zu den domestizierten
Drachen und legten ziemlich schmackhafte Eier. Sie waren eher nicht so
gut im Fliegen, selbst wenn ihnen nicht die Schwingen gestutzt wurden, was
sie deshalb hier auch nicht taten. Es musste deshalb gelegentlich bloß mal ein
verirrter Uhnerdrache wieder von der Straße eingesammelt werden.

Lilið hätte nicht aufstehen müssen, aber wenn ihr Vater nicht in
der Nähe war, tat sie so, als wäre sie Dienstpersonal und half überall
mit. Sie hatte beide Seiten als Eltern, also wollte sie auch beide
Seiten leben. Nun, das klappte natürlich nicht so ganz. Ihr war sehr
bewusst, dass es etwas anderes war, sich an manchen Tagen auszusuchen,
den Dienst des Dienstpersonals mit zuverrichten, als es tun zu müssen,
um unter dem Schutz des Lord Lurchs zu stehen. Sie hatte den Schutz
einfach so. Das Dienstpersonal musste dafür Verpflichtungen nachkommen,
den teils lebenswichtigen Schutz genießen zu dürfen.

Wenn beispielsweise
die Kutschperson gestern Nacht einem angreifenden Menschen auf der Straße begegnet
wäre, der sie hätte überfallen wollen, hätte die Kutschperson lediglich
kurz nachgewiesen, dem Lord Lurch schutzbefohlen zu sein. Dann
hätte der angreifende Mensch gewusst, dass er seine Magie nicht
mit der der Kutschperson, sondern wahrscheinlich später mit der
von Lord Lurch hätte messen müssen. Heimlich stehlen unterband das System
allerdings nicht.

Lilið goss die Pflanzen im Haus, putzte Treppen, half bei
Feldarbeiten und später bei ein paar Reperaturarbeiten. Erst gegen
Abend kam sie dazu, die Schwinge zu spielen. Dazu wurde sie eine
Weile in Ruhe gelassen und hatte den Feierraum für sich. Trotzdem
war ihr bewusst, dass die Klänge sich durch das ganze Anwesen
ausbreiteten und es kein Geheimnis war, was sie spielte.

Später versammelten sich Teile des Dienstpersonals im Feierraum,
speisten zusammen und unterhielten sich leise und doch
ausgelassen, während sie Liliðs Spiel lauschten.

Lilið wusste nicht genau, wie es funktionierte. Emotionen setzten
sich in Harmonien um. Als sie eine gute Stunde gespielt hatte, fühlte
sie sich ruhiger, als hätte sie die Sache mit der Übergriffigkeit
doch erzählt, und wäre noch viel mehr losgeworden.
Ein Frust über die Machtlosigeit. Alles
hatte sie ohne Filter mit der Musik erzählt, die nur sie verstand.

Als sie sich zu ihrer Mutter an den Tisch setzte, die, weil sie
ja kochte, zuletzt zum Essen kam, hatte diese Nachrichten, mit
denen Lilið nicht gerechnet hätte: "Ich glaube, dein Vater vermutet,
dass du fliehen könntest. Hier werden die Wachen verstärkt."

Lilið ließ sich nicht anmerken, dass sie ein kurzes Gefühl von
Panik und Wut durchströmte, sondern nickte bloß.

"Möchtest du gehen?", fragte ihre Mutter ebenso leise wie eben. Nicht
auffällig leise, nicht so, dass es Umstehende oder -sitzende neugierig
machte. Hilfreich war, dass sich eine andere Person an die
Schwinge gesetzt hatte und leichtere Melodien klimperte, denen
ebenso ausgelassen gelauscht wurde, wie Liliðs Dramatik zuvor.

Lilið überlegte kurz, nickte dann aber wieder. "Ich denke,
zurückkommen wäre dann einfacher, als später entkommen.", mutmaßte
sie. Allein das Wort 'entkommen' fühlte sich so seltsam an.

"Dann schlaf heute Nacht im Besuchshaus.", empfahl ihre Mutter. "Das
ist nicht auffällig, weil du das ohnehin oft machst."

Das stimmte. Die Hauskatzen hielten sich besonders gern dort
auf, und es war im Besuchshaus meist ruhiger und kühler, wenn nicht gerade
Besuch da war. Sie nickte. "In Ordnung. Sagst du mir, warum?"

"Ich werde gegen zwei Uhr nachts wegen eines vermeintlichen Einbruchs Alarm
schlagen." Dieses Mal sprach sie sehr leise. "Das wird die Wachen
von dort weglocken und du kannst übers Meer fliehen. Oder einen
passenden anderen Fluchtweg finden."
