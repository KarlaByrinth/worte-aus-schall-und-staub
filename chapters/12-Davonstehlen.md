Davonstehlen
============

*CN: Vergiftung als Thema, Herzprobleme - erwähnt, Leistungsdruck, Verfolgungspanik, Küssen.*

Sie trennten sich auf, was Lilið Unbehagen bereitete, aber sie
sah die Sinnhaftigkeit. Für ihre Reise brauchten sie eine
Karte, ein Boot und Proviant. Das war das Mindeste. Die ersten
zwei Punkte auf der Liste stellten sich als viel einfacher heraus, als Lilið
für möglich gehalten hätte: Marusch selbst war mit einem Boot
und einer Karte hierher gekommen, -- und mit einem Nautika, dessen
Deal mit Marusch Mitreise gegen Dienst gewesen war. Das Segelboot
war eine Ormorane (nach einem Seedrachen benannt), ein klassischer
Bootstyp, den Lilið schon einige Male während der Ausbildung
gesegelt hatte. Es war ein Zweihandboot, ein etwas schwereres, das
auch mit stärkerem Seegang oder hackigeren Winden zurecht käme, Raum für
ein wenig Gepäck hatte, dafür aber etwas langsamer war, als was Lilið
gewohnt war. Das Boot stand in einem Plantagenbewachungshaus
unter, wo die Familie des Nautika im Dienst von Lord und Lady
Pik Feldarbeit organisierte, die Plantagen und Erträge in
der Zwischenlagerung bewachte und eben heimlich nicht nur
das. Wenn die Häuser gerade nicht für Lagerung die Feld- und
Plantagenerträge für Lord und Lady Pik gebraucht wurden,
stellten sie sie unter der Hand für Diebesgesindel und
andere Leute, die etwas außerhalb des Gesetzes lebten,
zur Verfügung. Unter gewissen Auflagen und für gewisse
Gegenleistungen zumindest. Und in einem der Lagerhallen
stand Maruschs Ormorane unter.

Marusch hatte Lilið noch hierher begleitet, sie der Familie
als ein Freundeswesen vorgestellt (ja, mit dieser Bezeichnung) und
hatte sie sich selbst und den Karten überlassen, während sie
Proviant zusammen stehlen würde. Auf Liliðs fragenden Blick
in Richtung Lebensmittellager vor Ort hatte sie erklärt, dass
es zu den Auflagen der Familie gehörte, dass sich dort nicht
bedient würde. Höchstens für den akuten Hunger. Weil bei
der Menge an gesetzlosem Volk, das hier verkehrte und längere Reisen
plante, die Verluste auffällig werden würden und auf diese
Weise dann die Tarnung auffliegen könnte.

Es gab zwei Gründe, warum Marusch Lilið hier zurückließ, statt
erst mit ihr gemeinsam Proviant zu besorgen und anschließend
gemeinsam hier Boot und Karten abzuholen: Für Lilið hätte erwischt zu werden
derzeit viel üblere Konsequenzen und Lilið würde einiges an
Zeit benötigen, um eine Reiseroute zu planen. Für die Reise
mit einer Jolle, die keine so hohe Geschwindigkeit erreichen würde
wie eine Reisefragette oder -kagutte, und um jene zu segeln
sie auch beide wach und körperlich aktiv sein mussten, mussten
sie Zwischenhalte einplanen. Die einzigen Inseln, die sich
für Zwischenhalte anboten, waren die Reiseinseln, die, wie
ihr Name sagte, reisten. Deren Reiserouten sie also beim
Navigieren einplanen mussten. Die Navigation für diese Route
mit einer Jolle war bei Weitem kein leichtes Unterfangen.

Lilið hatte zwar noch keine Ausbildung zum Nautika, aber sie
stand auch nicht am Anfang, brachte durchaus einiges an Vorkenntnissen
aus Unterricht und Eigenstudium mit.

Sie betrachtete die Karte ausgebreitet auf einem großen Tisch
in dem derzeit in eine Bootshalle umfunktionierten
Lagerraum, wo nicht nur Maruschs Boot verwahrt wurde. Der
Tisch war eigentlich nur eine große Spanplatte auf Böcken, aber kippelte
dankenswerter Weise nicht. Er stand direkt unter einem großen
Fenster mit milchigen Gläsern, durch die die Vormittagssonne
hereinschien und ihr die Arbeit erleichterte. In den Sonnenstrahlen
wirbelte gemächlich feiner Staub durch die Luft.

Lilið strich mit den Fingern über die Karte. Sie hatte eine haftende
Oberfläche, auf der das Kartensteinchen, eine kleine, schwere
Scheibe, die für das zu navigierende
Schiff stand, zwar gut bewegt werden konnte aber nicht leicht
herunter fiel oder verrutschte. Es war eine große und vielteilige
Karte, anders als die kleinen Seekarten, die sie fürs Navigieren
in der Umgebung von Nederoge standardmäßig benutzt hatte. Aber es war auch nicht
die größte Karte, die sie je gesehen hätte.

Lilið machte sich daran, sie den Daten in den Tabellen aus
dem beigelegtem Büchlein entsprechend auf den heutigen Tag
einzustellen. Sie war erst vor wenigen Tagen zuletzt benutzt
worden. Also nahm sie sie nicht komplett auseinander, sondern
verschob die Inseln über ihre Bahnen an ihre neue Position
in der Karte, immer wieder in jenem Büchlein dafür
nachschlagend. Anschließend kontrollierte sie alles noch einmal,
bevor sie mit dem eigentlichen Navigieren anfangen konnte.

Sie hatte gerade das Kartensteinchen an seine Startposition gelegt
und sich die Winkellineale und den Zirkel gegriffen, als eine
Person die Halle betrat. Lilið blickte auf und erkannte
Heelem, den Marusch ihr vorhin als den Nautika vorgestellt hatte,
mit dem sie hierhergesegelt war.

Heelem trug in jeder Hand je eine Tasse
dampfenden Getränks, nickte grüßend und stellte eine vor ihr
ab. "Bist du also der berüchtigte Blutige Master M?", fragte
er. "Oder das Blutige Master M?"

"Was?" Lilið war äußerst verwirrt und verbarg die Verwirrung
auch nicht. Hatte Marusch Heelem darüber informiert? Das konnte
sie sich eigentlich nicht vorstellen.

"Marusch hat dich als Freundeswesen vorgestellt.", erklärte
Heelem. "Ich dachte, daher ist es dir vielleicht lieber,
wenn ich nicht 'der' sondern 'das' sage. Aber im Grunde
habe ich keine Ahnung, was warum richtig wäre. Ich habe weder einen Plan, warum
Marusch gern eine Sie sein will, noch warum du Freundeswesen
genannt wirst. Ich muss es aber auch nicht verstehen, um
es einfach zu akzeptieren und umzusetzen, sofern ich eben
weiß wie."

Die Antwort ging nicht in die Richtung, die Lilið erwartet oder
gern abgehakt hatte, aber legte etwas dar, was sie um jeden Preis
hätte gesagt bekommen wollen. Genau, dachte sie: Warum beharrten
so viele Leute einfach darauf, sie als Frau einzuordnen, die
wussten, dass sie es nicht mochte, wo es doch so einfach sein
könnte, es zu lassen, auch ohne zu verstehen.

Lilið nickte. "Ich mag neutrale Bezeichnungen, etwa das
Nautika, aber auch gern erfundene. Aber wo kommt von vornherein
diese Idee her, dass ich der Blutige Master M sein könnte?"

"Ach, ich stelle gern Fangfragen." Heelem grinste. "Ein
einfaches 'nein' hätte mir gereicht. Die Idee, ja, vielleicht
kennst du Marusch ein bisschen. Sie hat Kontakte zu den finstersten
Gestalten. Es hätte ihr ähnlich gesehen, nun mit dem Blutigen Master
M hier aufzukreuzen." Heelem gestikulierte zur Tasse, aus der es
angenehm aber etwas fremd roch. "Das ist für dich! Du siehst aus,
als könntest du eine Stärkung gebrauchen."

Lilið grinste und schüttelte den Kopf. "Wie du selbst dargelegt
hast, pflegt Marusch Umgang mit recht finsteren Gestalten.", griff
sie Heelems Wortwahl auf. "Daher werde ich so schnell nichts trinken,
was mir ein Kontakt von ihr anbietet."

"Nachvollziehbar.", kommentierte Heelem. "Wenn auch schade für
dich. Es ist guter Monua."

Es roch verführerisch. Sie hatte Monua lange nicht mehr getrunken. Es
war kein regionales Getränk auf Nederoge. Vielleicht war es sogar
ihr Schulausflug auf Angelsoge gewesen, dass sie zuletzt
Monua getrunken hatte.

Lilið konnte nicht leugnen, Flüssigkeit
und etwas Stärkung nötig zu haben. Aber sie hatte nicht vor, dem
Sog eines Getränks so leicht zu erliegen.

Heelem schritt mit dem eigenen Monua durch den Raum und sah sich
die Boote an. Vielleicht war das Gespräch vorerst beendet, also wandte
sich Lilið wieder der Karte zu. Sie fühlte sich abgelenkt durch
Heelems Anwesenheit, weil sie wusste, dass sie für größere
Abschnitte der Routenplanung einen konzentrierten Lauf bräuchte, der
hinfällig und ohne neue Erkenntnisse wäre, sobald sie dabei
unterbrochen werden würde.

"Darf ich dich nochmals stören?", fragte Heelem.

Lilið wandte sich ihm wieder zu. "Klar!", sagte sie. "Irgendwann
brauche ich einen Lauf, oder mehrere, da wäre das ungünstig, aber
ich bin noch nicht im Tunnel."

Heelem grinste. "Oh, das kenne ich gut. Daher frage ich!"

Auch das erfüllte Lilið mit Freude. Dass er es kannte, einen
Lauf zu brauchen oder im Tunnel zu sein, als Nautika. Lilið
hatte etwas Sorge gehabt, dass das etwas war, was nur unerfahrene
Leute benötigten, und dass geübtere oder gar professionelle Nautikae
jederzeit unterbrechen könnten.

Heelem hob die Hand und machte eine Geste, die langsam
eine Linie von oben nach unten machte. Er nickte mit dem
Kopf Richtung Tür der Halle.

Lilið sah, dem Nicken folgend, zur Tür, die gerade lautlos
aufgespalten wurde. Das war Einwirkung ziemlich fortgeschrittener
Magie, erkannte Lilið. Von der Mitte ihrer oberen Kante ausgehend
trennte sie sich an einer Holzmaserung entlang auf, und die Seite
der Tür, die nicht in den Angeln hing, rollte sich dabei ein
wie ein Sägespahn.

Heelem rollte sie bis zur Hälfte zu einer Rolle und kehrte dann den Vorgang
um. Es blieb nicht einmal eine Naht zurück, als sie sich
wieder zusammenfügte. Lilið blickte sich wieder
zu Heelem um und begutachtete, dass er dabei zwar konzentriert
aussah, aber nicht übermäßig angestrengt. Das konnte natürlich
auch täuschen. Manche Menschen konnte Anstrengung gut verbergen.
Trotzdem war das ohne Zweifel Magie, die nicht mehr zur Allgemeinbildung
gehörte.

"Ich kann auch dein Herz stehen lassen.", sagte Heelem. "Wenn du als Beweis
gern einen ungefährlichen, aber etwas gruseligen Vorgeschmack haben möchtest,
sag Bescheid." Heelem lächelte unpassend freundlich ob dieser Ankündigung. "Ich
will nicht angeben, darum geht es mir nicht. Jedenfalls, wenn ich dich töten
wollte, würde ich es nicht mit Gift probieren."

"Ah, darauf wolltest du hinaus!" Liliðs Anspannung fiel wieder
ein wenig ab.

Heelem nickte. "Ich hoffe, ich habe dich nicht allzu sehr
erschreckt. Das wäre das Gegenteil meiner Absicht gewesen."

"Ich bin allgemein im Moment ein wenig angespannt.", widersprach
Lilið. Ihr Blick wanderte abermals zur Tasse Monua, die neben
ihr stand. Heelem hatte schon recht. Er könnte sie, ohne Spuren
zu hinterlassen, oder mit eben denen, die er jeweils
verursachen wollte, töten. "Warst du beim Herstellungsprozess
dabei oder hast das Getränk selbst gebraut?"

Heelem nickte. "Letzteres."

Lilið nahm die Tasse in die Hand, drehte dem Kartentisch erst einmal
den Rücken zu und setzte sich mit dem halben Po darauf, den
nackten Fuß auf den Hocker daneben abstützend. Sie roch nun einmal
aus nächster Nähe an der Tasse. Es roch sehr angenehm und sehr
ungefährlich. Sie nickte. "Überredet."

Heelem grinste. "Du bist also Nautika."

"Ich möchte Nautika werden.", korrigierte Lilið. Sie nippte
vorsichtig vom Getränk. Es zerging angenehm und weich auf
der Zunge, und schmeckte ebenso ungefährlich wie es roch. Wenn
ein Gift darin war, dann ein schmackhaftes.

"Beachtlich, dann mit so einer Strecke anzufangen.", sagte
Heelem. "Ich würde dir gern zuschauen."

Lilið verkrampfte schon bei der Vorstellung der Magen oder
auch andere Eingeweide drum herum. Aber gleichzeitig sollte
sie sich wahrscheinlich nicht entgehen lassen, dass ein Nautika
ihr gegebenenfalls wichtige Hinweise geben würde, wenn sie
etwas falsch machen sollte. "Warum bist du eigentlich Nautika?", fragte
sie ausweichend und gestikulierte bedeutungsschwer zur Tür.

Heelem zuckte mit den Schultern. "Ich bin bei gewissen Einbrüchen
in die Ungunst von einigen mächtigen Leuten gefallen. Als Nautika habe
ich einfach unabhängig davon viel Bewegungsfreiheit, ob und wie sehr mich wenige
bestimmte Lords nicht mögen. Oder Ladys. Oder, wie heißen die
neutralen der Sorte? Jedenfalls hätte ich die Freiheit in
irgendeiner Stellung für Magieausführung nicht, fast egal wie
gut die Stellung ist. Naja, und ich wäre dann vermutlich auch eigentlich
in der Verpflichtung, meine Freundeswesen zu töten."

Lilið trank noch einen Schluck und nickte. Als das Gespräch nicht
weiterfloss, setzte sie die Tasse ab und wandte sich wieder
der Karte zu. Heelem ließ ihr einen Moment Zeit, sich einzufinden, bevor
er neben sie an den Tisch trat.

Lilið hasste es so sehr, wenn ihr Leute dabei zusahen, wie sie
komplexe Aufgaben löste. Es setzte sie unter Druck. Sie rechnete
immer damit, dass ihr Tun kommentiert würde und meistens eher
in einer kritisierenden Weise. Oder dass sie sich an bestimmte
Regeln halten musste, wie man Dinge eben täte, um positive
Rückmeldung zu bekommen.

Sie machte sich klar, dass es hier nicht darum ginge, persönlich
bewertet zu werden, sondern darum, auf mögliche Fehler hingewiesen
zu werden, die ihnen Tage kosten könnten. Sie versuchte zumindest, sich
das klar zu machen. Dieser permanente Druck, den sie in der Schule
erlebt hatte, saß zu tief, um ihn einfach wegzuschieben. Aber sie konnte
versuchen, sich nicht danach zu verhalten. (Darum herum zu navigieren.)

Sie seufzte und machte sich an die Arbeit. Sie schäzte die
Geschwindigkeit mit ausreichend Spielraum ab, die die Ormorane
fahren würde. Sie würde später für Fälle von Flaute
Alternativrouten berechnen, aber zunächst galt es, überhaupt
eine Route zu finden. Anschließend maß sie mit dem Zirkel
eine Distanz ab, die die Ormorane mit der Geschwindigkeit innerhalb
einer Zeiteinheit zurücklegen würde, die davon abhängig, wie stabil die
Inselkonstellation in ihrem Umkreis jeweils wäre. Sie hatte
in einer Tabelle einen Wert zur Orientierung dafür
nachgeschlagen und ihn nach Regeln, die sie kannte, oder vielmehr
inzwischen im Gefühl hatte, etwas abgerundet. Die Distanz, die
sie mit dem Zirkel abmaß, war außerdem davon abhängig, wo auf der
Karte sie gebraucht wurde, weil sich die Skalierung über sie
hinweg leicht änderte.

Dann kam der kniffelige Teil: Zu raten, welche Richtung vielversprechend
wäre. Denn die Wanderinseln bewegten sich ja auf ihren Bahnen weiter,
während die Ormorane sich fortbewegte, und ihre Reiserouten müssten mit einberechnet
werden. Lilið kannte keine Person, der sich einfach eine passende Route
offenbarte, sobald sie auf eine Karte blickte. Auch die Nautikae, die
sie mal bei der Arbeit beobachtet hatte, gingen zunächst auf gut
Glück einen Versuch durch, der selten zum Ziel führte, um sich mit
der Entwicklung der Lage während der Reise vertraut zu machen.

Trotzdem war sie nervös, als sie mit dem Zirkel von der Startposition
aus eine Richtung wählte, das Kartensteinchen verschob und die
verschiebbaren Teile der Karte den Regeln entsprechend der gewählten
Zeiteinheit weiterschob, bis sich das neue Inselbild ergab.

Sie atmete tief ein und aus, bevor sie die Prozedur wiederholte
und wiederholte, bis sie herausfand, dass die Route nicht zum
Ziel führen konnte. Also, nicht unter einem Jahr, oder nicht
ohne am Ende eine Segelstrecke mit vier Tagen am Stück
zurückzulegen, weil nur noch Reiseinseln zum Pausieren
vom Kartensteinchen erreichbar wären, die sich zu schnell von
Nederoge entfernten und erst im nächsten Jahr wieder von der
anderen Seite annähern würden.

Lilið seufzte innerlich und versuchte, ihre Enttäuschung, die
sie fühlte, nicht zu zeigen. Sie hätte damit gerechnet, im ersten
Versuch vielleicht eine Woche zu spät anzukommen, nicht gar nicht.

Sie sortierte die Karte zurück in ihren Ausgangszustand. Heelem
bewegte sich in der Wartezeit kurz durch den Raum, aber lehnte
sich, als sie fertig war, ihr schräggegenüber neben das Fenster
an die Wand, von seinem Monua nippend. "Vergiss deine Tasse
nicht.", riet er.

Lilið hatte eigentlich gerade keinen Sinn für leckere Getränke, aber
nippte trotzdem. Es tat überraschend gut. Es ließ sie sich weniger
steril in einem Unterrichtsraum und mehr heimelig in einem
Wohnraum oder eben einer räumigen Bootshalle fühlen, die sie
mit angenehmen Erinnerungen verband. Sie hatte die Kühle
von Bootshallen und die Vor- und Nachfreude darin über das
Segeln immer geliebt.

Sie atmete noch einmal tief ein und aus, als sie sich für
eine andere Anfangsrichtung entschied. Dieses Mal konnte sie
es bereits basierend auf einer Vorstellung machen, die sie
beim ersten Versuch bekommen konnte. Nun ging es wieder
daran: Das Kartensteinchen bewegen, die Karte anpassen, messen und
Daten nachschlagen. Wenn Kartenteile mit Inseln darauf oben
aus der Karte herausfielen, führte sie sie von unten wieder an
der richtigen Stelle in die Karte ein.

Dieses Mal kam sie an eine interessante Reiseinselkonstellation
heran, bei denen sie detaillierter zusehen musste, wie sie sich
bewegen würden. Dazu musste sie die Zeiteinheit entsprechend
kleiner wählen. Doch als sie die kleine Drehscheibe nach der
Tabelle ein Stück rotieren wollte, die eine Kreisströhmung
in das Kartenmaterial übertrug, hielt sie skeptisch inne. Ihr
Gefühl sagte ihr, dass da etwas nicht stimmte. Sie runzelte
die Stirn und nahm sich das Büchlein vor -- und vergaß
dabei Heelems Anwesenheit. Sie tauchte in das Gedankenuniversum ein,
das sie am Navigieren so liebte, in diesen Fluss, in dem sie
nichts anderes mehr wahrnahm, als Geometrie und komplexe
Zusammenhänge von Bewegungen. Der Moment, in dem ihr Denken
Raum für diese nicht visuellen, bewegten Bilder und Abläufe
freigab, sodass sie alles ganz umschließen konnte und nicht
mehr in Worten dachte, sondern direkt in der Struktur, erfüllte
sie immer mit starker Euphorie.

Sie blickte zwischen Karte und Buch hin und her, bis ihr
einfiel, was sie vergessen hatte. Es bewegten sich ja auch
die großen, festeren Inseln, nur langsamer. Aber wenn die Kanten
ihrer Seenplatten die kleinerer Seenplatten berührten, hatte
das einen großen Einfluss. Und das war gerade für diese drei
kleinen Reiseninseln, deren Route sie verfolgte, relevant.

Sie rekonstruierte den letzten Schritt mit diesem neuen
Gedanken und führte die Navigation fort. Nur landete sie
wieder in einer ähnlichen Situation wie eben, in der sie keine
Möglichkeit hätten Nederoge zu erreichen. Was hatte sie sich
da vorgenommen?

Sie seufzte frustriert und sah auf in Heelems Gesicht. Es
machte einen neugierigen Eindruck, fand sie. Überhaupt
gefiel ihr Heelems Körperhaltungltung, sie hatte einen gewissen
Coolnessfaktor. Ein Arm unter den Ellenbogen des anderen
gelegt, der die Tasse hielt, ein Bein angewinkelt, den Fuß
an die Wand hinter sich gestützt und alles in allem sehr
entspannt und selbstbewusst.

"Willst du einen Kommentar haben?", fragte er.

Lilið biss die Zähne zusammen und nickte.

"Ich dachte schon, das wäre einer von Maruschs selbstzerstörerischen
Plänen, mit einer Person ohne Ausbildung diese Strecke zurücklegen
zu wollen, aber mit dir hat er eine gute Chance.", sagte er. "Du
lässt dich nicht davon, dass jemand zuguckt, oder von Ungeduld oder
enttäuschten Gefühlen dazu verleiten, falsche Entscheidungen zu
fällen oder Flüchtigkeitsfehler zu machen und etwas
zu Übersehen. Und was du dir da vorgenommen
hast, ist nun mal wirklich nicht leicht. Dafür braucht es deine
Geduld und Präzision."

In Liliðs Hals bildete sich ein Kloß. Sie war gleichzeitig sehr
erleichtert, dass er sie nicht heruntermachte, und überfordert
mit diesem unbeschreiblichen Kompliment. Wortlos, aber vielleicht
mit einer Träne im Auge brachte sie die Karte wieder zurück
in ihren Ausgangszustand.

"Du magst mit deiner fehlenden Routine wahrscheinlich einen halben
Tag brauchen, bis du die ersten Routen hast, die funktionieren.", fuhr
Heelem fort. "Selbst ich bräuchte sicher eine Stunde. Und du bist
maximal eine halbe dabei. Das packst du noch."

"Danke.", murmelte Lilið. Ihre Anspannung ließ nach, und erst dadurch
merkte sie, wie groß sie eigentlich gewesen war. Schule war echt kein irgendwie
heilsamer oder hilfreicher Ort gewesen.

"Ich komme später noch einmal wieder und lass dich mal eine Weile
dein Ding machen.", sagte Heelem. "Du bist ja ganz schön fertig mit
den Nerven."

Lilið kicherte nervös und nickte. "Sie waren schon halbwegs
verbraucht, bevor ich überhaupt angefangen habe."

"Ich erinnere mich.", sagte Heelem. "Du magst keine klassische Ausbildung
genossen haben, das merkt man deinen Methoden an. Aber du machst dich
gut mit deiner eigenen Technik. Solltest du tatsächlich in Nederoge
ankommen, kannst du dich Leicht-Nautika nennen." Er winkte zum Abschied.

Wie denn?, dachte Lilið. Woher ein Zertifikat kriegen, wenn nicht
während oder nach einer offiziellen Ausbildung. Aber sie lächelte trotzdem ein
wenig in sich hinein, als sie den nächsten Versuch wagte.

---

Vielleicht drei Stunden später hatte Lilið den ersten Durchbruch. Sie
kehrte ihn direkt um, um ihn noch einmal durchzugehen und sich dabei
in das Kursbuch, das zur Ormorane gehörte, Notizen zu machen. Und
während sie dabei war, kam Heelem wieder mit einem zweiten Monua
und einem Stück Zapfelkuchen. Er sah ihr wieder dabei zu,
was sie immer noch nervös machte, aber nicht mehr so sehr wie
vorhin. Er unterbrach sie nicht, bis sie den Stift beiseite legte
und zufrieden darüber war, dass sie sich alles gut gemerkt hatte. Und
dass sie allmählich ein Gefühl für die Inselbewegungen der nächsten
zwei Wochen bekommen hatte.

"Ist die Route gut?", fragte Heelem.

"Sie fühlt zumindest zum Ziel, und das sogar in genau zwei
Wochen.", erwiderte Lilið.

"Aber ist sie gut?", wiederholte Heelem die Frage.

Lilið fragte sich, ob er auf etwas bestimmtes anspielte und ging
die Route ein drittes Mal durch. Das funktionierte doch alles so? Doch
sie brauchte nicht lange, um das Problem zu bemerken: "Wenn wir
auf dieser Insel nicht noch vor Sonnenaufgang aufbrechen, dann driften
wir zu viel ab. Auch wenn an dem Tag kein guter Wind ist."

Heelem nickte. "Es ist eine guter Start, aber es ist deshalb keine
Route, die ich empfehlen würde.", sagte er. "Vielleicht mag eine
andere einen Tag länger dauern, aber dafür darf dann auch
was schief gehen."

Lilið nickte. "Danke.", sagte sie erneut.

"Du wärest auch selbst drauf gekommen.", erwiderte Heelem. Er
schob ihr ein steifes, wasserfestes Papier zu. "Hier!"

Lilið nahm es irritiert entgegen. Es sah sehr offiziell aus, fand
sie. "Ist das ein Zertifikat? Bin ich damit Leicht-Nautika?", fragte
sie ungläubig.

"Nein." Heelem wirkte amüsiert, in einer wohlwollenden Weise. "Das
kannst du in Nederoge, wenn du es in unter siebzehn Tagen in
einer Nautikae-Ausbildungsverwaltung abgibst, gegen ein solches
Zertifikat eintauschen."

"Hui!", brachte Lilið hervor. Das war eine Chance, mit der sie
nicht gerechnet hätte. Stolz und Glücksgefühle durchströmten sie. Und
Ehrgeiz.

---

Es war bereits Abend, als Marusch mit ausreichend Proviant wieder
eintraf. Lilið hatte eine gute Route und allerlei
Alternativrouten herausgeschrieben. Sie fühlte sich müde und
zufrieden. Und sicher. In den letzten Durchgängen hatte sich in ihrer
Vorstellungswelt die Karte allmählich in Bilder von Inseln
und Strömungen übersetzt, die weniger abstrakt
waren. Sie wusste, was auf sie zukommen
würde. Heelem hatte ihr nicht geholfen, nicht mit weiteren
Tipps zumindest, aber damit, ihre Fähigkeiten einzuschätzen
und ihr sachlich mitzuteilen, welche Schwächen sie hatte. Nicht,
um sie abzuwerten, sondern um ihr ein Gefühl dafür zu geben,
auf welche ihrer Sinne sie sich mehr verlassen konnte und
auf welche weniger.

Marusch verfrachtete die Vorräte in den Gepäckberereich der
Ormorane. Dann kam sie zum Kartentisch und begutachtete
Liliðs Vorgehen. "Ich mag Nautikae wirklich gern bei
der Arbeit zusehen. Ich mag diese Hingabe, Präzision
und Sorgfalt beobachten.", sagte sie. "Aber ich habe keinen
Plan, was du da gerade tust."

Lilið blickte verwundert auf, als sie etwas realisierte, was
Heelem vorhin schon angesprochen hatte. "Du legst dein
Leben damit in meine Hände?", fragte sie.

"Unseres.", korrigierte Marusch mit einem zärtlichen Grinsen
in der Stimme. "Wenn wir wegen schlechter Navigation draufgehen,
dann tun wir es ja zusammen. Magst du eine Umarmung?"

Wie konnte Lilið innerhalb dieses halben Tages vergessen haben,
wie sich Maruschs Art anfühlte? Sie liebte ihre liebevolle
Sprechweise und teils fehlende Ernsthaftigkeit, die dann doch
da war, wenn es wichtig war.

"Ja bitte!", flüsterte sie.

Noch ehe sie sich zu Marusch umdrehen konnte, wanderten Maruschs
Arme von hinten um Lilið herum. Maruschs Körper schmiegte
sich weich von hinten an ihren und Marusch erlaubte
sich einen zarten Kuss in Liliðs Halsbeuge.

Lilið versuchte, in Heelems Gegenwart nicht zu heftig
einzuatmen oder anders stark zu reagieren. Sie strich
Marusch über die Unterarme und Finger, die sich viel zu
schnell von ihr wieder lösten.

"Bekomme ich auch eine?", fragte Heelem.

Damit hatte Lilið nicht gerechnet. Sie fühlte genau
in sich hinein, was mit ihr passierte, als Marusch auf
diese charmante Art, die sie hatte, zustimmte, Heelem sich
von der Wand abstieß, und sie eine innige Umarmung
austauschten. Sie küssten sich gegenseitig die Nasenspitzen, bevor
Marusch auch diese Umarmung auflöste.

Lilið fühlte keine Eifersucht. Das gefiel ihr. Sie wusste
eigentlich nicht einmal, was Nasenküsschen bedeuteten, oder
ob überhaupt von etwas ein Beziehungsstatus abgeleitet
werden könnte, aber es sah innig und liebevoll aus, und
es störte sie nicht. Im Gegenteil: Ein
Teil in ihr freute sich sehr, weil es sich für sie sofort
logisch und natürlich anfühlte, Beziehungen zu führen, die auf diese Art
offen oder frei waren. Sie freute sich, dass sie nicht viel
in sich selbst fand, woran sie hätte arbeiten müssen, damit sie
nichts Schlechtes dabei fühlte, weil es bereits war, wie sie
eben einfach war. Und sie freute sich interessanterweise, dass
sie auf diese Art nicht ins Gesellschaftsbild passte, das
sich immer so beengt anfühlte.

---

Diese Nacht verbrachten sie hier in der Halle in Schlafsäcken auf
dem Boden, die Marusch in der Ormorane gelagert hatte. Es hätte
Lilið vielleicht weniger gemütlich vorkommen sollen als das Bett in der
Nacht zuvor, aber das Gegenteil war der Fall. Sie waren hier
nicht so bedroht. Es fühlte sich viel mehr so an, als gehörte sie
hier her.

Aber als Lilið sich über Marusch beugte, um zärtlich ihr Gesicht
zu küssen, unterbrach Marusch sie, obwohl sie schneller atmete. "Ich
habe zu lange zu wenig geschlafen.", sagte sie. "So gern ich würde,
heute Nacht nicht."

Lilið ließ sofort von ihr ab. "Ich hätte fragen sollen."

"Ich fühle mich bei dir sicher genug, etwas zu sagen.", erwiderte
Marusch.

---

Lilið wachte vor Sonnenaufgang auf. Sie fühlte sich sehr ausgeruht,
aber sie hatten verabredet, bei Sonnenaufgang aufzubrechen, und sie hatte ein
Bedürfnis, sich vorher zu waschen. 

Sie trat aus der Halle in das neblige, feuchte Morgengrauen. Neben dem Eingang,
halb durch eine Holzwand abgeschirmt, gab es eine kalte Dusche aus gesammeltem
Regenwasser. Die ruhige Stimmung der Szenerie tat sehr gut und entspannte sie.
Das Geräusch des Wassers und die angenehme Kälte auf der Haut belebte ihre
Sinne. In der Ferne standen zwei Personen zusammen und unterhielten sich leise,
die eine davon erkannte sie an der Haltung und Kleidung als Heelem.

Übergangslos verschwand das angenehme Gefühl von Sicherheit und
Entspannung, das Lilið gerade noch gefühlt hatte, als wäre
es nicht erlaubt. Lilið überkam Panik, dass Heelem doch erkannt haben könnte,
wer sie war, und sie verraten hatte. Die beiden Personen blickten
sich in regelmäßigen Abständen zur Halle um.

Es war vermutlich eine unsinnige Panik, aber Lilið unterbrach das Auswaschen
ihrer Binde, legte ihre Anziehsachen über ihren feuchten Arm und trat wieder
in die Halle. Marusch schlief noch tief. Sollte sie sie wirklich
für einen Verdacht aufwecken, der sehr unrealistisch sein
könnte? Würde Marusch hier so ruhig schlafen, wenn sie Heelem nicht
voll vertrauen würde?

Lilið erinnerte sich daran, dass Heelem erzählt hatte, dass Marusch
manchmal selbstzerstörerische Pläne ausführte. Das brachte sie auf
einen neuen Gedanken: Vertraute Marusch ihr überhaupt? Oder kam
es Marusch nicht drauf an, was passieren könnte? Was war das eigentlich
genau für ein Mensch?

Lilið beschloss, für den Augenblick zu vertrauen, ihren Waschvorgang
abzuschließen und Marusch wie geplant, zum Sonnenaufgang zu wecken. Wenn
die Ühnerdrachen ihr nicht zuvorkommen würden.

---

Nichts passierte. Heelem hatte sich einfach mit seiner Schwester
unterhalten, von der Marusch das Kleid für den Ball geliehen hatte. Sie
hatten gewartet, bis Marusch und Lilið soweit wären, um ihnen zu helfen, das Segelboot
zum Wasser zu tragen. Das war zu viert viel einfacher als es das zu zweit
gewesen wäre, weil der Weg zum Wasser über eine breite Treppe hinabführte und auch
nicht ganz kurz war.

Heelem und Marusch umarmten sich auch zum Abschied und gaben sich wieder
Nasenküsschen, als sie bereits knietief in der Brandung standen. Lilið
hielt das Boot, während Marusch das Ruder einrastete, einstieg
und das größere der Segel dichter holte, sodass etwas Wind darin stand und
das Boot weniger kippelte, als Lilið an Bord kletterte. Sie steuerte
und bediente das Großsegel, während Marusch das Vorsegel und den
Gewichtstrimm übernahm. Unter der Landabdeckung hatten sie noch recht
milden Wind, aber sobald sie den Windschatten verließen, strich der
ablandige Wind gleichmäßig und stark in die Segel, sodass sie Angelsoge rasch
hinter sich ließen. Lilið fühlte sich überwältigt von alledem. Davon,
das erste Mal so eine lange Route zu segeln, die sie auch noch
selbst geplant hatte, mit einer Person an ihrer Seite, in die sie
sich verliebt hatte, um einen gestohlenen Gegenstand an seinen
Ort zurückzuschmuggeln. Das war das Abenteuer, das sie sich immer
gewünscht hatte.
