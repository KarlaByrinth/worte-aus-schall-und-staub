Frühstück bei einer Medika
==========================

*CN: Tiefes Wasser, Vergiftung, Fiebertraum.*

Aus irgendwelchen Gründen dachte Lilið an die Tiefe
unter sich. Ihr Kopf malte das Bild schematisch sehr
eindrucksvoll: Ihr kleiner Kopf und etwas von den
Schultern überwasser, ihr restlicher kleiner Körper
unterwasser und darunter endlose Tiefe, von der sie den
Grund nicht einmal erahnen konnte, voller Fische
verschiedenster Größe.

Ein Schauer oder ein Schaudern durchlief
ihren Körper, so genau konnte sie das nicht
unterscheiden. Das kalte Wasser drang ihren Nacken entlang
unter ihre Jacke und durchnässte, was sie darunter trug. Das
Wasser floss dort langsam wie Öl, aber kalt wie kleine
Eiskristalle. Die Jacke war immerhin eine, die auch zum Segeln gedacht war. Sie
lag eng am Körper an, die Taschen waren dicht, und
vor allem würde sich die sich bilndende Wasserschicht zwischen ihrem Körper und
der Jacke aufwärmen und sie wärmer halten, als jedes weniger
dafür gemachte Kleidungsstück das vermocht hatte.

Es war kalt und hatte sich im ersten Moment eisig angefühlt. Wasser
war immer zu kalt, wenn ein von der Sonne und von Aufregung
durchgeheizter Körper sich plötzlich in den Ozean begab. Das
Wasser mochte aber seine 12°C haben oder sogar etwas mehr. Das
war überlebbar. Selbst, wenn sie einen Kälteschock erlitten
haben sollte. Vor allem mit so einer Jacke, wie Lilið sie trug. Zumindest
für nicht allzu lange Strecken. Und mit einem unvergifteten Körper. Leider
hatte sie eine lange Strecke vor sich und einen zumindest ansatzweise
vergifteten Körper. Trotzdem rechnete sie sich Überlebenschancen aus,
die nicht vollkommen absurd unrealistisch wirkten.

Sie blickte sich um. Die Schaumkrönchen, die die Reiseschneise der
Reiseinseln markierten, hatten sie mit der Fragette schon passiert. Das war ein
Glück. Dort, oberhalb der sich aneinander vorbeibewegenden Seenplatten,
hätte sie mit starken Strömungen zu kämpfen gehabt. In der anderen Richtung
sah sie Land. Die ersten, kleinen Vorinseln, deren genaue Position
oft schwer berechenbar war und die deshalb kaum bewohnt waren, wirkten
gar nicht so weit weg. Natürlich täuschte das immer. Lilið hatte
lange gebraucht, um zu lernen, Strecken, die sie schwimmen wollte,
nicht vollkommen zu unterschätzen. Sie war schon als Kind oft
zu den alten Fischerpfählen rausgeschwommen. Auch das hatte sie
immer leicht gegruselt. An ihnen wuchsen Seepocken und Muscheln. Sie
hatte sie immer berühren wollen, aber auch Angst gehabt, die Strecke
zurück dann mit einer Verletzung schwimmen zu müssen, weil die
Seepocken schwarfe Kanten hatten. Auch da hatte es ihr immer etwas
bei der Vorstellung gegraut, dass unter ihr so viel Raum für
große Fische war, so viel schwarze Tiefe, aus der sie vielleicht mit
ihren strampelnden Beinen etwas hätte anlocken können. Aber auch
damals war schon immer die viel realistischere Gefahr gewesen, die
Strecke zu unterschätzen und ihre Kräfte nicht gut einzuteilen.

Lilið machte die ersten, ruhigen Schwimmzüge und atmete dabei
in einem zu den Arm- und Beinbewegungen passenden Rhythmus. Die Panik
vor der Tiefe war ihr sehr vertraut. Sie machte sich klar, dass
die größeren, gefährlicheren Fischkreaturen selten die Grenze der Seenplatten
passierten, weil sie sich vor der Zivilisation der Menschen
fürchteten und weil ihnen unter Land zu warm wurde. Hier war
eher Lebensraum für kleinere Wasserwesen, die ihr nichts anhaben
wollten. Und ob die größeren das wollten, selbst wenn sie doch
auf dieser Seite der Schaumkrönchen auftauchten, wäre auch nicht unbedingt
gegeben. Sie wären physisch dazu in der Lage, aber Menschen waren selten Beuteziel.

Lilið wusste, dass es keinen Sinn hatte, zu hetzen. Und gleichzeitig
wusste sie, dass sie in ein paar Stunden das Fieber durch das
Gift einholen würde. So klein die Dosis gewesen war, es würde
nicht ohne Fieber an ihr vorbeigehen. Sie wusste, dass es sinnvoll
gewesen wäre, den Körper nicht zu sehr zu belasten, bevor es eintreten
würde, aber sie hatte keine Wahl. Sie würde so um die zwei Stunden
stur schwimmen müssen, schätzte sie. Ein Schwimmzug nach dem anderen. Sie atmete
ein, und ins Wasser aus. Sie sang dabei eine monotone Melodie, jedes
Mal, wenn sie ausatmete, einen langen Ton. Das hatte sie immer getan,
um dem Schwimmen einen weiteren Sinn zu geben, und es beruhigte sie auch
jetzt. Mehrfach kämpfte sie den Impuls nieder, schneller zu schwimmen, damit
sie es rascher hinter sich hätte. Aber das hätte sie verausgabt, bevor sie
auch nur in die Nähe des Ufers gekommen wäre. Sie versuchte, nicht
über die Entfernung nachzudenken, als das Schwimmen viel mehr wie
eine meditative Beschäftigung aufzufassen. Sie versuchte, -- und das
sogar nicht ganz vergeblich --, das Schwimmen zu genießen. Den
Geschmack des Salzes im Mund, das beruhigende Wasser, die
Wellen, an die sie ihren Atemrhythmus anpasste, damit sie beim
Ausatmen durch sie hindurch tauchte.

Als sie so dicht an den Inseln war, dass es schon Sinn ergab, nicht einfach
nur grob in ihre Richtung zu schwimmen, sondern gezielt eine
von ihnen anzupeilen, pausierte Lilið ein wenig und beobachtete sie, sowie die
Strömung, die sie an den Richtungen der Wellen ablas und am Sog an
ihrem Körper spürte, die Wolken und den Wind. Sie schätzte ein, dass sie eher
die Insel links von der ihr nächsten anpeilen sollte, denn wenn sie die Strecke
überwunden hätte, wäre sie wohl schon so weit weitergereist, dass
sie dann die ihr nächste wäre. Lilið hatte das Fach Nautik immer Spaß gemacht
und sie hatte sich auch über den Unterricht hinaus in ihrer Freizeit
mit Nautikaufgaben beschäftigt. Sie war gut darin gewesen. Nun
wären ihre Fähigkeiten in dem Fach also wohl das erste Mal entscheidend.

Vielleicht eine weitere Stunde später, -- Lilið hatte längst jegliches
Zeitgefühl verloren --, erreichte sie die Schwimmkante der Insel. Die
Kante, an der der Grund steil abfiel, weil der daran angrenzende
Grund nicht mehr zur reisenden Insel gehörte. Sie hatte
recht gehabt: Diese Insel hatte sich so bewegt, wie sie es sich ausgemalt
hatte. In ihrem ermatteten Gehirn war sogar ein wenig Platz für Stolz
übrig.

Die Schwimmkante hatte sie durch den rapiden Temperaturanstieg des Wassers
bemerkt. Nun fühlte sie sich auf einmal zu warm. Aber das konnte auch das Fieber
sein. Wenige Schwimmzüge später konnte sie endlich stehen. Ungeduldig versuchte
sie zu gehen, aber dazu war es noch zu tief. Sie erreichte den sandingen
Grund nur mit den Zehenspitzen. Also tat sie doch noch ein paar
Züge, und als sie endlich mit den Schultern aus dem Wasser ragen
konnte, schleppte sie sich an Land. Ihr Körper, so lange getragen
von Salzwasser, fühlte sich schwer an. Es war ein wunderschöner Sandstrand, aber sie hatte
gerade keinen Sinn dafür. Sie war hier ja schließlich nicht zum Badeurlaub.

Sie taumelte den Strand hinauf, so weit weg von der Brandung, wie der Strand
es zuließ, damit das Wasser sie nicht wegspülen könnte, wenn sie schliefe,
und sank, erschöpfter als Menschen irgendwie erlaubt sein sollte, in
den Sand. Sie spürte, wie das Fieber ihren gestressten Körper erfasste
und durchschüttelte. Ob sie einschlafen könnte?

---

Eine Stimme drang in Liliðs Bewusstsein. Eine Kinderstimme hatte ihr
eine Frage gestellt. Durch die dichte Substanz aus Traum, die sie
umwaberte, drang das "Hallo?" nur schwer hindurch. Musste sie es
beantworten? Brauchte sie dafür nicht noch irgendetwas? Etwas
Gelbes. Und sie musste eine Aufgabe erfüllen, um das Gelbe zu bekommen.

Sie öffnete ein Auge und sah einige Schritte von sich entfernt eine
kleine Gestalt stehen. Ein sicherer Abstand. Das konnte sie verstehen. Sie
hatte ja diese Fangzähne. Liliðs Gehirn lieferte eifrig die Begründung
nach, warum sie nun diese Fangzähne hatte: Sie war zu lange im Meer
geschwommen, und zwar nicht nur nah vor Ufer, sondern zu weit draußen. Sofort
war sie gedanklich wieder dort und schwamm, ohne ein Ende in Sicht, im
tiefen, weiten Meer, gegen Strömungen an, gegen die sie nur eine
Chance hätte, wenn sie diese Aufgabe erfüllte.

Lilið realisierte, dass sie halb in einem Fiebertraum gefangen war, der
sich mit der Realität vermischte. Aber sie konnte nicht trennen, was was
war. Sie wusste nicht, woran sie sich hätte entlanghangeln müssen, um
zu trennen, welche Gedanken sie gerade ernst nehmen sollte und welche
zur Traumschicht gehörten. Das brachte sie zu der Aufgabe zurück, die
sie lösen musste. Dabei ging es um etwas Gelbes. Es war wichtig, herauszufinden,
was das Gelbe war. Es hätte ihr klar sein müssen. Wenn sie wach
gewesen wäre, hätte sie es vielleicht gewusst. Dann wäre die Aufgabe, die
sie gerade kaum greifen konnte, vielleicht einfach gewesen.

Sie wusste nicht, wieviel Zeit vergangen war, als sie eine zweite
Stimme hörte. Dieses Mal eine ältere. Sie stellte die selbe
Frage. Und sagte dann: "Hol Hermen und Berne mit der Trage, ja? Das
Geschöpf ist zu schwer, als dass ich es selbst tragen könnte."

Die ältere Stimme war näher, als die Kinderstimme vorhin. Vielleicht
war es die Kinderstimme von vorhin, oder eine andere, die einen unsicheren,
aber klar bestätigenden Laut von sich gab. Lilið schloss aus den wenigen
Geräuschen, die das machte, dass das Kind den Auftrag ausführend weggelaufen
war. Aber sie war sich auch nicht sicher, in ihren Ohren rauschten so
verschiedene Geräusche, wie Wind, Oder Strömungen?

Lilið fühlte eine kühle Hand auf der Stirn. Die Traumschicht wirkte weniger
dick als vorhin, aber schwerer, wie eine durchnässte Filzdecke. Daher
schaffte sie es nicht, die Augen zu öffnen, obwohl das sicher höflich
gewesen wäre. Sie war gerade fertig, sich Gedanken dazu zu machen,
dass sie einfach nicht konnte, als die Person nach dem Verschluss
ihrer Jacke griff, um diese zu öffnen. Lilið riss die Augen ohne
Mühe auf, erhob den Oberkörper ein Stück und sackte wieder zusammen.
"Nicht ausziehen!" Die Stimme ein energisches Flüstern, das
sich anfühlte wie Seife im Hals.

"Du solltest aus den nassen Sachen.", empfahl die neben ihr
kniende Person ruhig "Ich
bin Medika. Ich tu dir nichts, ich möchte dir helfen. Weißt du, ob
du verletzt bist?"

"Vergiftet.", korrigierte Lilið sachlich. Sie hob die schweren Arme,
der Empfehlung folgend, um selbst die Jacke zu öffnen. Sie nahmen
einen ungewöhnlich flachen Weg zu den Verschlüssen, weil sie so
schwer und labberig waren.

"Du weißt nicht zufällig den Wirkstoff?" Die Stimme klang
freundlich.

Lilið hätte ihr fast reflexartig vertraut, aber sperrte sich noch
dagegen. Trotzdem informierte sie ohne Umschweife: "Gelbwurz."

Die freundliche Stimme lachte leise. "Das ist kein Gift.", sagte
sie. "Mit dem Gewürz müsstest du dir schon den Magen vollgeschlagen
haben, damit es dir schadet."

Lilið war fertig mit den Verschlüssen und versuchte, sich die
Jacke abzustreifen. Sie kam nicht weit, aber dort, wo die Luft
am Brustkorb die nasse Kleidung darunter berührte, fror sie
sofort bitterlich. Was war das für ein Medika? Kannte sich mit
Giften nicht aus und empfahl ihr, die Jacke auszuziehen, die
sie so sehr geschützt hatte. Aber ein kleiner Teil in Liliðs
Gehirn verstand, dass das durchaus einen Sinn haben mochte. Sie
wusste nur nicht genau, ob dieser Teil zu ihrem Traum gehörte,
der ihr unsinnige Aufgaben stellte, die sie nicht zur Gänze
verstand. Jedenfalls wehrte sie sich nicht, als die Person
eine Hand auf ihr Brustbein legte, und einen Finger an ihre
Halsschlagader. Das war typisch für Medikae, so etwas zu tun.

"Puls und Körpertemperatur sprechen für mittelstarkes Fieber
und schwere Erschöpfung. Du hast
recht, dass du keine blutenden Verletzungen hast, weder äußerlich
noch innerlich. Das ist gut." Viele Medikae konnten so etwas mit
etwas Magie über den Blutfluss spüren, wenn sie guten Zugang zu Information
bezüglich Blutversorgung hatten. "Es gibt einige Gifte, die auf
Wurz enden. Bist du dir sicher mit Gelbwurz?"

Lilið schüttelte schwach den Kopf. Dabei wurde ihr schwindelig.

Das Medika hatte recht. Lilið hatte Namen durcheinandergebracht, aber
der richtige fiel ihr nicht ein. Sie hatte ihn doch immer parat. Sie
hatte gern in der Schule so getan, als hätte sie immer ein Fässchen davon
bei sich, weil sie den makaberen Gedanken daran gemocht hatte. Sie
versuchte, über die Jugenderinnerung den Namen zu angeln, aber merkte,
dass sie in diese abdriften würde.

"Ich muss dagegen Brot essen und trinken.", gab sie also Anweisungen,
statt den Namen des Gifts weiterzusuchen. "Und schlafen. Und vielleicht
hilft Stiftkohlenstaub."

"Das habe ich da.", versicherte das Medika.

Richtig, das wäre bei Medikae auch arg unwahrscheinlich gewesen, dass
sie so etwas nicht da hätten.

---

Lilið fühlte sich unbehaglich, wie sie so wehrlos von den zwei
starken Menschen, die hinzugekommen waren,
auf eine Liege gelegt und über die Dünen durch die Gegend transportiert
wurde. Sie versuchte, die Augen offen zu halten, um sich den Weg zu
merken. Aber wozu eigentlich? Sie kam vom Strand, nicht von einem Ort, der
ihr Schutz geboten hatte. Sie musste grinsen, als ihr klar wurde, wie
wörtlich sie gestrandet war.

Durch einen weiteren Fieberschub hatte sie verpasst, wie sie schließlich
in einen Raum gelangt war. Der Mensch, den das Medika Berne genannt hatte, hielt
sie, damit sie nicht umkippte, während das Medika sich
nun doch durchsetzte, und sie auszog. Berne
wirkte nicht wie eine Person, die die Lage ausgenutzt hätte, um sie an
irgendwelchen Stellen sexualisiert anzufassen. Beide fassten sie einfach
zweckgemäß an.

"Ich werde deine Jacke über den Stuhl neben deiner Liege hängen, auf
sie aufpassen und nicht weiter anrühren.", versprach das Medika. Es hatte
wohl gemerkt, schloss Lilið, dass sie sich dagegen wehrte, dass sie ihr
abgenommen wurde.

Lilið hatte nicht die Absicht, zu vertrauen, aber sie hatte auch keine
Wahl, als zu akzeptieren, dass die Jacke wohl nun eine Weile nicht so sicher
bei ihr war, wie sie das gern gehabt hätte. Sie fühlte sich tatsächlich wohler, als
die Meeresnässe von ihrem Körper abgerieben worden war, nun nur noch
die Fiebernässe blieb und auch die weite Kleidung, die ihr übergestreift
worden war, trockener war, als es Liliðs Fantasie gerade zugelassen
hätte. War das überhaupt gut? War sie nicht jetzt ein Meereswesen?

Lilið dachte über ihre Fangzähne nach, und dass sie sich an das Haigebiss
gewöhnen könnte, als sie einschlief und in einen ruhigeren Traum
überglitt.

---

Als sie wieder aufwachte, fühlte sie sich nicht mehr fiebrig, nur
noch sehr erschöpft, als wäre sie eine Woche sehr krank gewesen. Vor
ihr Kopf wieder wach und ihre Gedanken klar. Ihr
Blick wanderte zum Stuhl neben dem Bett, wo wie versprochen unverändert
die Jacke hing. Dann durch den Raum. Niemand war hier.

Vorsichtig und leise stand Lilið auf. Ihr Körper war schwer, aber
ließ sich ganz gut kontrollieren. Ihre Knie fühlten sich noch weich an. Sie versuchte,
ruhig zu atmen. Der Atem fühlte sich etwas flatterig an, aber
sie bekam genug Luft. Vorsichtig wagte sie sich an ein paar
Dehnübungen und Kniebeugen. Anschließend ging sie im Zimmer auf und
ab. Das machte die Sache eher besser. Ihr Körper gewöhnte sich daran,
sich wieder zu bewegen.

Dann war es wohl besser, abzuhauen. So nett es auch war, dass diese
Leute ihr geholfen hatten, sie sollte so wenige Spuren wie möglich
hinterlassen. Sie wusste auch nicht, wie es bei fremden Medikae
abliefe. Ob sie ihnen mitteilen müsste, wem sie schutzbefohlen wäre, damit
so etwas wie Schuld beglichen würde? Oder ob sie dafür eine Marke
bräuchte? Sie war noch nie bei Medikae gewesen, die nicht zu Lord
Lurchs Hofstaat gehört hätten, und eben auch immer in der Rolle seines
Kindes.

Sie scheute sich davor, die trockene Kleidung, die sie am Körper
trug, zu stehlen. Also beschloss sie, die noch klamme Kleidung
anzuziehen, die im Zimmer auf einem Ständer trockente. Wurde in
diesem Haushalt immer Kleidung im Krankenzimmer getrocknet, oder
machten sie es ihretwegen, damit sie sie im Blick behalten konnte?

Bevor sie sich umzog, überzeugte sie sich davon, ob das
Buch und der Brief noch an Ort und Stelle waren. Sie war erleichtert,
als sie feststellte, dass die Tasche auch über diese lange Zeit
hinweg unter Wasser dicht gehalten hatte. Zumal sie sie kleiner
genäht hatte und darin nicht übermäßig geschickt war. Aber der Faden
war von einer Beschaffenheit, dass er sich ausdehnte, wenn er
mit Wasser in Berührung kam, und sofort abdichtete.

Dieser olle Brief. Er war so schön geschrieben, erzählte so
wundervolle Dinge. Er fühlte sich so sehr nach Marusch an. Und
gleichzeitig war in ihm eine Nachricht verborgen gewesen, die sie
gern vorher entdeckt hätte. Auf so eine einfache Kodierung hätte
sie auch mal früher kommen können. Es ärgerte sie beinahe im
Nachhinein, hätte Ärger nicht zu viel Energie benötigt. Die Anfangsbuchstaben
der Sätze bildeten "Vergiftungsgefahr durch Allil". Deshalb war der
erste Teil des Briefs auch so ein langer Textblock ohne Absatz
gewesen. Weil das Wort "Vergiftungsgefahr" so lang war.

Es war einfach, aber geschickt eingewoben, dachte sie erneut. Sie
würde in Zukunft jeden Brief, den sie bekäme, auf geheime Botschaften
untersuchen!

Sie hatte sich gerade fertig umgezogen und machte Antsalten, zum
Fenster hinauszusteigen, als das Medika sich in den Türrahmen
lehnte und sie schmunzelnd anblickte. "Ich werde dich nicht
aufhalten, aber du kannst auch noch ein Frühstück bekommen, bevor
ich das nicht tun werde."

Lilið verharrte. Ihr erster Gedanke war, dass sie beim Essen leicht
vergiftet werden könnte. Ihr zweiter, dass sie nicht allzu bald einfach
an Essen gelangen würde. Sie war auf einer der kleinen Inseln, die
erfahrungsgemäß wenig bewohnt waren, und sie müsste stehlen. Wenn
sie erwischt würde, würde es nur umso schwerer werden, von hier
wegzukommen, oder gar unmöglich. Ihr
dritter Gedanke war, dass es schon sehr unwahrscheinlich wäre, von
einer Person erst gesundgepflegt und dann wieder vergiftet zu
werden.

"Lärchenwurz war es.", erinnerte sie sich.

"Hui, aber nicht viel!", erwiderte das Medika. "Ich bin Barb. Ich
bin die Medika auf dieser Insel, auf der du gestrandet bist, die den
Namen Schleseroge trägt. Vielleicht ist das für dich auch hilfreich
zu wissen."

"Danke für alles überhaupt.", fiel Lilið ein zu sagen.

Sie atmete noch einmal bewusst den Salzwind ein, der durch
das Fenster hereinwehte, und ließ den tiefen Atemzug wieder
entweichen. Dann trat sie zurück in den Raum.

"Wenn du möchtest, wechsel deine Sachen ruhig noch einmal und
häng die, die du aus den Augen lassen magst, draußen auf die Leine
in den Wind. Dann sind sie vielleicht trocken, wenn du
gehst.", empfahl Barb.

Lilið zögerte nur noch einen Moment, dann nickte sie. "Danke!"

---

Lilið hatte nur mäßige Schwierigkeiten, sich zurecht zu finden. Zum
Aufhängen ihrer klammen Anziehsachen brauchte sie zum Beispiel
Wäscheklammern. Ihr suchender Blick war dem Kind wohl aufgefallen, das
sie beobachtete. Es hielt einen Abstand von mehreren Metern. Lilið
musste an ihr Haifischgebiss denken, dass sie natürlich nicht hatte, sondenr
dass ein Auswuchs ihres Fiebertraums gewesen war. Aber einer, der
ihr gar nicht so schlecht gefallen hatte. Die Aufgabe mit dem
Gelb, an das sie sich nicht genau erinnern konnte, war fieser
gewesen.

Vielleicht war es das Kind von gestern. Jedenfalls
war es freundlich und reichte ihr schließlich wortlos Klammern, als
es verstanden hatte, wo das Problem lag. Dann vergrößerte es den
Abstand wieder.

Das Kind saß auch mit am Frühstückstisch und redete die ganze
Zeit kein Wort. Niemand drängte es. Lilið fragte sich, ob sie früher
auch so still gewesen war. Das war gut möglich.

Es gab gutes Brot, Obst, etwas Gemüse und Aufstrich. Lilið fiel erst
etwas verspätet auf, dass es kein Fleisch gab. Ob das daran lag, dass
sie von niedrigem Stand waren? Sie hatte keine Vorstellung vom
Gefüge hier, aber wie sollte sie so etwas erfragen? Und dann auch
noch respektvoll?

"Wäre vorgesehen, dass ich irgendetwas als Gegenleistung erbringe?", fragte
sie vorsichtig.

Barb lächelte ein breites einladendes Lächeln. "Nein, hier nicht.", sagte
sie. "Aber dass du fragst, spricht dafür, dass du dich nicht auskennst. Dann
solltest du wissen, dass es hier etwas Besonderes ist und es anderswo
auf der Welt anders abläuft. Wir haben auf dieser
Insel eine Kommune gegründet, die etwas außerhalb der Regeln der
Monarchie-Bündnisse funktioniert."

Lilið unterbrach das Essen und legte ihr beschmiertes Brot ab. Das klang
sehr interessant. Und schön! "Kann ich einfach hier bleiben?" Sie lachte
vorsichtig etwas verlegen. "Was für eine freche Frage.", kommentierte
sie selbst. "Ich nehme an, selbst wenn ihr einfach Leute in eure Kommune
aufnehmt, müssen sie ihren Anteil leisten oder so etwas. Ich weiß überhaupt
nicht, ob Sinn ergibt, was ich frage."

"Wenn du bliebest, wäre es gut, wenn du deinen Anteil leistest, wenn du kannst, aber es
gibt keine Erwartungshaltung oder Verpflichtung.", antwortete Barb. "Die
Sache funktioniert, weil alle, die hier leben, dahinterstehen. Du kannst
dir vielleicht eine sehr große Familie vorstellen, in der wir die
Arbeit, die eben da ist, so aufteilen, dass es möglichst allen gut geht. Und
wenn was nicht geht, geht es halt nicht."

Lilið merkte, wie allein die Vorstellung sie erleichterte. Und wie sie
Barb merkwürdigerweise doch vertraute. Das wäre auch eine zu abgefahrene
Geschichte, um sie mit irgendwelchen betrügerischen Absichten
vorzutragen. "Es klingt wunderschön!"

"Es hat nicht nur Vorteile und ist nicht nur rosig. Aber ich will es
keinen Tag missen.", erwiderte Barb. "Schleseroge ist eine kleine
Reiseinsel mit etwas chaotischen Bahnen. Wir sind häufig weit ab
vom Schuss. Das macht Kontakthalten zu Leuten außerhalb recht schwer. Wir
sind hier also ziemlich isoliert. Und wenn wir doch mal nicht so autark
sein können, wie wir eigentlich wollen, ist es herausfordernd, weil
wir keine rettenden Notanker zu einer ausgewählten und eingeweihten
festen Insel in der Umgebung aufrecht erhalten können."

Lilið verstand die Nachteile sofort. Wenn sie irgendwann ihre Eltern besuchen
wollte, dann wäre sie gerade am nächsten dran, aber im Winter müsste sie
vermutlich um den halben Globus reisen. "Habt ihr hier Nautikae?", fragte
Lilið.

"Was glaubst du? Eine Reiseinsel ohne Nautikae?" Barb wirkte
amüsiert. Und gleichzeitig klang sie warm. "Klar! Wir haben
zwei. Und doch könnten wir mehr Expertise auf dem Gebiet
gebrauchen. Bist du Nautika?"

Lilið schüttelte den Kopf. "Noch nicht. Das ist nur mein Traumberuf."

Eine Weile widmeten sie sich still dem Essen. Liliðs Frage vom Anfang
war halbwegs beantwortet. Sie gehörten hier sozusagen zu gar keiner
Gesellschaftsschicht. Warum kein Fleisch auf dem Tisch war, war damit nicht
beantwortet. Vielleicht brauchte es weniger Platz, sich von Gewächsen
zu ernähren, und sie hatten für ihre Kommune nur diese eine kleine
Insel.

Lilið spielte tatsächlich mit dem Gedanken, zu bleiben. Sie hatte gut
verteiltes Wissen und Können. Sie hatte eine solide Ausbildung
genossen. Sie war nicht sehr gut in Magie, aber das war hier vielleicht
auch nicht so nötig. Sie mochte Barb und das Kind. Auf der anderen Seite
war ihre Menschenkenntnis nicht die beste. Und ein paar Wochen
hierzubleiben, um herauszufinden, ob ihr Eindruck sich über die Zeit
hielt oder änderte, würde bedeuten, dass sie irgendwo anders, sehr
weit weg, in der Welt landen würde.

Sie konnte nicht bleiben. Da die Vertauschung, wenn auch mit
unschönen Hindernissen, erfolgreich verlaufen war, war ihr Plan
nun, den Blutigen Master M zu finden. Ihr Vater schwebte in
Gefahr. Und ihr bester Anknüpfpunkt war Marusch.

Bei der Überlegung ging ihr auf, dass, wenn sie den verschwundenen
Wertgegestand fände und ihrem Vater zurückbrächte, zumindest wenn
sie es selbst täte, tatsächlich Allil am Internat für skorsche
Damen mit Pech aufgeflogen wäre. Daran hatte sie nicht gedacht. Ein
wenig verstand sie Allils Motivation, sie vorsichtshalber
umzubringen, schon. Aber wäre für den Fall
der Fälle weitere Absprachen zu treffen nicht auch eine Option
gewesen? Dass Lilið Allil informieren würde, wenn sie ihren Vater
besuchen würde, sodass Allil zu dazu passenden Zeiten das Internat
verlassen würde?

"Wenn ich gehe, finde ich Schleseroge vermutlich nie wieder, oder?", fragte
Lilið.

"Tatsächlich ist die Insel nur in den sehr präzisen Rotationsseekarten
verzeichnet. Wenn du wirklich Nautika wirst, hast du eine Chance.", widersprach
Barb. Und fügte hinzu: "Du möchtest also doch nicht bleiben." Lilið konnte
nicht lesen, ob Enttäuschung mitschwang oder nicht.

"Puh!", seufzte Lilið. "Ich würde schon gern. Aber da draußen schweben
Leute in Gefahr, denen ich helfen möchte. Und ich kenne hier bisher nur
euch."

"Hast du ein bestimmtes Ziel? Vielleicht können unsere zwei Nautikae
dir helfen. Hermen hast du gestern im Fiebertraum schon beinahe
kennen gelernt."

---

Einen halben Tag später saß sie wieder in eigener Kleidung und mit
einem Sack Proviant, den ihr Barb freundlicherweise vermacht hatte, tatsächlich
mit Hermen zusammen in einer kleinen Jolle mit Kurs auf Angelsoge. Schleseroge
hatte derzeit ein recht hohes Tempo gen Süden drauf, sodass sie die große
Insel Angelsoge schon heute passierte. Es war die Insel, auf der der Ball
stattfinden würde. Aber das musste nicht heißen, dass sie ihn rechtzeitig
erreichen würde. Die Insel war noch um einiges größer, als die ganze
nederoger Inselvereinigung, zu der die namengebende Hauptinsel ihres Vaters
gehörte.

Der Wind war böig, doch Hermen verstand sein Segelhandwerk
gut. Sie brauchten nur wenig Absprache, um ein
eingespieltes Team zu sein. Und wie die Jolle schräg gegen den Wind in die
Wellenkämme schnitt und ihr Gesicht als Resultat davon mit Wasser überspült wurde,
gab ihr Lebensfreude zurück, die sie schon lange nicht mehr so sehr
gefühlt hatte. Sie war nicht mehr Teil des Monarchie-Systems. Sie war
frei und würde eine Weile im Untergrund leben. Und sie lebte noch. Sie
wusste noch nicht wie, aber wenn sie ihre Mission
erfüllt hätte, den Blutigen Master M oder den
verschwundenen Anteil des Schatzes der Monarchie zu finden, würde
sie Nautika werden und dann vielleicht nach Schleseroge zurückkehren. Oder
anderweitig über die Meere schippern.
