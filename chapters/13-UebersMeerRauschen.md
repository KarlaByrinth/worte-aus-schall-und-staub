Übers Meer rauschen
===================

*CN: Gefühlstaubheit, Depression, Mord, miese Politik, küssen, Körperdysphorie.*

Marusch war eine solide Seglerin. Das war für Lilið keine Überraschung. Immerhin
war sie ja auch von Nederoge hierher gesegelt, was selbst, wenn sie vor Reiseantritt
noch nie gesegelt wäre, genug Übung für diesen Eindruck gewesen
wäre. Obwohl die Hinreise sicher, da eher mit der Strömung als gegen sie und mit
einem guten Nautika, höchstens eine Woche gedauert hatte. Wie Marusch
während einer kurzen Unterhaltung während der Fahrt erzählt hatte,
war aber auch das nicht ihre erste Segelerfahrung mit einer Jolle gewesen.

Obwohl es Lilið nicht hätte überraschen sollen, war sie
auf etwas anderes eingestellt gewesen. Vielleicht,
weil sie in den vergangenen Jahren nur dann mit neuen Personen zum
Segeln gruppiert worden war, wenn diese neuen Personen keine Erfahrung
mitbrachten und von ihr lernen sollten. Oder weil Marusch nicht navigieren
konnte, sich dahingehend auf sie verließ, und Lilið einfach naheliegend
empfunden hätte, wenn Marusch sich auch beim Segeln auf sie verlassen
müsste. Aber Marusch brauchte so wenige Anweisungen und machte
ihre Sache so gut, dass Lilið ihr die Steuerposition überlassen
hätte, wenn Marusch denn gewollt hätte.

Als Steuerperson saß Lilið hinten dem Heck näher auf der der Segelseite
abgewandten Bortskante, die bei der Ormorane breit und beinahe
gemütlich war, und hielt den Pinnenausleger in der Hand. Das
war ein Stab, der mit einem Gelenk an der Pinne befestigt
war, womit sie jene also aus einigem Abstand bewegen konnte. Die
Pinne wiederum war ein Bestandteil der Ruderanlage. Schob sie diese
Richtung Segel, so drehte sich das Ruderblatt am Heck des Bootes
vom Segel weg und lenkte den Bug des Schiffes mehr in den Wind.

Die meiste Zeit über bewegte Lilið das Ruderblatt über den Pinnenausleger
nur um Nuancen. Sie spürte den Wasserdruck auf selbigem im Pinnenausleger,
der locker in ihrer Hand lag, sah und fühlte, wann der Wind sich veränderte,
zum Beispiel leicht drehte oder etwas nachließ, und die Ausrichtung
des Segelbootes beeinflusste. Dann korrigierte sie den Kurs entsprechend
für ein paar Augenblicke ein klein wenig. Und dann waren da noch
die rhythischen leichten Schlenker, die sie permanent fuhr, um die
Wellen als Anschub zu nutzen. Auf den Wellen zu reiten, wurde das
Vorgehen genannt. Das ging bei der schweren Ormorane nicht so gut
wie bei den leichteren Jollen, die sie meistens segelte, aber
bei eher starkem Wind von fast hinten waren Wellen dabei, bei
denen sie es schaffte, so zu steuern, dass sie sich sozusagen
unter dem Heck verfingen und sie beim Versuch, unter ihnen
hindurchzurauschen, antrieben.

Immer wenn sie so eine Welle ritt, verlagerte Marusch, ohne Aufforderung
ihr Gewicht weiter gen Bug, denn wenn der Bug beschwerter war, konnte
die Welle unter ihnen noch schlechter weg. Sie tat es für Lilið
ersichtlich nicht nur, weil sie Liliðs Gewichtsverlagerung nachahmte, sondern hatte
auch selbst ein Gefühl dafür. Sie lächelten sich an dabei, immer wieder.
Wellenreiten mit einer Ormorane war eine Kunst, gerade weil es so selten
möglich war. Es machte auch deshalb Spaß und nicht nur, weil sie
dabei so viel Fahrt aufnahmen.

All diese Bewegungsabläufe machte ihr Körper fast automatisch. Sie dachte
nicht mehr darüber nach, sondern fühlte in das rauschende
Wasser um das Boot herum hinein, in die Vibration des Holzes, aus
dem die Ormorane gemacht war, und ihr Körper reagierte einfach
als Antwort auf Verändrung. Die kurzen Bindfäden, die als Hilfe auf dem Segeltuch
mit Aufklebern befestigt waren und die Windverwirbelungen darauf
anzeigen, brauchte sie eigentlich nicht. Wenn die äußeren verwirbeln
würden, die sie durch das Segeltuch hindurchschimmern sah, müsste
sie fieren, also die Schot lockerer lassen, und
wenn die inneren verwirbelten, dichterholen. Aber sie kamen fast
nicht dazu, überhaupt zu verwirbeln. Bei Maruschs Vorsegel passierte
es gelegentlich, sie blickte mehr darauf.

Lilið konzentrierte sich vor allem auf die Richtung. Darauf, dass
die Inseln, die sie am Horizont sah, im passenden Winkel vom
Boot aus gesichtet werden konnten. Sie segelten zwischen zweien
hindurch, anschließend einen langen Bogen, bis sie eine dritte dahinter
anpeilten. Lilið liebte den Moment, als die Küste der einen
Insel, die sie passierten, nah genug war, um Details auszumachen.

Sie war fast traurig, dass sie schon gegen frühen Nachmittag auf
ihrer Zielinsel ankamen. Sie hätte noch zwei oder drei Stunden
durchgehalten. Aber nun machte es keinen Sinn, weiterzusegeln.

Sie schoss vor Ufer in den Wind auf, sodass die
Segel flatterten und der Gegenwind die Fahrt aus dem
Boot nahm. Marusch glitt über die
Bordskante, um die Ormorane zu halten. Lilið hatte gut geschätzt,
das Wasser reichte Marusch höchstens bis zur Hüfte. Lilið zog
das Schwert, das mittig durch einen Schlitzkasten durch den
Rumpf gesteckt war, damit das Boot beim Segeln nicht
abdriftete und lauschte auf das laute Flattern der
Segel. Es fühlte sich für sie oft schon ein wenig wie
Magie an, wie diese agressiv Geräusche verursachenden, mächtigen
Flächen an Tuch, die kaum wirkten, als wären sie zu
bändigen, sofort Ruhe gaben und ungefährlich wirkten, sobald
Wind in ihnen stand.

Sie rutschte über die Bordkante und klemmte auch
die Ruderanlage aus, bevor sie gemeinsam das Segelboot den
leeren Strand hinaufzogen, die Segel einholten, sorgfältig
aufrollten und befestigten.

Dies war eine unbewohnte, kleine Insel. Eine mit etwas Zivilisation
zum Auffüllen von Vorräten wäre erst übermorgen wieder dran. Lilið
hatte, außerhalb von Fieberträumen, noch nie auf einer kleinen
Reiseinsel übernachtet. Die Vorstellung war aufregend, einzuschlafen,
und wenn sie wieder aufwachte, ganz andere Landmassen am Horizont
zu sehen. Und vermutlich auch die ein oder andere Reisekagutte, weil
sie sich einer der Nebenfahrwasser der größeren Reiseschiffe nähern
würden.

Lilið stellte das kleine Zelt auf und bereitete auf Maruschs inzwischen
wieder gewaschenen Decke ein Abendessen vor, während Marusch sich auf der
Insel nach einer Quelle umsah. Nicht alle Inseln hatten so eine, daher
war es wichtig, wenn sie eine fanden, ihr Trinkwasser wieder aufzufüllen,
falls es auf der nächsten Insel dann nicht die Möglichkeit gäbe.

Marusch brachte tatsächlich frisches Wasser in Flaschen und einen Kanister
abgefüllt mit, als sie wieder auftauchte. "Das sieht sehr edel aus,
wie du das hergerichtet hast.", sagte sie. Sie stellte den unförmigen Kanister
in die Mitte, und rückte etwas Obst, das Lilið dort hingelegt
hatte, darum herum an, als wäre der Kanister etwas besonderes.

Lilið grinste. "Eigentlich wäre besser, das Wasser in den Schatten zu
stellen, oder?", fragte sie schnippisch.

Marusch warf einen Blick in den wolkenverhangenen Himmel, der nicht
wirkte, als würde er vor der Abenddämmerung noch aufklaren.

"Du hast recht.", stimmte Lilið zu. "Vielleicht denke ich zu leicht bei
veralbert ausgeführten Aktionen, dass sie vielleicht nicht ganz
durchdacht sind, und verderbe dann ausversehen den Spaß."

"Was nicht völlig verkehrt ist! Ersteres.", betonte Marusch. Sie ließ sich Lilið
gegenüber nieder, -- was Lilið etwas enttäuschte, weil sie gern
Körperkontakt gehabt hätte --, und belegte sich eine der Scheiben
Brot, die Lilið abgeschnitten hatte. "Ich bin häufig albern, und ja, manchmal
sehe ich dabei nicht genau genug auf Notwendigkeiten oder Gefahren. Da
ist es gut, wenn du da ein Auge drauf wirfst."

"Wie, als du dir überlegt hast, mir einen wunderschönen, verspielt
klingenden Brief zu schreiben, in den eine Botschaft zu unauffällig
hineinkodiert war?", fragte Lilið grinsend.

"Wenn es auffälliger gewesen wäre, hätte Allil die Kodierung
wahrscheinlich entdeckt.", erwiderte Marusch sachlich.

Lilið fragte sich, ob Marusch das Thema nicht allmählich
leid wäre. Sie hatten darüber nun schon mehrfach gesprochen und vielleicht war
es nicht nett, darauf herumzureiten. Aber vielleicht war es auch in
so eine Zusammenhang Unfug, Rücksicht auf Maruschs Gefühle zu nehmen.

"Wie fühlst du dazu überhaupt?", fragte Lilið. Sie bemerkte, dass Marusch,
das belegte Brot in der Hand, mit dem Essen auf sie wartete. Also nahm
sie sich kurzerhand eine Banune und biss hinein. Die meisten Menschen
schälten die dünne, blassrosa Schale ab, aber Lilið fand sie
eigentlich ziemlich lecker.

"Wozu genau?", fragte Marusch neugierig. "Möchtest du wissen, wie ich mich
beim Schreiben des Briefs gefühlt habe? Oder wie ich darüber
fühle, dass ich beinahe einen Mord an dir mitverschuldet hätte?" Endlich
biss auch sie vom Brot ab. Sie wirkte ziemlich unbekümmert, fand Lilið.

"Letzteres.", sagte sie, sich nicht einmal die Mühe gebend, vorher
aufzukauen.

Marusch hingegen ließ sich Zeit mit der Antwort, bis sie das Brot
verspeist, sich die Finger abgeleckt und dann in einer bereitgestellten
Wasserschale gewaschen hatte. "Meistens ist es mir egal.", sagte sie.

Lilið verbarg nicht, dass sie die Information ziemlich schockte. Sie
legte die angebissene Banune beiseite. Sie wusste nicht, was sie dazu
sagen sollte.

"Manchmal überrollt mich bei der Vorstellung ein Grauen, was gewesen
wäre, wenn Allil es geschafft hätte.", fügte Marusch hinzu. "Aber
es ist Vergangenheit. Emotionen treiben oft Handlungen an, oder sind
dazu da, etwas zu verarbeiten, glaube ich. Letzteres ist aussichtslos. Wenn
ich etwas verarbeiten wollte, würde ich außerdem vielleicht bei den Morden
anfangen, die ich mit mehr Erfolg begangen habe. Und mein Handeln ist
bereits ausreichend beeinflusst. Ich weiß, worauf ich in Zukunft achten
muss und werde."

"Morde, die du mit mehr Erfolg begangen hast?", fragte Lilið. Sie
war interessanterweise darüber dann nicht entsetzt, obwohl sie Marusch glaubte. Zur
Unterstreichung, der nicht vorhandenen Gefühle, hob sie die Banune wieder auf
und aß weiter.

"Ich sagte dir schon, dass ich kein moralisch einwandfreier Menschn bin.",
erinnerte Marusch sie. "Und dass ich dir nichts über meine vergangene
Identität erzählen werde. Kannst du damit leben?"

"Ich muss ohnehin! Ich habe da in den nächsten Wochen nicht
so die Wahl.", erwiderte Lilið. "Aber du suggerierst gerade, dass diese
Sache, dass du Morde begangen hast, zu deiner vergangenen Identität gehört."

Marusch nickte. "Ich verstehe, wenn dir das zu schlimm ist.", sagte sie. "Ich
weiß nur nicht, wie wir damit dann gut in den nächsten zwei Wochen umgehen
werden, die du schon sagtest."

"Du interpretierst in eine falsche Richtung.", widersprach Lilið. "Solange
ich nichts unmittelbar mit den Morden zu tun habe und nun keine solche
Gefahr mehr von dir ausgeht", sie unterbrach sich, "also, nehmen wir
an, du hältst dein Versprechen auf mehr Vorsicht und von dir ginge
tatsächlich keine todbringende Gefahr mehr aus. In dem Fall zumindest
ist mir deine Vergangenheit egal." Lilið kicherte, weil sie sich
so sehr in Gedanken verhedderte. "Auch nicht egal, weil ich ein übermäßig
neugieriger Mensch bin. Aber egal für die Entscheidung, ob ich mit dir
etwas zu tun haben will."

"Ich halte mein Versprechen.", versicherte Marusch. Sie atmete tief
durch und setzte sich gerader hin. Was unbeschreiblich schön aussah,
fand Lilið. Das entspannte, minimal besorgt wirkende Gesicht, der Blick
auf ihr zweites Brot gerichtet, das schwarze Haar, das sachte im Wind wehte, bei dieser
geraden Körperhaltung. "Was wäre die richtige Interpretationsrichtung gewesen?"

"Du möchtest über deine vergangene Identität nichts erzählen, aber
du hast dann doch etwas preisgegeben.", antwortete Lilið. "Das verleitet
mich dazu, abgrenzende Fragen stellen zu wollen, was du preisgeben möchtest
und was nicht. Was vielleicht abstrakt genug für dich ist, um doch
darüber zu reden. Aber ich möchte dich auch nicht bedrängen."

Marusch lächelte ihr Brot an. "Du bist lieb!", sagte sie. "Du
darfst alles fragen, solange es immer selbstverständlich in Ordnung
sein wird, wenn ich abblocke. Ich glaube, ich würde vielleicht auch noch
ein paar Worte zu den begangenen Morden loswerden, um die Sache
ein bisschen einzuordnen, aber ich weiß noch nicht, wie genau. Vielleicht
helfen da sogar abgrenzende Fragen, wie du sie genannt hast."

"War es bei den Morden, die du begangen hast, ähnlich, wie bei dem
Fastmord an mir?", fragte Lilið. "Du hast Allil mal Mörderin genannt
und nicht nur potenzielle Mörderin." Lilið erinnerte sich genau
daran. Es war ihr aufgefallen. "Hat sie mal eine Person ermordet, der
du eine Zusammenarbeit mit ihr nahegelegt hast? Oder hat sich etwas
in der Richtung mit einer weiteren Mordesperson ereignet
und du nennst dich deshalb so?" Lilið wagte nicht den Oder-Teil
der Frage in Worte zu fassen. Dass Marusch einen Mord eigenhändig
ausgeführt hätte.

"Ich weiß, dass Allil mal einen sexuell übergriffigen Mentor
ermordet hat.", erwiderte Marusch.

Wieder hatte sie dabei diese Strenge im Ausdruck, die Lilið inzwischen glaubte, einordnen
zu können. Die hatte Marusch schon beim letzten Mal gehabt, als sie
über Allil gesprochen hatten. "Du hast dazu eine feste Einstellung, dass
Allils verbrecherisches Verhalten üblicherweise zu undifferenziert verurteilt
wird, und bereitest dich darauf vor, gegen mich gegenzureden, wenn
ich den Punkt diskutieren wollte?", fragte sie.

Marusch blickte sie eine Weile ernst an und nickte schließlich. "Und
es ist nicht einfach, die Standardargumente auseinanderzunehmen.", fügte
sie hinzu. "Obwohl für mich offensichtlich ist, was alles auf welche Weise falsch
läuft, weil ich eine Übersicht der politischen Gesamtlage zu haben glaube, muss
ich dann auf einzelne, aus dem Gefüge herausgerissene Argumente eingehen,
die Schmerz verursachen, weil sie Lagen von Menschen und Not verkennen."
Marusch seufzte. "Obwohl ich eigentlich Übung darin haben müsste,
fühle ich mich immer noch nicht gewappnet, angemessen zu verteidigen. Während
ich gleichzeitig auch nicht so viel von Morden halte, wie es vielleicht
den Anschein erweckt."

Lilið nickte. "Ich war gerade zumindest gar nicht darauf aus, Allil
anzugreifen.", räumte sie ein. "Das muss in meinem Kopf arbeiten. Dem
Komplex gebe ich noch etwas Zeit, bevor ich dich damit wieder behellige. Es
geht gerade um eine Einordnung deiner Einstellung. Hast du aus der Not
heraus gemordet? Oder aus so einer anderen Lage heraus, die du gemeint
hast?"

Zu Liliðs Überraschung schüttelte Marusch den Kopf. "Ich habe gemordet,
weil ich beigebracht bekommen habe, dass es das Richtige ist.", sagte
sie. "Vielleicht etwa so wie Lord Lurch."

Lilið schluckte. An dem Bissen Banune vorbei, den sie nicht ganz
hinuntergekriegt hätte, aber nun schnell aufkaute. Das war eine
Perspektive, mit der sie nicht gerechnet hätte, aber sie ergab einen
grauenvollen Sinn und zerriss sie innerlich. Wieso konnte sie eine
Person wie ihren Vater lieben? Ja, sie hatte ihn vielleicht kritisiert
dafür, aber sie hatte eben auch verstanden, was die Wache gesagt
hatte. Ihr Vater ermordete keine Leute, die Lebensmittel stahlen, sondern
es ging um so etwas wie Bücher, die zwar interessant wirkten, aber
einfach nicht lebensnotwendig wären. Sie verstand nicht, warum
Leute für so einen Diebstahl ermordet wurden, aber sie verstand
genauso wenig, warum Menschen für so einen Raub ihr Leben riskieren
würden. Ergab ihr Strom von Gedanken überhaupt einen Sinn? Lilið
fühlte, dass sie eben das Gesamtbild nicht sah.

Warum fiel es ihr so schwer, sich vorzustellen, Allil zu
verstehen oder zumindest zu akzeptieren, wie sie war, aber
sie fühlte nicht jedes Mal ein Grauen, wenn sie an die Morde
ihres Vaters dachte? Warum hätte sie bis gerade am liebsten
so etwas wie eine Verteidigung von Marusch gehabt, um sich
mit ihr wohlzufühlen, aber fühlte sich so sehr distanziert
von ihrem Alltag, in dem sie großgeworden war, nur weil das,
was darin ablief, festen Regeln folgte und Maruschs oder Allils
Handeln eben nicht?

Sie versuchte, das Gewusel in ihrem Kopf zu beruhigen und auf das
zu reduzieren, was gerade wichtig für ihr Verständnis war. "Du bist
zwar durchaus nicht die erste einbrechende Person gewesen, die
ich nicht verraten habe,", sagte sie schließlich, "aber wenn welche
erwischt worden sind, habe ich keinen Versuch gewagt, meinen Vater
davon zu überzeugen, sie gehen zu lassen. Und ich habe auch nicht
versucht, Menschen aus den Kerkern zu befreien, womit ich vielleicht
sogar mehr Erfolg gehabt hätte, als damit, meinen Vater von
etwas zu überzeugen. Habe ich mich dadurch auch an Morden
schuldig gemacht?"

Marusch lächelte sanft, aber entspannt wirkte sie nicht. "Vielleicht. Das
kann ich nicht für dich entscheiden.", sagte sie. "Ich hatte etwas
unmittelbarer damit zu tun. Kannst du damit leben?"

Lilið nickte. "Es ist ja auch nicht so, als würde es dir nicht
leidtun.", sagte sie nachdenklich. "So scheint es zumindest."

Aber wo und wie anfangen, diese Ungerechtigkeit der Welt, die
sich Lilið hier darlegte, dieses Messen nach zweierlei Maß
zu bekämpfen? Sie könnte ihren Vater wahrscheinlich von
nichts überzeugen. Weil er wiederum von anderen abhinge. Und
Leute aus dem Kerker zu befreien, wäre ein Rudern gegen
Seenplattenströmungen. Sie verabscheute den Gedanken, so über
individuelle Menschenleben zu denken. Vielleicht wäre jede
Befreiung einer Person aus einem Kerker ein Gewinn. Aber dazu
müsste sie vielleicht auch wissen, wofür die Leute saßen.

"Ich finde es nicht richtig, und ich handele vielleicht, als würde es mir
leidtun. Aber eigentlich fühle ich nichts.", widersprach Marusch. "Ich
bin innerlich sozusagen selber tot. Ich fühle meistens überhaupt nichts. Ich
handele nach meinen Überzeugungen, nicht nach meinem Mitgefühl, und die
Überzeugungen werden durch diese Welt auch regelmäßig durchgerüttelt. Hat
es überhaupt einen Sinn?" Marusch grinste plötzlich breit, wie um
etwas zu überspielen. "Nun fange ich mit ganz schön belastendem Zeug
an. Lass dir den Appetit nicht verderben."

Es bräuchte eine Revolution, dachte Lilið. Aber wie zettelte
man so etwas an?

Nachdenklich aßen sie, nun eine Weile schweigend, bevor Lilið
fragte: "Aber du fühlst Romantik?" Sie versuchte, es sachlich
zu tun, weil es so beabsichtigt war, und es funktioniert auch halbwegs.

"Ja sehr!" Marusch lächelte wieder sanft und blickte sie dabei
an. Die dunklen Augen glänzten unter Wimpern, deren Schönheit
Lilið noch nicht lange genug angesehen hatte, fand sie.

"Ist deine Hoffnung, dass dich deine romantischen Gefühle aus deiner
Depression herausholen werden?", fragte Lilið. Sie fragte sich, ob
die Diagnose von ihrer Seite in Ordnung war und fügte die Bedenken
auch gleich hinzu: "Es klingt für mich nach einer, aber ich bin
keine Fachperson."

"Es kann sein, dass es eine ist, ich weiß es nicht. Ich hatte mein Leben lang schon
diese langen Phasen von Gefühllosigkeit. Und wenn die Phasen sich
mal verzogen, brannte in mir Wut, und die ist noch schlimmer. Oder
ich fühle eine tiefe Trauer in mir, die ich sehr mag, aber niemand
versteht das. Und ich kann sie oft nicht erreichen.", sagte
sie. "Romantik ist mehr ein Gefühl, das außen
herum sitzt und durchaus sehr schön ist. Gerade ist das Gefühl extrem stark, ich
genieße es sehr. Aber ich hoffe nicht darauf, dass sie mich heilt
oder so etwas. Ich habe gemordet, Lilið, und ich fühle dazu nichts. Solche
Dinge, wie dass du durch mich mitverschuldet hättest sterben
können, sind mir meistens, wie gesagt, egal. Ich weiß nicht einmal,
ob das nur schlecht ist. Oder ob das eben einfach ein Teil von mir
ist. Ein Teil, der immer da ist, und keineswegs dadurch verschwindet,
dass ich arg verliebt in dich bin."

"Und in Heelem?", fragte Lilið. Sie hatte den Eindruck, dass die
Frage verräterisch Eifersucht wiederspiegeln könnte, die sie gar nicht
empfand. Oder verdrängte sie sie einfach? "Ich glaube, ich bin
einfach nur neugierig. Du musst nichts dazu sagen." Es fühlte
sich richtig an, was sie sagte. Sie fühlte sich eigentlich sehr
entspannt dabei, vielleicht ein wenig freudig oder alberig verspielt.

"Weniger.", antwortete Marusch. "Heelem und ich sind schon länger
befreundet. Und ja, wir finden uns auch gegenseitig anziehend. Wir
haben irgendwann darüber gesprochen und dann ausprobiert, was wir
mögen. Aber es war nie so ein Gefühlschaos wie das jetzt mit dir."

"Ist Heelem zufällig dir Person, die vom Küssen auf den Mund
enttäuscht war?", fragte Lilið, sich an die kleinen Nasenküsschen
erinnernd.

Marusch nickte mit einem Grinsen im Gesicht. "Heelem hat es auch
noch mit anderen ausprobiert und mag es einfach nicht." Sie
fügte etwas ausdrücklicher hinzu: "Ich glaube, das darf ich erzählen,
aber weitere private Fragen über Heelems Vorlieben werde ich
nicht beantworten."

Lilið nickte. "Ich habe viel zu persönliche Fragen gestellt.", sagte
sie und schämte sich tatsächlich ein wenig.

Sie schwiegen eine Weile, in der sie zu Ende aufaßen. Lilið hatte
nur ausgepackt, was heute gegessen werden durfte, damit sie ihre
Vorräte nicht irgendwann verfrüht zur Neige gehen würden.

Marusch hatte eine Menge von sich offenbart, was sie eigentlich
wirklich nichts anginge, fand sie. Auf der anderen Seite half es
ihr tatsächlich, ein gewisses Vertrauen aufzubauen. Mehr, als
wenn sie nicht einmal gewusst hätte, dass irgendwas so großes,
schweres hinter Maruschs Fassade lauerte.

Hatte es überhaupt eine Fassade gegeben? Es hatte sich nicht
angefühlt, als wäre irgendeine Mauer durchbrochen worden. "Fällt
es dir schwer, über deine Gefühlsleere zu reden? Vertraust
du mir auf besondere Weise? Oder legst du das einfach jeder
Person offen dar, die fragt."

"Ein klein wenig Grundvertrauen brauche ich, aber sonst
letzteres.", antwortete Marusch.

"Warum?", fragte Lilið. Sie legte sich auf die Decke, nachdem alles
aufgeräumt war, sodass Marusch neben ihr Platz hätte.

Marusch setzte sich neben sie in einen Schneidersitz. "Ich
weiß es nicht.", sagte sie. "Umgekehrt habe ich aber auch nicht
das Gefühl dass das privat sein müsste. Ich wüsste nicht, warum ich
es nicht erzählen sollte. Vielleicht möchte ich mich einfach nicht
verstecken. Vielleicht möchte ich auch zu meinen Taten stehen. Vielleicht
erhoffe ich mir, irgendwann gesehen zu werden und dafür gemocht
zu werden, wer ich bin."

In Liliðs Inneren zog sich bei diesem letzten Satz alles
traurig zusammen und sie wollte sagen, dass sie Marusch dafür
mochte. Aber sie reflektierte, ob das wirklich stimmte. Sie
mochte Marusch, aber war es dafür? "Ich mag dich zumindest mit
diesem etwas. Mit dem Universum in dir, das zu dir gehört."

Marusch legte sich nun doch neben sie und berührte vorsichtig
ihre Wange mit ihren Fingerspitzen. "Das bedeutet mir viel.", flüsterte sie. "Das
dringt tatsächlich ein bisschen in mich hinein und zupft an
der Traurigkeit. Danke." Sie griff Liliðs Hand, die sich
gerade, von Maruschs Geste inspiriert, auf
dem Weg zu Maruschs Gesicht befand, und küsste
sachte ihren Handrücken, bevor sie sie wieder ablegte. "Und
wenn du morgen anders fühlen solltest, ist das auch in Ordnung."

Lilið verstand das Ablegen ihrer Hand als Hinweis, dass sie
Marusch gerade nicht anfassen sollte. Sie atmete tief ein
und aus, um das Lodern niederzuringen, dass Maruschs Lippen
gerade entfacht hatten. "Ich habe noch eine vielleicht schlimme
Frage, eine Verständnisfrage, glaube ich.", sagte sie.

"Ich nehme all deine Fragen.", sagte Marusch.

"Würdest du gern zur Rechenschaft gezogen werden?" Lilið
grauste es davor, aber sie konnte sich vorstellen, dass es
zu Marusch passen könnte. "Für die Morde, meine ich?"

Marusch lächelte, vielleicht eine Spur veralbert. "Eigentlich
nicht.", sagte sie. "Es sei denn, damit ist etwas gemeint, was
irgendwem etwas bringt. Was nicht einfach Vergeltung oder
Strafe ist, sondern dazu beiträgt, dass es Leuten besser
gehen kann. Ich halte nicht viel von Rache oder davon, dass
Leute sich besser fühlen, wenn bloß der bösen Person genügend
weh getan worden ist." Sie machte eine Atempause, vielleicht,
um den nächsten Satz noch einmal mehr zu betonen: "Aber ich
würde es akzeptieren. Was auch immer das zur Rechenschaft
ziehen dann beinhalten würde.."

"Ich möchte das nicht.", flüsterte Lilið. Interessanterweise
war dies für sie bis jetzt der emotionale Tiefpunkt dieses
Gesprächs. "Aber ich möchte dir auch nicht im Wege stehen. Und
ich möchte dich verstehen, ohne dir einfach reinzureden, weil
ich was anderes für besser halte.", fügte sie hinzu. "Ich
hoffe einfach, wenn es je dazu kommen sollte, dass du zur
Rechenschaft gezogen wirst, dass dabei erkannt wird, dass
du nun ein anderer Mensch bist."

Marusch grinste auf einmal sehr breit. "Die Wahrscheinlichkeit,
dass ich für diese Vergangenheit zur Rechenschaft gezogen werde,
ist ohnehin sehr klein!", sagte sie. "Es ist viel wahrscheinlicher,
dass du getötet wirst, weil du ein olles Buch gestohlen hast. Zehn
Jahre hat das Buch niemand mit dem Arsch angesehen, und jetzt,
da es weg ist, wollen diese hohen Herrschenden allen an die Gurgel, die
etwas mit dem Schwund zu tun haben. Es ist alles so absurd!"

"Hast du es stehlen wollen, weil es absurd ist?", fragte
Lilið amüsiert.

"Vielleicht?" Marusch schmunzelte verschmitzt und berührte
sie abermals an der Wange. "Und um es zu lesen, wie ich schon
sagte. Wenn man keinen Sinn im Leben hat, ist es egal, was für Ziele
dir doch noch irgendetwas geben. Dann gehst du denen halt
nach." Marusch runzelte kurz die Stirn, ohne das Schmunzeln
aufzulösen. "So ist es bei mir zumindest. Und ich glaube,
Heelem ist auch ein bisschen so."

Lilið musste an Allil denken. Ob sie auch keinen Sinn im
Leben sah? Aber sie verdrängte den Gedanken rasch. Marusch sah
keinen Sinn im Leben und wollte sie trotzdem nicht ermorden. Und
dass, obwohl sie ihren Zielen im Weg stand. Lilið
wollte gedanklich gerade viel lieber bei Marusch bleiben. Und
bei dem Verlangen, das sie bei Maruschs zweiter Berührung nicht
unterdrückt hatte, sich ihr wieder körperlich zu nähern. "Magst du wieder
küssen und solche Dinge?", fragte sie mit möglichst weicher
Stimme.

Marusch lächelte. "Sehr gern, wenn es dunkel wird."

Lilið runzelte die Stirn. "Ist dir das im Hellen unangenehm?", fragte
sie. Sie bemerkte, dass die Frage wertend klang. "Was in
Ordnung wäre. Ist es, weil dein Körper nicht so aussieht,
wie du es dir wünschen würdest?"

Marusch schüttelte den Kopf. "Ich glaube, ich hätte gern
kleine Brüste. Also, ich habe Brüste, aber ich glaube,
ich hätte gern, dass sie sich ein bisschen mehr wölben.", sagte
sie und strich sich dabei selbst mit den Händen über die
flache Brust. "Sonst bin ich eigentlich zufrieden. Und ich habe auch
kein Problem mit Nacktheit im Hellen. Vielleicht mag ich dich,
wenn du magst, sehr gern mal im Hellen nackt ausführlich
betrachten. Aber solange Licht da ist, würde ich mich
gern mit dem Buch auseinandersetzen. Gibst du es mir?"
